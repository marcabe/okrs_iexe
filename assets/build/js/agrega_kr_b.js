$(document).ready(function () {

	$("#descripcionkr").focus(function () {
        $("#msj_descripcionkr").hide();
        $("#descripcionkr").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });

    $("#fechaCumplimiento").focus(function () {
        $("#msj_fc").hide();
        $("#fechaCumplimiento").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });
    $("#fechaCumplimiento").focus(function () {
        $("#msj_ponderacion").hide();
    });

	$("#btnGuardar").click(function () {

		avanza=1;
        if ($("#descripcionkr").val() != '') {
            var descripcionkr = $("#descripcionkr").val()
        } else {
            avanza = 0;
            $("#msj_descripcionkr").show();
            $("#msj_descripcionkr").html("Debe ingresar el valor del avance");
            $("#msj_descripcionkr").css({'color': 'red'});
        }

        if ($("#fechaCumplimiento").val() != '') {
            var fechaCumplimiento = $("#fechaCumplimiento").val()
        } else {
            avanza = 0;
            $("#msj_fc").show();
            $("#msj_fc").html("Debe ingresar el valor del avance");
            $("#msj_fc").css({'color': 'red'});
        }

        if( $("#ponderacion").val() <= $("#ponderacionDisponible").html() || $("#ponderacion").val()!=''){
            var ponderacion = $("#ponderacion").val();

        }else{
            avanza = 0;
            $("#msj_ponderacion").show();
            $("#msj_ponderacion").html("La ponderacion debe ser menor o igual a la ponderacion disponible");
            $("#msj_ponderacion").css({'color': 'red'});

        }
        if(avanza==1){
			$.ajax({
                url: '../KeyResultController/insert',
                data: {
                    idObjetivo: $("#idObjetivo").val(),
                    descripcion: descripcionkr,
                    metrica: "Porcentaje",
                    orden: "Ascendente",
                    medicioncomienzo: 0,
                    medicionfinal: 100,
                    avance: 0,
                    ponderacion: ponderacion
                },
				type: 'POST',
				success: function (response) {
					if (response != 0) {
                        alert("Los registros han sido guardados correctamente");
                        window.location = '../lista_objetivos';
					} else if (response == 0) {
						alert("Error");
					}
				},
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
			});
		}
	});




});

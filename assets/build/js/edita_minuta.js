
$(document).ready(function () {

    $('.timepicker').wickedpicker();

    idAnexo = 0;
	idAcuerdo = 0;
    function showStatus(online) {
        const statusEl = document.querySelector('.onlineState');

        if (online) {
            statusEl.classList.remove('warning');
            statusEl.classList.add('success');
            statusEl.innerText = ` (Online)`;
        } else {
            statusEl.classList.remove('success');
            statusEl.classList.add('warning');
            statusEl.innerText = ` (Ofline)`;
        }
    }

    window.addEventListener('load', () => {
        // 1st, we set the correct status when the page loads
        navigator.onLine ? showStatus(true) : showStatus(false);

        // now we listen for network status changes
        window.addEventListener('online', () => {
            showStatus(true);
        });

        window.addEventListener('offline', () => {
            showStatus(false);
        });
    });

    $("#plan").change(function () {
        if($("#fecha").val()!='' ){
            $("#codigo").val($("#plan").val()+$("#fecha").val());
        }
    });

    $("#fecha").blur(function () {
        if($("#plan").val()!=''){
            $("#codigo").val($("#plan").val()+$("#fecha").val());
        }
    });


    $("#agregarAdjuntos").click(function () {
        idAnexo++;
        $("#formAnexo").append('' +
            '<div id="anexo' + idAnexo + '" style="height: 50px">' +
            '	<div class="col-md-8 col-sm-8 col-xs-12">' +
            '		<input type="file" class="form-control anexoFile" name="'+idAnexo+'" id="ane' + idAnexo + '">' +
            '		<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>' +
            '	</div>' +
            '	<div class="col-md-4 text-center" style="margin-top: 5px;">' +
            '		<button class="btn btn-danger btn-xs elimAnex" value="' + idAnexo + '" >' +
            '			<i class="fa fa-trash"></i>Eliminar' +
            '       </button>' +
            '	</div>' +
            '</div>'
        );
        $(".elimAnex").click(function () {
            var valorFila = $(this).attr("value");
            var archivo = $("#ane"+valorFila).val();
            $("#anexo" + valorFila).remove();
            $.ajax({
                url: '../MinutasController/deleteFile',
                dataType: "json",
                data: {
                    archivo: archivo,
                },
                type: 'POST',
                success: function (response) {}
            });

        });
    });

    $(".elimAnexData").click(function () {
        var valorFila = $(this).attr("value");
        var archivo = $("#ane"+valorFila).val();
        $("#anexoData" + valorFila).remove();
        $.ajax({
            url: '../MinutasController/deleteFileData',
            dataType: "json",
            data: {
                archivo: archivo,
            },
            type: 'POST',
            success: function (response) {}
        });

    });

    $("#agregarAcuerdos").click(function () {
        idAcuerdo++;

        $("#formAcuerdos").append('' +
            '<div id="acuerdo' + idAcuerdo + '" style="height: 50px">' +
            '	<div class="col-md-3">' +
            '   	<input class="form-control has-feedback-left" type="text" placeholder="Actividad">' +
            '	   <span class="fa fa-chevron-circle-right form-control-feedback left" aria-hidden="true"></span>' +
            '	</div>' +
            '	' +
            '	<div class="col-md-3">' +
            '   	<input class="form-control has-feedback-left" type="text" placeholder="Responsable">' +
            '	   	<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>' +
            '	</div>' +
            '' +
            '	<div class="col-md-5">' +
            '   <select class="col-md-4 text-center" style="height: 30px;" id="año">' +
            '   <option>2019</option>'+
            '   <option>2020</option>'+
            '   <option>2021</option>'+
            '   <option>2022</option>'+
            '   </select>'+
            '   <select class="col-md-4 text-center" style="height: 30px;" id="mes">' +
            '   <option>01</option>'+
            '   <option>02</option>'+
            '   <option>03</option>'+
            '   <option>04</option>'+
            '   <option>05</option>'+
            '   <option>06</option>'+
            '   <option>07</option>'+
            '   <option>08</option>'+
            '   <option>09</option>'+
            '   <option>10</option>'+
            '   <option>11</option>'+
            '   <option>12</option>'+
            '   </select>'+
            '   <select class="col-md-4 text-center" style="height: 30px;" id="dia">' +
            '   <option>01</option>'+
            '   <option>02</option>'+
            '   <option>03</option>'+
            '   <option>04</option>'+
            '   <option>05</option>'+
            '   <option>06</option>'+
            '   <option>07</option>'+
            '   <option>08</option>'+
            '   <option>09</option>'+
            '   <option>10</option>'+
            '   <option>11</option>'+
            '   <option>12</option>'+
            '   <option>13</option>'+
            '   <option>14</option>'+
            '   <option>15</option>'+
            '   <option>16</option>'+
            '   <option>17</option>'+
            '   <option>18</option>'+
            '   <option>19</option>'+
            '   <option>20</option>'+
            '   <option>21</option>'+
            '   <option>22</option>'+
            '   <option>23</option>'+
            '   <option>24</option>'+
            '   <option>25</option>'+
            '   <option>26</option>'+
            '   <option>27</option>'+
            '   <option>28</option>'+
            '   <option>29</option>'+
            '   <option>30</option>'+
            '   <option>31</option>'+
            '   </select>'+
            //'   	<input class="form-control has-feedback-left datepicker-here" data-language="es" type="text" id="acuerdoFecha'+idAcuerdo+'" placeholder="Fecha">' +
            //'		<span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>' +*/
            '	</div>'+
            '' +
            '	<div class="col-md-1 text-center">'+
            '		<button class="btn btn-danger btn-xs elimAcuerdo" value="' + idAcuerdo + '" >' +
            '			<i class="fa fa-trash"></i>Eliminar' +
            '   	</button>' +
            '	</div>'+
            '</div>'
        );

        $(".elimAcuerdo").click(function () {
            var valorFila = $(this).attr("value");
            $("#acuerdo" + valorFila).remove();
        });
    });


    $(".elimAcuerdoData").click(function () {
        var valorFila = $(this).attr("value");
        $.ajax({
            url: '../MinutasController/deleteAcuerdo',
            dataType: "json",
            data: {
                idAcuerdo: valorFila,
            },
            type: 'POST',
            success: function (response) {
                $("#acuerdoData" + valorFila).remove();
            }
        });
    });

    $(document).on('change', '.anexoFile', function(e) {
        var data = new FormData();
        var lugar = $(this).attr("name");

        var inputFileImage = document.getElementById("ane" + lugar);
        var file = inputFileImage.files[0];
        data.append('ane' + lugar, file);

        var url = '../MinutasController/upFile/';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            success: function (response) {
                if(response!='') {
                    $("#barraCarga").fadeOut(2000);
                    $("#ane"+lugar).attr("type", "text");
                    $("#ane"+lugar).val(response);
                    $("#ane"+lugar).attr("readonly", true);
                    $("#ane"+lugar).css("padding-left", "45px");
                }

            },
            xhr: function(){
                $("#barraProgresoCarga").css("width", 0 + "%");
                $("#barraCarga").show();
                var xhr = $.ajaxSettings.xhr() ;
                xhr.upload.onprogress=function (e) {
                    if (e.lengthComputable) {
                        var porcentaje = Math.floor((e.loaded / e.total) * 100);
                        $("#barraProgresoCarga").attr("data-transitiongoal", porcentaje);
                        $("#barraProgresoCarga").css("width", porcentaje + "%");
                        $("#porcentajeProgreso").text(porcentaje + '%');
                    }
                }
                return xhr ;
            }
        });
    });

    $("#guardar").click(function () {
        var avanza = 1;
        var lacuerdos = new Array();
        var anexos = new Array();
        if($("#plan option:selected").attr("name")==0){
            $("#msj_plan").html("Debe seleccionar un plan");
            $("#msj_plan").css({"color": "red"});
            avanza=0;
        }else{
            var plan = $("#plan option:selected").attr("name");
        }

        var fecha = $("#fecha").val();
        var hora = $("#hora").val();
        var lugar = $("#lugar").val();

        var objetivo = $("#objetivo").val();
        var asunto = $("#asunto").val();
        var codigo = $("#codigo").val();

        $("#formAnexo").find('input:text').each(function () {
            var archivo = $(this).val();
            anexos.push(archivo);
        });
        $("#formAnexoData").find('input:text').each(function () {
            var archivo = $(this).val();
            anexos.push(archivo);
        });


        var contador = 0;
        $("#formAcuerdosData").find('input:text').each(function () {
            if(contador==0) {
                avanceA = $(this).val();
                contador++;
            }else if(contador==1){
                actividadA  = $(this).val();
                contador++;
            }else if(contador==2) {
                responsableA = $(this).val();
                var acuerdos = new Object();
                acuerdos.avance = avanceA;
                acuerdos.actividad = actividadA;
                acuerdos.responsable = responsableA;
                contador++;
                lacuerdos.push(acuerdos);
                contador = 0;
            }
        });


        contador = 0;
        var indice = 0;
        $("#formAcuerdosData").find('select option:selected').each(function () {
            if(contador==0){
                ano = $(this).val();
                contador++;

            }else if(contador==1){
                mes = $(this).val();
                contador++;

            }else if(contador==2){
                dia = $(this).val();
                fecha = dia+"/"+mes+"/"+ano;
                lacuerdos[indice].fecha = fecha;
                indice++;
                contador=0;
            }
        });


        var contador = 0;
        $("#formAcuerdos").find('input:text').each(function () {
            if(contador==0){
                actividadA  = $(this).val();
                contador++;
            }else if(contador==1) {
                responsableA = $(this).val();
                var acuerdos = new Object();
                acuerdos.avance = 0;
                acuerdos.actividad = actividadA;
                acuerdos.responsable = responsableA;
                contador++;
                lacuerdos.push(acuerdos);
                contador = 0;
            }
        });



        contador = 0;
        $("#formAcuerdos").find('select option:selected').each(function () {
            if(contador==0){
                ano = $(this).val();
                contador++;

            }else if(contador==1){
                mes = $(this).val();
                contador++;

            }else if(contador==2){
                dia = $(this).val();
                fecha = dia+"/"+mes+"/"+ano;
                lacuerdos[indice].fecha = fecha;
                indice++;
                contador=0;
            }
        });


        if(avanza==1) {
            $.ajax({
                url: '../MinutasController/edit/' + $("#minutaId").val(),
                data: {
                    idPlan: plan,
                    fecha: fecha,
                    avancePorcentaje: $("#avancePorcentaje").val(),
                    codigo: codigo,
                    hora: hora,
                    lugar: lugar,
                    objetivo: objetivo,
                    asunto: asunto,
                    anexsos: anexos,
                    acuerdos: lacuerdos
                },
                type: 'POST',
                success: function (response) {
                    if (response !=0) {
                        $("#minutaId").val(response);
                        window.location = "../lista_minutas";
                    }
                },
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
            });
        }

    });

    $("#regresar").click(function () {
        window.location="../lista_minutas";
    });

});

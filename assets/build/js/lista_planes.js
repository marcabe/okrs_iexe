function editarPlan(idObjetivo) {
    window.location = 'edita_plan/' + idObjetivo;
}

$(document).ready(function () {

    idAnexo = 0;
    idAnexoI = 0;
    $(".enviarChat").click(function () {
        var idPlan = $(this).attr("text");
        var para = $("#listaLider" + idPlan).val();
        var fechaHoy = $("#fechaHoy").val();
        var usuarioLogueado = $("#usuarioLogueado").val();
        var nombreLogueado = $("#nombreLogueado").val();
        var men = $("#mensajeChat" + idPlan).val();
        var mensaje = '' +
            '<li>' +
            '<div class="col-md-10 mensajeChatDer">' +
            '<label>' + nombreLogueado + '(' + usuarioLogueado + ')</label>' +
            '<p>' + men + '</p>' +
            '<small class="col-md-12 fechamen text-right">' + fechaHoy + '</small>' +
            '</div>' +
            '</li>';
        $("#chatPlan" + idPlan).append(mensaje);
        $("#PlanChat" + idPlan).animate({scrollTop: $('#PlanChat' + idPlan)[0].scrollHeight}, 1000);
        $("#mensajeChat").val('');

        $.ajax({
            url: 'ChatController/insert',
            dataType: "json",
            data: {
                mensaje: men,
                idUsuario: $("#usuarioLogueado").val(),
                tipo: 'plan',
                para: para,
                idTipo: idPlan
            },
            type: 'POST',
            success: function (response) {
                $("#mensajeChat" + idPlan).val('')
            }
        });

    });


    $(".iconito").click(function () {
        if ($(this).hasClass('fa fa-arrow-circle-down iconito')) {
            $(this).removeClass('fa fa-arrow-circle-down iconito');
            $(this).addClass('fa fa-arrow-circle-up iconito');
        } else if ($(this).hasClass('fa fa-arrow-circle-up iconito')) {
            $(this).removeClass('fa fa-arrow-circle-up iconito');
            $(this).addClass('fa fa-arrow-circle-down iconito');
        }

    });

    $(".eliminaPlan").click(function () {
        //Mostramos modal de validacion
        $("#leyendaEliminar").text('Esta apunto de eliminar un plan')
        $('#modalClave').modal();
        planElimina = $(this).val();
    });

    $("#validaClave").click(function () {
        var clave = $("#contraseña").val();
        if (clave != '') {
            $.ajax({
                url: 'UsuariosController/getUserByClaveUser',
                data: {
                    user: $("#usuarioLogueado").val(),
                    clave: clave
                },
                type: 'POST',
                success: function (response) {
                    if (response == 1) {
                        $.ajax({
                            url: 'elimina_plan/' + planElimina,
                            data: {},
                            type: 'POST',
                            success: function (response) {
                                if (response != 0) {
                                    $("#listaPlanes" + planElimina).hide();
                                    $('#modalClave').modal('toggle');
                                } else if (response == 0) {
                                    alert("Ubo un error");
                                }
                            }
                        });

                    } else if (response == 0) {
                        $("#msj_claveNoValida").html("La clave es incorrecta");
                        $("#msj_claveNoValida").fadeIn();
                        $("#msj_claveNoValida").css({"color": "red"});
                        $("#msj_claveNoValida").fadeOut(4000);
                    }
                }
            });
        }
    });


    $("#descripcionavance").focus(function () {
        $("#msj_descripcionavance").hide();
        $("#descripcionavance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });

    $("#avance").focus(function () {
        $("#msj_avance").hide();
        $("#avance").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
    });

    $(".updateIndicador").click(function () {
        idIndicadorUpdate = $(this).val();
        $.ajax({
            url: 'IndicadoresTempController/validaAprobado',
            data: {
                idIndicador: idIndicadorUpdate,
            },
            type: 'POST',
            success: function (response) {
                console.log(response);
                if (response != 0) {
                    $('#modalIndicador').modal();
                } else if (response == 0) {
                    $('#updateCancel').modal();


                }
            }
        });
    });

    $(".updateAvance").click(function () {
        $("#avance").val('');
        $("#descripcionavance").val('');
        idIndicador = $(this).val();
        $.ajax({
            url: 'BitacoraIndicadorController/validaAprobado',
            data: {
                idIndicador: idIndicador,
            },
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $.ajax({
                        url: 'IndicadoresController/getById',
                        data: {
                            idIndicador: idIndicador,
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                indicador = JSON.parse(response);
                                console.log(indicador);
                                $("#tituloIndicador").html(indicador[0].nombreIndicador);
                                $("#tipometrica").html(indicador[0].nombreIndicador);
                                $("#actual").html('Último avance : ' + indicador[0].avance);
                                $("#rango").html('El rango debe de ser entre (' + indicador[0].inicio + ')  y (' + indicador[0].final + ')');
                                $('#myModal').modal();
                            } else if (response == 0) {
                                alert("Ubo un error");
                            }
                        }
                    });
                } else if (response == 0) {
                    $('#aprobadoCancel').modal();
                }
            }
        });
    });

    /*$("#avance").blur(function () {
        $("#avancet").val(parseInt($(this).val())+ parseInt(indicador[0].avance));
    });*/


    $(".autorizaAvance").click(function () {
        botonDesaparecer = $(this);
        idBitacora = $(this).val();
        $.ajax({
            url: 'BitacoraIndicadorController/getById',
            data: {
                idBitacora: idBitacora,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#tablaAnexos").empty();

                    var count = 0;
                    bitacora = JSON.parse(response);
                    console.log(bitacora);
                    indicadorTa
                    $("#indicadorTa").html(bitacora[0].tituloInd);
                    $("#mensajeValida").html('Último avance elaborado por: ' + bitacora[0].capturista);
                    $("#capturista").html(bitacora[0].capturista);
                    $("#avanceValida").html('Valor: ' + bitacora[0].avance);
                    $("#descripcionValida").html('Descripción: ' + bitacora[0].descripcion);

                    $('#modalValida').modal();
                    /*for (count = 0; count < bitacora[0].anexos.length; count++) {
                        var fila = "<tr><td><a href='pdfstempInd/" + bitacora[0].anexos[count].file + "' target='_blank'> <i class='fa fa-file-pdf-o fa-2x' style='color: red;'></i> " + bitacora[0].anexos[count].file + "</a> </td></tr>"
                        $("#tablaAnexos").append(fila);
                    }*/
                } else if (response == 0) {
                    alert("Ubo un error");
                }
            }
        });
    });


    $("#btnAceptarValida").click(function () {
        $.ajax({
            url: 'BitacoraIndicadorController/aprobar',
            data: {
                idBitacora: idBitacora,
                userAprobado: $("#usuarioLogueado").val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $.ajax({
                        url: 'IndicadoresController/editarAvance',
                        data: {
                            idIndicador: bitacora[0].idIndicador,
                            avance: bitacora[0].avance
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                $('#modalValida').modal('toggle');
                                $("#indicadorI" + bitacora[0].idIndicador).attr("data-transitiongoal", bitacora[0].avance);
                                $("#indicadorI" + bitacora[0].idIndicador).css("width", bitacora[0].avance + "%");
                                $("#avance" + bitacora[0].idIndicador).html(bitacora[0].avance);
                                $("#small" + bitacora[0].idIndicador).text((bitacora[0].avance) + "%");
                                $("#marcadorAvance" + bitacora[0].idIndicador).hide();
                                botonDesaparecer.hide();


                            } else if (response == 0) {
                                alert("Ubo un error");
                                return 0;
                            }
                        }
                    });
                } else if (response == 0) {
                    alert("Ubo un error");
                    return 0;
                }
            }
        });
    });


    $("#btnAceptarEditarIndicador").click(function () {
        var avanza = 1;
        var vinicial = $("#vinicial").val();
        var vfinal = $("#vfinal").val();
        if (vinicial > vfinal) {
            $("#msj_vinicial").text('El valor inicial debe ser menor al valor final');
            avanza = 0;
        }
        if (vinicial < 0) {
            $("#msj_vinicial").text('El valor inicial no puede ser negativo');
            avanza = 0;
        }
        if (vfinal < 0) {
            $("#msj_vfinal").text('El valor final no puede ser negativo');
            avanza = 0;
        }
        if (avanza == 1) {
            $.ajax({
                url: 'IndicadoresTempController/insert/',
                data: {
                    inicio: vinicial,
                    final: vfinal,
                    idIndicadores: idIndicadorUpdate,
                    status: 0
                },
                type: 'POST',
                success: function (response) {
                    var indic = JSON.parse(response);
                    $('#modalIndicador').modal('toggle');
                    $("#vinicial").val('');
                    $("#vfinal").val('');
                }
            });
        }
    });

    $(".updateSiIndicador").click(function () {
        indiUpdate = $(this).val();
        $('#modalClaveEditar').modal();
    });

    $("#validaClaveEditar").click(function () {
        var clave = $("#contraseñaUpdate").val();
        if (clave != '') {
            $.ajax({
                url: 'UsuariosController/getUserByClaveUser',
                data: {
                    user: $("#usuarioLogueado").val(),
                    clave: clave
                },
                type: 'POST',
                success: function (response) {
                    if (response == 1) {
                        $.ajax({
                            url: 'cambioIndicador/' + indiUpdate,
                            data: {},
                            type: 'POST',
                            success: function (response) {
                                if (response != 0) {
                                    window.location = "lista_planes";
                                    // $('#modalClaveEditar').modal('toggle');
                                } else if (response == 0) {
                                    alert("Ubo un error");
                                }
                            }
                        });
                        alert("Procedemos a hacer el cambio");

                    } else if (response == 0) {
                        alert("Contraseña erronea");
                    }
                }
            });
        }
    });


    $(".agregaAnexo").click(function () {
        idAnexoI ++;
        $("#formAnexoIndicador").append('' +
            '<div id="anexo' + idAnexoI + '" style="height: 50px">' +
            '	<div class="col-md-8 col-sm-8 col-xs-12">' +
            '		<input type="file" class="form-control anexoFileI" name="' + idAnexoI + '" id="ane' + idAnexoI + '">' +
            '		<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>' +
            '	</div>' +
            '	<div class="col-md-4 text-center" style="margin-top: 5px;">' +
            '		<button class="btn btn-danger btn-xs elimAnex" value="' + idAnexoI + '" >' +
            '			<i class="fa fa-trash"></i>Eliminar' +
            '       </button>' +
            '	</div>' +
            '</div>'
        );
        $(".elimAnex").click(function () {
            var valorFila = $(this).attr("value");
            var archivo = $("#ane" + valorFila).val();
            $("#anexo" + valorFila).remove();
            $.ajax({
                url: 'BitacoraIndicadorController/borrarArchivoTemporal',
                dataType: "json",
                data: {
                    archivo: archivo,
                },
                type: 'POST',
                success: function (response) {
                }
            });

        });
    });


    $(document).on('change', '.anexoFileI', function (e) {
        var data = new FormData();
        var lugar = $(this).attr("name");

        var inputFileImage = document.getElementById("ane" + lugar);
        var file = inputFileImage.files[0];
        data.append('ane' + lugar, file);

        var url = 'BitacoraIndicadorController/uploadFileTemp/';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            success: function (response) {
                if (response != '') {
                    $("#barraCarga").fadeOut(5000);
                    $("#ane" + lugar).attr("type", "text");
                    $("#ane" + lugar).val(response);
                    $("#ane" + lugar).attr("readonly", true);
                    $("#ane" + lugar).css("padding-left", "45px");
                }

            },
            xhr: function () {
                $("#barraProgresoCarga").css("width", 0 + "%");
                $("#barraCarga").show();
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        var porcentaje = Math.floor((e.loaded / e.total) * 100);
                        $("#barraProgresoCarga").attr("data-transitiongoal", porcentaje);
                        $("#barraProgresoCarga").css("width", porcentaje + "%");
                        $("#porcentajeProgreso").text(porcentaje + '%');
                    }
                }
                return xhr;
            }
        });
    });


    $("#btnFinalizar").click(function () {
        $("#formAnexo").empty();
    });


    $("#btnNoAutorizar").click(function () {
        console.log(bitacora[0]);

        $("#modalValida").modal('toggle');
        $("#motivoCancel").html(bitacora[0].descripcion);
        $("#avanceDe").html(bitacora[0].capturista);
        $("#noAutorizar").modal();
    });

    $("#aceptarCancelacion").click(function () {
        var idBitacora = bitacora[0].idBitacora;
        var motivo = $("#motivo").val();
        $.ajax({
            url: 'BitacoraIndicadorController/rechazar',
            data: {
                idBitacora: idBitacora,
                motivo: motivo,
                userNoAutorizo: $("#usuarioLogueado").val(),
            },
            type: 'POST',
            success: function (response) {
                $("#marcadorAvance" + bitacora[0].idIndicador).hide();
            }
        });
    });


    $(".avanceCancelado").click(function () {
        idIndicador = $(this).val();
        $.ajax({
            url: 'BitacoraIndicadorController/cancelado',
            data: {
                idIndicador: idIndicador,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    info = JSON.parse(response);
                    console.log(info);

                    $("#tituloCancelado").html("Key Result Cancelado");
                    $("#usuarioCancelado").html('Cancelado por: ' + info[0].userCancel);
                    $("#avanceCancelado").html('Avance Cancelado : ' + info[0].avance);
                    $("#motivoCancelado").html('Motivo: ' + info[0].motivo);
                    $('#modalCancelado').modal();
                } else if (response == 0) {
                    alert("Ubo un error");
                }
            }
        });
    });

    $("#btnAceptarCancelado").click(function () {
        var idBitacora = info[0].idBitacora;
        $.ajax({
            url: 'BitacoraIndicadorController/validaCancelado',
            data: {
                idBitacoraEnvio: idBitacora
            },
            type: 'POST',
            success: function (response) {
                if (response > 0) {
                    $("#modalCancelado").modal('toggle');
                    $("#avic" + info[0].idIndicador).hide();

                } else {
                    alert("Ubo un error");
                }
            }
        });
    });

    $(".bitacoraLista").click(function () {
        var i = 0;
        $.ajax({
            url: 'BitacoraIndicadorController/getByIdIndicador',
            data: {
                idIndicador: $(this).val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    var anexos = '';
                    var i = 0;
                    var tam = 0;
                    var j = 0;
                    $("#tablaBitacora tbody tr").remove();
                    var dataResponse = JSON.parse(response);
                    console.log(dataResponse);
                    $("#tIndicador").html(dataResponse[0].tituloIndicador);
                    for (i = 0; i < dataResponse.length; i++) {
                        for (j = 0; j < dataResponse[i].adjuntos.length; j++) {
                            anexos += "<a download style='font-size: 10px;' href='pdfstempInd/" + dataResponse[i].adjuntos[j].file + "'>" + dataResponse[i].adjuntos[j].file + "</a><br><br>";
                        }
                        $("#tablaBitacora tbody").append(
                            "<tr>" +
                            "<td>" +
                            dataResponse[i].fecha +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].descripcion +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].avance +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].avance +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].aprobado +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].user +
                            "</td>" +
                            "<td>" +
                            dataResponse[i].motivo +
                            "</td>" +
                            "<td>" +
                            anexos +
                            "</td>" +
                            "</tr>"
                        );
                    }
                    $("#modalBitacora").modal();
                } else if (response == 0) {
                    $("#tablaBitacora tbody tr").remove();
                    $("#indicadorT").html("Sin bitácora");
                    $("#tIndicador").html("");
                    $("#modalBitacora").modal();
                }
            }
        });
    });

    $(".listaLider").change(function () {
        var user = $(this).val();
        var i = 0;
        var idPlan = $(this).attr("name");
        $("#chatPlan" + idPlan).empty();
        $.ajax({
            url: "ChatController/getMeParaPlaen",
            type: "POST",
            data: {
                usuario: user,
                idPlan: idPlan,
                me: $("#usuarioLogueado").val()
            },
            success: function (response) {
                var dataChat = JSON.parse(response);
                console.log(dataChat);
                for (i = 0; i < dataChat.length; i++) {
                    var mensaje = '' +
                        '<li>' +
                        '<div class="col-md-10 mensajeChatDer">' +
                        '<label>' + dataChat[i].nombre + '(' + dataChat[i].nombrePuesto + ')</label>' +
                        '<p> ' + dataChat[i].mensaje + ' </p>' +
                        '<small class="col-md-12 fechamen text-right">' + dataChat[i].fechahora + '</small>' +
                        '</div>' +
                        '</li>';
                    $("#chatPlan" + idPlan).append(mensaje);
                }
            }

        });
    });

    $(".visto").click(function () {
        var idPlan = $(this).attr("text");
        var visto = $(this).attr("name");
        if (visto == 1) {
            var visible = 0;
            var clase = "fa fa-eye fa-2x visto";
            var claseAdd = "fa fa-eye-slash fa-2x visto";
        } else if (visto == 0) {
            var visible = 1;
            var clase = "fa fa-eye-slash fa-2x visto";
            var claseAdd = "fa fa-eye fa-2x visto";
        }
        $.ajax({
            url: "PlanesController/editVisible/" + idPlan,
            data: {
                visible: visible
            },
            type: "POST",
            success: function (response) {
                if (response != 0) {
                    $("#visible" + idPlan).removeClass(clase);
                    $("#visible" + idPlan).addClass(claseAdd);
                    $("#visible" + idPlan).attr("name", visible);
                }
            }
        });
    });


    $(".vaciarChat").click(function () {
        var idPlan = $(this).attr("name");
        $.ajax({
            url: "ChatController/deletePlan/" + idPlan,
            data: {},
            type: 'POST',
            success: function (response) {
                $("#chatPlan" + idPlan).hide();
            }
        });
    });


    $(".aceptarCambio").click(function () {
        $("#myModalUpdatePlan").modal();
        idCambioProy = $(this).attr("text");

        $.ajax({
            url: "PlanesController/getInter/" + idCambioProy,
            data: {},
            type: 'POST',
            success: function (response) {
                var dataCambio = JSON.parse(response);
                console.log(dataCambio);
                $("#p2018").text(dataCambio[0].presupuesto2018);
                $("#p2019").text(dataCambio[0].presupuesto2019);
                $("#ie").text(dataCambio[0].indicadorEstrategico);
                $("#i2018").text(dataCambio[0].inversion2018);
                $("#i2019").text(dataCambio[0].inversion2019);
                $("#mi").text(dataCambio[0].metaIndicador);
            }
        });

    });

    $("#btnAceptarUpdatePlan").click(function () {
        $.ajax({
            url: "PlanesController/intercambio/" + idCambioProy,
            data: {},
            type: 'POST',
            success: function (response) {
                window.location = "lista_planes";
            }
        });
    });

    $("#noautorizarUpdate").click(function () {
        $.ajax({
            url: "PlanesController/cambiaStatusUpdate/" + idCambioProy,
            data: {},
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#aceptarCambio" + idCambioProy).hide();
                }
            }
        });
    });


    $(".editDiagnostico").click(function () {
        idProy = $(this).val();
        $.ajax({
            url: "PlanesController/getInfo/" + idProy,
            data: {},
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    var dataResponse = JSON.parse(response);
                    console.log(dataResponse);
                    $("#diagnostico").val(dataResponse[0].diagnostico);
                    $("#modalDiagnostico").modal();
                }

            }
        });
    });

    $("#aceptaDiganostico").click(function () {
        var diagnostico = $("#diagnostico").val();
        $.ajax({
            url: "PlanesController/updateInfo/" + idProy,
            data: {
                diagnostico: diagnostico
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    alert("Cambio generado correctamente");
                    $("#modalDiagnostico").modal('toggle');
                }

            }
        });
    });


    $(".adjuntarDiagnostico").click(function () {
        idPlanpdf = $(this).val();
        tipoAdjunto = $(this).attr("name");
        $("#modalAdjuntos").modal();
    });

    $(".agregaAnexoDiag").click(function () {
        idAnexo++;
        $("#formAnexoDiag").append('' +
            '<div id="anexo' + idAnexo + '" style="height: 50px">' +
            '	<div class="col-md-8 col-sm-8 col-xs-12">' +
            '		<input type="file" class="form-control anexoFile" name="' + idAnexo + '" id="ane' + idAnexo + '">' +
            '		<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>' +
            '       <small id="mensajeError'+idAnexo+'" style="display: none;"></small>'+
            '	</div>' +
            '	<div class="col-md-4 text-center" style="margin-top: 5px;">' +
            '		<button class="btn btn-danger btn-xs elimAnex" value="' + idAnexo + '" >' +
            '			<i class="fa fa-trash"></i>Eliminar' +
            '       </button>' +
            '	</div>' +
            '</div>'
        );
        $(".elimAnex").click(function () {
            var valorFila = $(this).attr("value");
            var archivo = $("#ane" + valorFila).val();
            $("#anexo" + valorFila).remove();
            $.ajax({
                url: 'pdfPlanesController/borrarArchivoTemporal',
                dataType: "json",
                data: {
                    archivo: archivo,
                },
                type: 'POST',
                success: function (response) {
                }
            });

        });
    });


    $(document).on('change', '.anexoFile', function (e) {
        var dato = $(this);
        var data = new FormData();
        var lugar = $(this).attr("name");
        var inputFileImage = document.getElementById("ane" + lugar);
        var file = inputFileImage.files[0];
        data.append('ane' + lugar, file);
        var desconcatena = (file.name).split(".");
        console.log(desconcatena);
        if(desconcatena[1]!="pdf" && desconcatena.length==2) {
            $("#mensajeError"+lugar).css({
                "color": "red"
            })
            $("#mensajeError"+lugar).show();
            $("#mensajeError"+lugar).html("El fortato debe de ser unicamente en pdf");
            $("#mensajeError"+lugar).fadeOut(3000);
            dato.val('');
            return 0;
        }
        var url = 'pdfPlanesController/upFile/';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            cache: false,
            success: function (response) {
                if (response != '') {
                    $("#barraCargadp").fadeOut(5000);
                    $("#ane" + lugar).attr("type", "text");
                    $("#ane" + lugar).val(response);
                    $("#ane" + lugar).attr("readonly", true);
                    $("#ane" + lugar).css("padding-left", "45px");
                }

            },
            xhr: function () {
                $("#barraProgresoCargadp").css("width", 0 + "%");
                $("#barraCargadp").show();
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        var porcentaje = Math.floor((e.loaded / e.total) * 100);
                        console.log(porcentaje)
                        $("#barraProgresoCargadp").attr("data-transitiongoal", porcentaje);
                        $("#barraProgresoCargadp").css("width", porcentaje + "%");
                        $("#barraProgresoCargadp").text(porcentaje + '%');
                    }
                }
                return xhr;
            }
        });
    });


    $("#btnAceptarDiagAnex").click(function () {
        var avanza = 1;
        var anexos = new Array();
        var i = 1;
        var conArchivos = 0;

        i = 1;
        $("#formAnexoDiag").find('input:text').each(function () {
            var archivo = $(this).val();
            anexos.push(archivo);
            i++;
        });
        if (avanza == 1) {
            $.ajax({
                url: 'pdfPlanesController/insert/' + idPlanpdf,
                data: {
                    archivos: anexos,
                    tipoAdjunto: tipoAdjunto
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        alert("Archivos adjuntos correctamente");
                        $("#modalAdjuntos").modal('toggle');
                        $("#formAnexoDiag").empty();
                    }
                }
            });
        }
    });

    $(".actualizarIndicador").click(function () {
        var anexosI = new Array();
        $("#avance").val('');
        $("#descripcionavance").val('');
        idIndicador = $(this).val();
        $.ajax({
            url: 'BitacoraIndicadorController/validaAprobado',
            data: {
                idIndicador: idIndicador,
            },
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    $.ajax({
                        url: 'IndicadoresController/getById',
                        data: {
                            idIndicador: idIndicador,
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                indicador = JSON.parse(response);
                                console.log(indicador);
                                $("#tituloIndicador").html(indicador[0].nombreIndicador);
                                $("#tipometrica").html(indicador[0].nombreIndicador);
                                $("#actual").html('Último avance : ' + indicador[0].avance);
                                $("#rango").html('El rango debe de ser entre (' + indicador[0].inicio + ')  y (' + indicador[0].final + ')');
                                $('#modalAcutaliza').modal();
                            } else if (response == 0) {
                                alert("Ubo un error");
                            }
                        }
                    });
                } else if (response == 0) {
                    $('#noAprobado').modal();
                }
            }
        });
    });

    $("#btnAceptar").click(function () {
        var avanza = 1;
        var anexosI = new Array();
        i=0;
        if ($("#avance").val() == '') {
            avanza = 0;
        }
        if ($("#descripcionavance").val() == '') {
            avanza = 0;
        }
        $("#formAnexoIndicador").find('input:text').each(function () {
            var archivo = $(this).val();
            anexosI.push(archivo);
            i++;
        });
        if (avanza == 1) {
            $.ajax({
                url: "BitacoraIndicadorController/insert",
                data: {
                    idIndicador: indicador[0].idIndicadores,
                    descripcion: $("#descripcionavance").val(),
                    ultimoAvance: indicador[0].avance,
                    avance: $("#avance").val(),
                    user: $("#usuarioLogueado").val(),
                    aprobado: 0,
                    archivos: anexosI
                },
                type: "POST",
                success: function (response) {
                    if(response != 0){
                        var av = parseFloat(response).toFixed(2);
                        $('#modalAcutaliza').modal('toggle');
                        $("#marcadorAvance" + idIndicador).show();
                        $("#updateAvance" + idIndicador).hide();
                        if ($("#tipoUsuario").val() == 'superadmin' || $("#tipoUsuario").val() == 'admin') {
                            $("#small" + idIndicador).empty();
                            $("#marcadorAvance" + idIndicador).html(parseFloat(av).toFixed(2) + '%');
                            $("#avance" + idIndicador).html(avance);
                        } else {
                            $("#marcadorAvance" + idIndicador).html(' ( ' + parseFloat(av).toFixed(2) + '% sin autorizar )');
                            $("#marcadorAvance" + idIndicador).css({'color': 'red'});
                        }
                        $("#avance").val('');
                        $("#descripcionavance").val('');
                    }
                }
            });
        } else {
            console.log("Error");
        }
    });

    $(".indicadorRechazado").click(function () {
        $("#aprobadoCancel").modal();
    });

    $(".eliminaInd").click(function () {
        var idIndicador  = $(this).attr("name");
        $.ajax({
            url: "IndicadoresController/delete",
            data: {
                idIndicador: idIndicador
            },
            type: "POST",
            success: function (response) {
                    $("#listaIndi"+idIndicador).hide();
            }
        });
    });
    
    $(".new_indicator").click(function () {
        idPlanNew = $(this).attr("name");
        $("#modalAddIndicador").modal();

    });
    
    $("#guardarIndicador").click(function () {
        var lindicadores = new Array();
        var indicadores = new Object();
        var metrica;
        indicadores.indicador = $("#indicador").val();
        indicadores.valorInicial = $("#vinicial").val();
        indicadores.valorFinal = $("#vfinal").val();
        indicadores.metrica = $("#metrica").val();
        lindicadores.push(indicadores);
        $.ajax({
            url: "IndicadoresController/insert",
            data: {
                idPlan: idPlanNew,
                indicadores: lindicadores
            },
            type: 'POST',
            dataType: "json",
            success: function () {
                if($("#metrica").val()=='p'){
                    metrica = "Porcentaje";
                }else if($("#metrica").val()=='m'){
                    metrica = "Minutos";
                }else if($("#metrica").val()=='c') {
                    metrica = "Cantidad";
                }
                $("#cuerpoIndicador"+idPlanNew).append(`
                    <tr>
                        <td>
                            ${$("#indicador").val()}
                        </td>
                        <td>
                            ${metrica}
                        </td>
                        <td>
                            0
                        </td>
                        <td>
                            <div class="project_progress">
                                <small>0%</small> 
                                <div class="progress progress_sm">
                                    <div
                                        class="progress-bar bg-green"
                                        role="progressbar"
                                        data-transitiongoal="0.00"
                                        aria-valuenow="56"
                                        style="width: 0%;">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            ${$("#vinicial").val()}
                        </td>
                        <td>
                            ${$("#vfinal").val()}
                        </td>
                        
                    </tr>
                `);
                alert("Los registros han sido guardados correctamente");
                $("#modalAddIndicador").modal("toggle");

            }
        });
    });
    
    $(".editarIndi").click(function () {
        var idIndicador = $(this).attr("name");
        $.ajax({
            url: "IndicadoresController/getById",
            data: {
                idIndicador: idIndicador
            },
            type: 'POST',
            success: function (response) {
                datosIndi = JSON.parse(response);
                console.log(datosIndi);
                $("#modalUpdateIndicador").modal();
                $("#Updateindicador").val(datosIndi[0].nombreIndicador);
                $("#Updatemetrica").val(datosIndi[0].metrica);
                $("#Updatevinicial").val(datosIndi[0].inicio);
                $("#Updatevfinal").val(datosIndi[0].final);
            }
        });
    });

    $("#updateguardarIndicador").click(function () {
        $.ajax({
            url: "IndicadoresController/updateIndi/"+datosIndi[0].idIndicadores,
            data: {
                nombreIndicador : $("#Updateindicador").val(),
                inicio : $("#Updatevinicial").val(),
                final : $("#Updatevfinal").val(),
                metrica : $("#Updatemetrica").val(),
            },
            type: 'POST',
            success: function (response) {
                $("#modalUpdateIndicador").modal("toggle");
            }
        });

    });


    $(".vadjuntos").click(function () {
        var i = 0;
        $.ajax({
                url: "pdfPlanesController/getAllByIdPlan/",
            data: {
                idPlan: $(this).val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#tablaBitacora tbody tr").remove();
                    var dataResponse = JSON.parse(response);
                    console.log(dataResponse.length);
                    console.log(dataResponse);
                    if (dataResponse.length > 0) {
                        $("#tIndicador").html(dataResponse[0].tituloAcuerdo);
                        for (i = 0; i < dataResponse.length; i++) {
                        	if(dataResponse[i].tipo=='p'){
                        		tip="Policy memo";
                        	}else if(dataResponse[i].tipo=='d'){
                        		tip = "Adjuntos Adiagnostico";
                        	}
                            $("#tablaBitacora tbody").append(
                                "<tr>" +
                                	"<td style='font-size: 15px;'>" +
                                		"<a download href='pdfsminutas/" + dataResponse[i].pdf + "'><i style='color: red;' class='fa fa-file-pdf-o fa-2x'></i> " + dataResponse[i].pdf + "</a>" +
                                	"</td>"+
                                	"<td>" +
                                		tip+
                                	"</td>"+
                                	"<td>"+
                                		"<button class='btn btn-success btn-xs elimArch' value='" + dataResponse[i].idPdfPlan + "'>Eliminar</button>" +
                                	"</td>" +
                                "</tr>"
                            );
                        }
                        $("#modalAcuerdos").modal();
                    } else {
                        $("#tablaBitacora tbody tr").remove();
                        $("#indicadorT").html("Sin bitácora");
                        $("#tIndicador").html("");
                        $("#modalAcuerdos").modal();
                    }
                }
            }
        });
    });

    $(".justificacion").click(function () {
        var idPdfPlan = $(this).val();
        $.ajax({

            url: "PlanesController/getInfo/" + idPdfPlan,
            data: {},
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    dataResponseJustifica = JSON.parse(response);
                    console.log(dataResponseJustifica);
                    $("#justifica").val(dataResponseJustifica[0].justificacion);
                    $("#modalJustificacion").modal();
                }
            }
        });
    });


    $("#aceptarJustifica").click(function () {
        $.ajax({
            url: "PlanesController/updateInfo/"+dataResponseJustifica[0].idMv,
            data: {
                justificacion: $("#justifica").val(),
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#modalJustificacion").modal('toggle');
                }
            }
        });
    });



$(document).on('click', '.elimArch', function (e) {
        var idPdfPlan = $(this).val();
        $.ajax({
            url: 'pdfPlanesController/delete',
            data: {
                idPdfPlan: idPdfPlan,
            },
            type: 'POST',
            success: function (response) {
                if (response != 0) {
                    $("#modalAcuerdos").modal("toggle");
                }

            }
        });
    });



});

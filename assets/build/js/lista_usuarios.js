$(document).ready(function () {


	function showStatus(online) {
		const statusEl = document.querySelector('.onlineState');

		if (online) {
			statusEl.classList.remove('warning');
			statusEl.classList.add('success');
			statusEl.innerText = ` (Online)`;
		} else {
			statusEl.classList.remove('success');
			statusEl.classList.add('warning');
			statusEl.innerText = ` (Ofline)`;
		}
	}

	window.addEventListener('load', () => {
		// 1st, we set the correct status when the page loads
		navigator.onLine ? showStatus(true) : showStatus(false);

		// now we listen for network status changes
		window.addEventListener('online', () => {
			showStatus(true);
		});

		window.addEventListener('offline', () => {
			showStatus(false);
		});
	});


	$(".editar").click(function () {
		url = "edita_usuario/"+$(this).val()
        url = encodeURI("edita_usuario/"+$(this).val());
        window.location=url;
	});

	/*$(".eliminar").click(function () {
		$.ajax({
			url: 'UsuariosController/delete',
			data: {
				user: $(this).val(),
			},
			type: 'POST',
			success: function (response) {
				if (response == 1) {
					alert("Usuario eliminado correctamente");
					window.location='lista_usuarios';
				} else if (response == 0) {
					alert("Ubo un error");
				}
			}
		});
	});*/

    $(".eliminar").click(function () {
        user = $(this).val();
        $("#myModal").modal();
    });
    
    $("#btnAceptar").click(function () {
        $.ajax({
            url: 'UsuariosController/delete',
            data: {
                user: user,
            },
            type: 'POST',
            success: function (response) {
                if (response == 1) {
                    alert("Usuario eliminado correctamente");
                    window.location='lista_usuarios';
                }
            },
            xhr: function(){
                var xhr = $.ajaxSettings.xhr() ;
                xhr.onloadstart = function(e) {
                    $("#fondoLoader").show();
                };
                xhr.upload.onloadend = function (e) {
                    $("#fondoLoader").fadeOut(2000);
                }
                return xhr ;
            }
        });
    });

    $('#table-usuarios').DataTable({
        "paging": false,
        "ordering": true,
        "info": false,
        "searching": true,
        "language": {
            "search": "Buscar:"
        }
    });




});

$(document).ready(function () {
    addIndicador = 0;
    $("#proyecto").focus(function () {
        $("#msj_proyecto").html('');
        $("#proyecto").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#status").focus(function () {
        $("#msj_status").html('');
        $("#status").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#finicio").focus(function () {
        $("#msj_finicio").html('');
        $("#finicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#ffin").focus(function () {
        $("#msj_ffin").html('');
        $("#ffin").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#lider").focus(function () {
        $("#msj_lider").html('');
        $("#lider").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#inversion").focus(function () {
        $("#msj_inversion").html('');
        $("#inversion").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#finalidad").focus(function () {
        $("#msj_finalidad").html('');
        $("#finalidad").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#poblacionP").focus(function () {
        $("#msj_poblacionP").html('');
        $("#poblacionP").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#proposito").focus(function () {
        $("#msj_proposito").html('');
        $("#proposito").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#poblacionO").focus(function () {
        $("#msj_poblacionO").html('');
        $("#poblacionO").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#presupuesto2018").focus(function () {
        $("#msj_presupuesto2018").html('');
        $("#presupuesto2018").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#presupuesto2019").focus(function () {
        $("#msj_presupuesto2019").html('');
        $("#presupuesto2019").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#indicadorEstrategico").focus(function () {
        $("#msj_indicadorEstrategico").html('');
        $("#indicadorEstrategico").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#inversion2018").focus(function () {
        $("#msj_inversion2018").html('');
        $("#inversion2018").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#inversion2019").focus(function () {
        $("#msj_inversion2019").html('');
        $("#inversion2019").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });

    $("#metaIndicador").focus(function () {
        $("#msj_metaIndicador").html('');
        $("#metaIndicador").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
    });


    $("#btnGuardar").click(function () {
        var finalizar = 1;
        var contador = 0;
        var lindicadores = new Array();

        if ($("#proyecto").val() == '') {
            $("#msj_proyecto").html("Debe ingresar el nombre del proyecto");
            $("#proyecto").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#status").val() == '') {
            $("#msj_status").html("Debe ingresar el status del proyecto");
            $("#status").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#finicio").val() == '') {
            $("#msj_finicio").html("Debe ingresar la fecha inicial del proyecto");
            $("#finicio").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#ffin").val() == '') {
            $("#msj_ffin").html("Debe ingresar la fecha final del proyecto");
            $("#ffin").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#lider").val() == '') {
            $("#msj_lider").html("Debe ingresar el lider del proyecto");
            $("#lider").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#inversion").val() == '') {
            $("#msj_inversion").html("Debe ingresar la inversion del proyecto");
            $("#inversion").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#finalidad").val() == '') {
            $("#msj_finalidad").html("Debe ingresar la finalidad del proyecto");
            $("#finalidad").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#poblacionP").val() == '') {
            $("#msj_poblacionP").html("Debe ingresar la poblacion potencial del proyecto");
            $("#poblacionP").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#proposito").val() == '') {
            $("#msj_proposito").html("Debe ingresar el proposito del proyecto");
            $("#proposito").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#poblacionO").val() == '') {
            $("#msj_poblacionO").html("Debe ingresar la poblacion objetivo");
            $("#poblacionO").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#presupuesto2018").val() == '') {
            $("#msj_presupuesto2018").html("Debe ingresar el presupusto 2018");
            $("#presupuesto2018").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#presupuesto2019").val() == '') {
            $("#msj_presupuesto2019").html("Debe ingresar el presupusto 2019");
            $("#presupuesto2019").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#indicadorEstrategico").val() == '') {
            $("#msj_indicadorestrategico").html("Debe ingresar el indicador estrategico");
            $("#indicadorEstrategico").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#inversion2018").val() == '') {
            $("#msj_inversion2018").html("Debe ingresar la inversion del 2018");
            $("#inversion2018").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#inversion2019").val() == '') {
            $("#msj_inversion2019").html("Debe ingresar la inversion de 2019");
            $("#inversion2019").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#metaIndicador").val() == '') {
            $("#msj_metaIndicador").html("Debe ingresar la meta del indicar del proyecto");
            $("#metaIndicador").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#codigo").val() == '') {
            $("#msj_codigo").html("Debe ingresar el código");
            $("#codigo").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#ie").val() == '') {
            $("#msj_ie").html("Debe ingresar el indicador estratégico");
            $("#ie").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#mie").val() == '') {
            $("#msj_mie").html("Debe ingresar el código");
            $("#mie").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }


        if ($("#i2018").val() == '') {
            $("#msj_i2018").html("Debe ingresar el código");
            $("#i2018").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        if ($("#i2019").val() == '') {
            $("#msj_i2019").html("Debe ingresar el código");
            $("#i2019").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

        $("#fila_indicadores").find('input:text').each(function () {
            if (contador == 0) {
                indicador = $(this).val();
                contador++;
            } else if (contador == 1) {
                valorInicial = $(this).val();
                contador++;
            } else if (contador == 2) {
                valorFinal = $(this).val();
                var indicadores = new Object();
                indicadores.indicador = indicador;
                indicadores.valorInicial = valorInicial;
                indicadores.valorFinal = valorFinal;
                lindicadores.push(indicadores);
                contador = 0;
            }
        });
        var indice = 0;
        $("#fila_indicadores").find('select option:selected').each(function () {
            metrica = $(this).val();
            lindicadores[indice].metrica = metrica;
            indice++
        });

        if (finalizar == 0) {
            return false;
        } else if (finalizar == 1) {
            $.ajax({
                url: 'PlanesController/insert',
                data: {
                    mv: $("#proyecto").val(),
                    status: $("#status").val(),
                    lider: $("#lider").val(),
                    inversionT: $("#inversion").val(),
                    fin: $("#finalidad").val(),
                    poblacionPotencial: $("#poblacionP").val(),
                    proposito: $("#proposito").val(),
                    poblacionObjetivo: $("#poblacionO").val(),
                    presupuesto2018: $("#presupuesto2018").val(),
                    presupuesto2019: $("#presupuesto2019").val(),
                    indicadorEstrategico: $("#indicadorEstrategico").val(),
                    inversion2018: $("#inversion2018").val(),
                    inversion2019: $("#inversion2019").val(),
                    metaIndicador: $("#metaIndicador").val(),
                    finicial: $("#finicio").val(),
                    ffinal: $("#ffin").val(),
                    idReuniones: $("#reunion").val(),
                    beneficio: $("#beneficio").val(),
                    diagnostico: $("#diagnostico").val(),
                    codigo: $("#codigo").val(),
                    iestrategico: $("#ie").val(),
                    metodoiestrategico: $("#mie").val(),
                    d_inversion2018: $("#i2018").val(),
                    d_inversion2019: $("#i2019").val()

                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var idPlan = response;
                        console.log(idPlan);
                        $.ajax({
                            url: "IndicadoresController/insert",
                            data: {
                                idPlan: idPlan,
                                indicadores: lindicadores
                            },
                            type: 'POST',
                            dataType: "json",
                            success: function () {
                                alert("Los registros han sido guardados correctamente");
                                window.location = 'lista_planes';
                            }
                        });
                    } else if (response == 0) {
                        return false;
                    }
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr;
                }
            });
        }
    });

    $("#add_indicador").click(function () {
        addIndicador++
        $("#fila_indicadores").append(`
            <div class="row filaGrupo" id="indicador${addIndicador}">
                <div class="col-md-3">
                    <div class="form-group row text-right">
                        <div class="col-md-12">
                            <input type="text" class="form-control has-feedback-left" id="fisico" >
                            <span class="fa fa-dollar form-control-feedback left" aria-hidden="true"></span>
                            <small id="msj_presupuesto2018"></small>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group row text-right">
                        <div class="col-md-12">
                            <select class="form-control has-feedback-left sel" id="sel${addIndicador}">
                                <option value="0">Selecciona una opción</option>
                                <option value="c">Cantidad</option>
                                <option value="p">Porcentaje</option>
                                <option value="m">Minutos</option>
                            </select>
                            <span class="fa fa-dot-circle-o form-control-feedback left" aria-hidden="true"></span>
                            <small id="msj_presupuesto2019"></small>
                        </div>
                    </div>                                                                                                                                      
                </div>

                <div class="col-md-2">
                    <div class="form-group row text-center">
                        <div class="col-md-12">
                            <input type="text" class="form-control has-feedback-left" id="vifi${addIndicador}" placeholder="Valor inicial">
                            <span class="fa fa-hourglass form-control-feedback left" aria-hidden="true"></span>
                            <small id="msj_vifi"></small>
                        </div>
                    </div>
                </div>
                
                
                <div class="col-md-2">
                    <div class="form-group row text-center">
                        <div class="col-md-12">
                            <input type="text" class="form-control has-feedback-left" id="vffi"placeholder="Valor final">
                            <span class="fa fa-exclamation form-control-feedback left" aria-hidden="true"></span>
                            <small id="msj_vffi"></small>
                        </div>
                    </div>
                 </div>
                 
                 <div class="col-md-2">
                    <div class="form-group row text-cenetr">
                        <div class="col-md-12">
                            <button class="btn btn-danger btn-xs elimAcuerdo" value="${addIndicador}"><i class="fa fa-trash-o"></i> Eliminar</button>
                        </div>
                    </div>
                 </div>
            </div>`
        );
        $(".elimAcuerdo").click(function () {
            var valorFila = $(this).attr("value");
            $("#indicador" + valorFila).remove();
        });


    });


});

$(document).ready(function () {
	$(".enviarChat").click(function () {
		var idPlan = $("#idPlan").val();
        var para = $("#listaLider"+idPlan).val();

        var fechaHoy = $("#fechaHoy").val();
		var usuarioLogueado = $("#usuarioLogueado").val();
		var nombreLogueado = $("#nombreLogueado").val();
        var puestoLogueado = $("#puestoLogueado").val();
		var men = $("#mensajeChat" + idPlan).val();
		var mensaje = '' +
			'<li>' +
			'<div class="col-md-10 mensajeChatDer">' +
			'<label>'+nombreLogueado+'('+puestoLogueado+')</label>' +
			'<p>' + men + '</p>' +
			'<small class="col-md-12 fechamen text-right">'+fechaHoy+'</small>' +
			'</div>' +
			'</li>';
		console.log("hasta aca llegamos");
		$("#chatPlan" + idPlan).append(mensaje);
		$("#PlanChat" + idPlan).animate({scrollTop: $('#PlanChat'+idPlan)[0].scrollHeight}, 1000);
		$("#mensajeChat").val('');

		$.ajax({
			url: 'ChatController/insert',
			dataType: "json",
			data: {
				mensaje: men,
				idUsuario: $("#usuarioLogueado").val(),
				tipo: 'plan',
                para: para,

                idTipo: idPlan
			},
			type: 'POST',
			success: function (response) {
				$("#mensajeChat" + idPlan).val('')
			}
		});

	});



    $(".listaLider").change(function () {
        var user = $(this).val();
        var i=0;
        var idPlan = $(this).attr("name");
        $("#chatPlan" + idPlan).empty();
        $.ajax({
            url: "ChatController/getMeParaPlaen",
            type: "POST",
            data: {
                usuario: user,
                idPlan : idPlan,
                me: $("#usuarioLogueado").val()
            },
            success: function (response) {
                var dataChat = JSON.parse(response);
                console.log(dataChat);
                for(i=0; i<dataChat.length; i++) {
                    var mensaje = '' +
                        '<li>' +
                        '<div class="col-md-10 mensajeChatDer">' +
                        '<label>'+dataChat[i].nombre+'('+dataChat[i].nombrePuesto+')</label>' +
                        '<p> '+dataChat[i].mensaje+' </p>' +
                        '<small class="col-md-12 fechamen text-right">'+dataChat[i].fechahora+'</small>' +
                        '</div>' +
                        '</li>';
                    $("#chatPlan" + idPlan).append(mensaje);
                }
            }

        });


    });


});
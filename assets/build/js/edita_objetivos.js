$(document).ready(function () {

	$("#mv").focus(function () {
		$("#msj_mv").hide();
		$("#mv").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#objetivo").focus(function () {
		$("#msj_objetivo").hide();
		$("#objetivo").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#descripcion").focus(function () {
		$("#msj_descripcion").hide();
		$("#descripcion").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#inicio").focus(function () {
		$("#msj_inicio").hide();
		$("#inicio").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});
	$("#final").focus(function () {
		$("#msj_final").hide();
		$("#final").css({"border": "1px solid #8ea6a2", "border-radius": "4px"});
	});


	$("#btnEditar").click(function () {
		var avanza = 1;
		var obj = new Object();

		if ($("#mv").val() == '0') {
			$("#msj_mv").html("Debe seleccionar unas misión / Visióp");
			$("#mv").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#mv").val() != '') {
			obj.idmv = $("#mv").val();
		}

		//Validamos el input del objetivo
		if ($("#objetivo").val() == '') {
			$("#msj_objetivo").html("Debe ingresar el objetivo");
			$("#objetivo").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#objetivo").val() != '') {
			obj.objetivo = $("#objetivo").val();
		}

		//Validamos el input de la descripcion
		if ($("#descripcion").val() == '') {
			$("#msj_descripcion").html("Debe ingresar la descripcion del objetivo");
			$("#descripcion").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#descripcion").val() != '') {
			obj.descripcion = $("#descripcion").val();
		}

		//Validamos el input de la fecha inicial
		if ($("#inicio").val() == '') {
			$("#msj_inicio").html("Debe ingresar la fecha inicial del objetivo");
			$("#inicio").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#inicio").val() != '') {
			obj.finicio = $("#inicio").val();
		}

		//Validamos el input de la fecha final
		if ($("#final").val() == '') {
			$("#msj_final").html("Debe ingresar la fecha final del objetivo");
			$("#final").css({"border": "1px solid red", "border-radius": "4px"});
			avanza = 0;
		} else if ($("#final").val() != '') {
			obj.ffinal = $("#final").val();
		}

		if ($("#final").val() != '' && $("#inicio").val() != '') {
			if ((new Date(obj.finicio).getTime() > new Date(obj.ffinal).getTime())) {
				$("#msj_inicio").html("La fecha inicial debe ser menor a la final");
				$("#msj_inicio").show();
				$("#inicio").css({"border": "1px solid red", "border-radius": "4px"});
				avanza = 0;
			}
		}
		if (avanza==1) {
			var url = '../ObjetivosController/update/' + $("#idObjetivo").val();
			var dataObjt = JSON.stringify(obj);

			$.ajax({
				url: url,
				dataType: "json",
				data: {
					obj: dataObjt
				},
				type: 'POST',
				success: function (response) {
					if (response != 0) {
						alert("Datos guardados correctamente");
                        window.location='../lista_objetivos';
                    } else if (response == 0) {
                        return false;
                    }
				},
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr() ;
                    xhr.onloadstart = function(e) {
                        $("#fondoLoader").show();
                    };
                    xhr.upload.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(2000);
                    }
                    return xhr ;
                }
			});
		}
	});


	$(".btnFinalizar").click(function () {
		window.location = 'lista_objetivos';
	});

});

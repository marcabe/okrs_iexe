$(document).ready(function () {
	$(".enviarChat").click(function () {
		var idPlan = $(this).attr("text");
        var para = $("#listaLider"+idPlan).val();

        var fechaHoy = $("#fechaHoy").val();
		var usuarioLogueado = $("#usuarioLogueado").val();
		var nombreLogueado = $("#nombreLogueado").val();
		var men = $("#mensajeChat" + idPlan).val();
		var mensaje = '' +
			'<li>' +
			'<div class="col-md-10 mensajeChatDer">' +
			'<label>'+nombreLogueado+'</label>' +
			'<p>' + men + '</p>' +
			'<small class="col-md-12 fechamen text-right">'+fechaHoy+'</small>' +
			'</div>' +
			'</li>';
		$("#chatObj" + idPlan).append(mensaje);
		$("#PlanChat" + idPlan).animate({scrollTop: $('#PlanChat'+idPlan)[0].scrollHeight}, 1000);
		$("#mensajeChat").val('');

		$.ajax({
			url: 'ChatController/insert',
			dataType: "json",
			data: {
				mensaje: men,
				idUsuario: $("#usuarioLogueado").val(),
				tipo: 'objetivo',
                para: para,
                idTipo: idPlan
			},
			type: 'POST',
			success: function (response) {
				$("#mensajeChat" + idPlan).val('')
			}
		});

	});


    $(".listaLider").change(function () {
        var user = $(this).val();
        var i = 0;
        var idObj = $(this).attr("name");
        $("#chatObj" + idObj).empty();
        $.ajax({
            url: "ChatController/getMeParaObj",
            type: "POST",
            data: {
                usuario: user,
                idObj: idObj,
                me: $("#usuarioLogueado").val()
            },
            success: function (response) {
                var dataChat = JSON.parse(response);
                console.log(dataChat);
                for (i = 0; i < dataChat.length; i++) {
                    var mensaje = '' +
                        '<li>' +
                        '<div class="col-md-10 mensajeChatDer">' +
                        '<label>' + dataChat[i].nombre + '(' + dataChat[i].nombrePuesto + ')</label>' +
                        '<p> ' + dataChat[i].mensaje + ' </p>' +
                        '<small class="col-md-12 fechamen text-right">' + dataChat[i].fechahora + '</small>' +
                        '</div>' +
                        '</li>';
                    $("#chatObj" + idObj).append(mensaje);
                }
            }

        });
    });


});

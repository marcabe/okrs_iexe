/*
 Navicat Premium Data Transfer

 Source Server         : Developer
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : okrs_iexe

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 14/03/2019 10:30:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acciones
-- ----------------------------
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones`  (
  `idAcciones` int(11) NOT NULL AUTO_INCREMENT,
  `idKr` int(11) NOT NULL,
  `accion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  `fecha` date NOT NULL,
  `ponderacion` int(255) NOT NULL,
  `avancePonderacion` float(10, 2) NOT NULL,
  PRIMARY KEY (`idAcciones`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acciones
-- ----------------------------
INSERT INTO `acciones` VALUES (41, 78, 'Test', 0.00, b'1', '0000-00-00', 0, 0.00);
INSERT INTO `acciones` VALUES (42, 78, 'Test 1', 0.00, b'1', '0000-00-00', 0, 0.00);
INSERT INTO `acciones` VALUES (43, 79, 'Test 2.2', 100.00, b'1', '2019-03-04', 50, 50.00);
INSERT INTO `acciones` VALUES (44, 78, 'Test 2', 0.00, b'1', '0000-00-00', 0, 0.00);
INSERT INTO `acciones` VALUES (45, 79, 'Test3', 0.00, b'1', '0000-00-00', 30, 0.00);
INSERT INTO `acciones` VALUES (46, 79, 'Test 4', 0.00, b'1', '2019-02-04', 20, 0.00);
INSERT INTO `acciones` VALUES (47, 81, 'Nuevo por marco', 0.00, b'1', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (48, 81, 'Nuevo por antonio', 0.00, b'1', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (49, 87, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,', 0.00, b'0', '2019-05-05', 0, 0.00);
INSERT INTO `acciones` VALUES (54, 87, 'Nuevo por marco', 0.00, b'0', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (55, 87, 'Nuevo 1', 0.00, b'0', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (56, 87, 'Nuevo 2', 0.00, b'0', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (57, 87, 'Nuevo 3', 0.00, b'1', '2019-03-06', 0, 0.00);
INSERT INTO `acciones` VALUES (58, 79, 'Nuevito', 0.00, b'1', '2019-03-13', 0, 0.00);

-- ----------------------------
-- Table structure for acuerdos
-- ----------------------------
DROP TABLE IF EXISTS `acuerdos`;
CREATE TABLE `acuerdos`  (
  `idAcuerdo` int(255) NOT NULL AUTO_INCREMENT,
  `idMInuta` int(255) NOT NULL,
  `actividad` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `responsable` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `fechaAlta` date NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`idAcuerdo`) USING BTREE,
  INDEX `fk_acuerdo_minuta`(`idMInuta`) USING BTREE,
  CONSTRAINT `fk_acuerdo_minuta` FOREIGN KEY (`idMInuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acuerdos
-- ----------------------------
INSERT INTO `acuerdos` VALUES (138, 126, 'Actividad1', 'Marco', '2019-01-01', 100.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (139, 127, '1', '1', '2019-01-01', 100.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (140, 128, 'Actividad 1', '1', '2019-01-01', 35.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (141, 129, 'Actividad 1', 'Marco', '2019-01-01', 30.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (142, 129, 'Actividad 2', 'Antonio', '2019-02-01', 0.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (143, 129, 'Actividad 3', 'Cardona', '2019-03-01', 0.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (144, 129, 'Actividad 4', 'Benitez', '2019-04-01', 0.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (145, 129, 'Actividad 5', 'Marcos', '2019-05-01', 0.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (146, 129, 'Actividad 6', 'jESUS', '2019-01-01', 0.00, '2019-03-11', b'1');
INSERT INTO `acuerdos` VALUES (147, 130, 'Actividad1', 'Marco', '2019-01-01', 100.00, '2019-03-12', b'1');
INSERT INTO `acuerdos` VALUES (148, 130, 'Actividad 2', 'Antonio', '2019-01-02', 0.00, '2019-03-12', b'1');
INSERT INTO `acuerdos` VALUES (149, 130, 'Actividad 3', 'Cardona', '2019-01-03', 0.00, '2019-03-12', b'1');
INSERT INTO `acuerdos` VALUES (150, 130, 'Actividad 4', 'Marco', '2019-01-02', 0.00, '2019-03-12', b'1');
INSERT INTO `acuerdos` VALUES (151, 130, 'Actividad 5', 'mARCO', '2019-01-01', 0.00, '2019-03-12', b'1');

-- ----------------------------
-- Table structure for bitacoraacciones
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacciones`;
CREATE TABLE `bitacoraacciones`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  INDEX `bitaccion_fk_userno`(`userNoAutorizo`) USING BTREE,
  INDEX `bitaccion_fk_userauto`(`userAprobado`) USING BTREE,
  CONSTRAINT `bitaccion_fk_userauto` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitaccion_fk_userno` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraacciones_ibfk_1` FOREIGN KEY (`idKeyResult`) REFERENCES `acciones` (`idAcciones`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraacciones_ibfk_2` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 267 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacciones
-- ----------------------------
INSERT INTO `bitacoraacciones` VALUES (266, 43, 'Test', 0.00, 100.00, 1, '2019-03-13 18:01:00', 'cap1', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraacuerdo
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacuerdo`;
CREATE TABLE `bitacoraacuerdo`  (
  `idBitacoraGral` int(255) NOT NULL AUTO_INCREMENT,
  `idAcuerdo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacoraGral`) USING BTREE,
  INDEX `fk_bitacora_acuerdo`(`idAcuerdo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacuerdo
-- ----------------------------
INSERT INTO `bitacoraacuerdo` VALUES (21, 83, 'Test', 0.00, 10.00, 1, '2019-02-08 16:42:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (22, 94, 'Test', 0.00, 100.00, 1, '2019-02-12 19:56:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (23, 96, 'Test', 0.00, 10.00, 1, '2019-02-12 20:23:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (24, 111, 'Test', 0.00, 50.00, 1, '2019-02-21 23:43:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (25, 108, 'Test', 10.00, 20.00, 1, '2019-02-22 12:59:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (26, 108, 'Test', 20.00, 30.00, 1, '2019-02-22 13:49:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (27, 110, 'Test', 0.00, 10.00, 1, '2019-02-22 13:50:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (28, 125, 'Test', 0.00, 10.00, 1, '2019-03-07 10:51:00', 'lider1', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (29, 125, 'Test', 10.00, 20.00, 1, '2019-03-07 10:54:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (30, 126, 'Test', 0.00, 35.00, 0, '2019-03-07 10:55:00', 'lider1', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraindicadores
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraindicadores`;
CREATE TABLE `bitacoraindicadores`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idIndicador` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idIndicador`) USING BTREE,
  INDEX `bitacoraIndicador_user_fk`(`user`) USING BTREE,
  INDEX `fk_user_aprobado`(`userAprobado`) USING BTREE,
  INDEX `fk_user_noautorizo`(`userNoAutorizo`) USING BTREE,
  CONSTRAINT `bitacoraIndicador_user_fk` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraindicadores_ibfk_1` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_aprobado` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_noautorizo` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraindicadores
-- ----------------------------
INSERT INTO `bitacoraindicadores` VALUES (57, 91, 'Test', 0.00, 55.00, 0, '2019-03-04 16:15:00', 'cap1', 'No me gusto', 'sadmin', 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (58, 92, 'Test', 0.00, 2.00, 3, '2019-03-04 16:15:00', 'cap1', 'asd', 'sadmin', 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (59, 95, 'Test', 0.00, 50.00, 1, '2019-03-06 17:15:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (60, 96, 'Test 30', 0.00, 30.00, 1, '2019-03-06 17:15:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (61, 95, 'Test', 50.00, 75.00, 1, '2019-03-13 12:06:00', 'sadmin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacorakeyresult
-- ----------------------------
DROP TABLE IF EXISTS `bitacorakeyresult`;
CREATE TABLE `bitacorakeyresult`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  INDEX `fk_userNoautorizo_bkr`(`userNoAutorizo`) USING BTREE,
  INDEX `fk_userAutorizo_bkr`(`userAprobado`) USING BTREE,
  CONSTRAINT `fk_bitacora_kr` FOREIGN KEY (`idKeyResult`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_bitacora_kr_user` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_userAutorizo_bkr` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_userNoautorizo_bkr` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 187 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacorakeyresult
-- ----------------------------
INSERT INTO `bitacorakeyresult` VALUES (176, 80, 'tEST', 0.00, 50.00, 1, '2019-02-22 00:23:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (177, 80, 'Tet', 50.00, 75.00, 1, '2019-02-22 00:29:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (178, 80, 'Test', 75.00, 80.00, 1, '2019-02-22 00:30:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (179, 80, 'Test', 80.00, 85.00, 1, '2019-02-22 00:32:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (180, 80, 'Test', 85.00, 87.00, 1, '2019-02-22 00:34:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (181, 80, 'Test', 87.00, 90.00, 1, '2019-02-22 00:39:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (182, 84, 'Test', 0.00, 50.00, 1, '2019-02-22 12:33:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (183, 84, 'Test', 0.00, 50.00, 1, '2019-02-22 12:39:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (184, 84, 'Test', 0.00, 50.00, 1, '2019-02-22 12:39:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (185, 84, 'Test', 0.00, 50.00, 1, '2019-02-22 12:39:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (186, 84, 'Test', 0.00, 50.00, 1, '2019-02-22 12:40:00', 'sadmin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoramodificaciones
-- ----------------------------
DROP TABLE IF EXISTS `bitacoramodificaciones`;
CREATE TABLE `bitacoramodificaciones`  (
  `idModificacion` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacora` int(255) NOT NULL,
  `user` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avanceModificado` float(10, 2) NOT NULL,
  `nuevoAvance` float(10, 2) NOT NULL,
  `motivo` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time(6) NOT NULL,
  PRIMARY KEY (`idModificacion`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoramodificaciones
-- ----------------------------
INSERT INTO `bitacoramodificaciones` VALUES (16, 237, 'sadmin', 60.00, 40.00, 'El valor era menor\n                                ', '2019-03-05', '17:18:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (17, 238, 'sadmin', 100.00, 80.00, '\n               Test                 ', '2019-03-05', '17:20:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (18, 241, 'sadmin', 90.00, 0.00, '\n                                ', '2019-03-06', '17:54:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (19, 241, 'sadmin', 0.00, 0.00, '\n                                ', '2019-03-06', '17:54:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (20, 241, 'sadmin', 0.00, 1.00, '\n             Test                   ', '2019-03-06', '17:54:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (21, 242, 'sadmin', 10.00, 25.00, 'Test', '2019-03-06', '19:11:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (22, 266, 'sadmin', 50.00, 100.00, 'Motvio', '2019-03-13', '18:02:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (23, 266, 'sadmin', 50.00, 100.00, 'Test', '2019-03-13', '18:09:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (24, 266, 'sadmin', 100.00, 100.00, 'Test', '2019-03-13', '18:16:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (25, 266, 'sadmin', 50.00, 100.00, 'Test', '2019-03-13', '18:17:00.000000');
INSERT INTO `bitacoramodificaciones` VALUES (26, 266, 'sadmin', 50.00, 100.00, 'Test', '2019-03-13', '18:23:00.000000');

-- ----------------------------
-- Table structure for bitacoramovimientos
-- ----------------------------
DROP TABLE IF EXISTS `bitacoramovimientos`;
CREATE TABLE `bitacoramovimientos`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `movimiento` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time(0) NOT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_bitacora_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 218 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoramovimientos
-- ----------------------------
INSERT INTO `bitacoramovimientos` VALUES (173, 'Alta de usuario: lider1', 'sadmin', '2019-02-14', '16:21:00');
INSERT INTO `bitacoramovimientos` VALUES (174, 'Edicio de usuario: cap1', 'sadmin', '2019-02-15', '17:13:00');
INSERT INTO `bitacoramovimientos` VALUES (175, 'Alta de nuevo plan', 'sadmin', '2019-02-18', '10:29:00');
INSERT INTO `bitacoramovimientos` VALUES (176, 'Alta de objetivo', 'sadmin', '2019-02-19', '01:10:00');
INSERT INTO `bitacoramovimientos` VALUES (177, 'Edicio de usuario: cap1', 'sadmin', '2019-02-20', '21:47:00');
INSERT INTO `bitacoramovimientos` VALUES (178, 'Alta de nuevo plan', 'sadmin', '2019-02-22', '16:32:00');
INSERT INTO `bitacoramovimientos` VALUES (179, 'Alta de objetivo', 'sadmin', '2019-02-23', '00:24:00');
INSERT INTO `bitacoramovimientos` VALUES (180, 'Alta de nuevo plan', 'sadmin', '2019-02-26', '12:40:00');
INSERT INTO `bitacoramovimientos` VALUES (181, 'Alta de objetivo', 'sadmin', '2019-02-26', '22:09:00');
INSERT INTO `bitacoramovimientos` VALUES (182, 'Alta de usuario', 'sadmin', '2019-02-26', '15:59:00');
INSERT INTO `bitacoramovimientos` VALUES (183, 'Alta de usuario', 'sadmin', '2019-02-26', '16:03:00');
INSERT INTO `bitacoramovimientos` VALUES (184, 'Alta de usuario: acuerdo1', 'sadmin', '2019-02-26', '16:48:00');
INSERT INTO `bitacoramovimientos` VALUES (185, 'Alta de usuario: acuerdo1', 'sadmin', '2019-02-26', '16:53:00');
INSERT INTO `bitacoramovimientos` VALUES (186, 'Edicio de usuario: acuerdo1', 'sadmin', '2019-02-26', '17:09:00');
INSERT INTO `bitacoramovimientos` VALUES (187, 'Edicio de usuario: acuerdo1', 'sadmin', '2019-02-26', '17:10:00');
INSERT INTO `bitacoramovimientos` VALUES (188, 'Alta de usuario: acuerdo2', 'sadmin', '2019-02-26', '17:24:00');
INSERT INTO `bitacoramovimientos` VALUES (189, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '15:17:00');
INSERT INTO `bitacoramovimientos` VALUES (190, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '15:20:00');
INSERT INTO `bitacoramovimientos` VALUES (191, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:32:00');
INSERT INTO `bitacoramovimientos` VALUES (192, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:33:00');
INSERT INTO `bitacoramovimientos` VALUES (193, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:33:00');
INSERT INTO `bitacoramovimientos` VALUES (194, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:35:00');
INSERT INTO `bitacoramovimientos` VALUES (195, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:37:00');
INSERT INTO `bitacoramovimientos` VALUES (196, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:38:00');
INSERT INTO `bitacoramovimientos` VALUES (197, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:38:00');
INSERT INTO `bitacoramovimientos` VALUES (198, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:38:00');
INSERT INTO `bitacoramovimientos` VALUES (199, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:39:00');
INSERT INTO `bitacoramovimientos` VALUES (200, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:39:00');
INSERT INTO `bitacoramovimientos` VALUES (201, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:40:00');
INSERT INTO `bitacoramovimientos` VALUES (202, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:40:00');
INSERT INTO `bitacoramovimientos` VALUES (203, 'Alta de nuevo plan', 'sadmin', '2019-02-28', '16:55:00');
INSERT INTO `bitacoramovimientos` VALUES (204, 'Alta de objetivo', 'sadmin', '2019-03-01', '00:04:00');
INSERT INTO `bitacoramovimientos` VALUES (205, 'Edicio de usuario: cap1', 'sadmin', '2019-03-01', '13:16:00');
INSERT INTO `bitacoramovimientos` VALUES (206, 'Alta de nuevo plan', 'sadmin', '2019-03-06', '11:21:00');
INSERT INTO `bitacoramovimientos` VALUES (207, 'Alta de nuevo plan', 'sadmin', '2019-03-06', '17:03:00');
INSERT INTO `bitacoramovimientos` VALUES (208, 'Alta de objetivo', 'sadmin', '2019-03-07', '00:16:00');
INSERT INTO `bitacoramovimientos` VALUES (209, 'Alta de usuario', 'sadmin', '2019-03-06', '17:16:00');
INSERT INTO `bitacoramovimientos` VALUES (210, 'Edicio de usuario: lider1', 'sadmin', '2019-03-07', '10:51:00');
INSERT INTO `bitacoramovimientos` VALUES (211, 'Edicio de usuario: cap1', 'sadmin', '2019-03-07', '13:57:00');
INSERT INTO `bitacoramovimientos` VALUES (212, 'Alta de usuario', 'sadmin', '2019-03-13', '18:41:00');
INSERT INTO `bitacoramovimientos` VALUES (213, 'Alta de usuario', 'sadmin', '2019-03-13', '18:47:00');
INSERT INTO `bitacoramovimientos` VALUES (214, 'Alta de usuario', 'sadmin', '2019-03-13', '18:53:00');
INSERT INTO `bitacoramovimientos` VALUES (215, 'Alta de usuario', 'sadmin', '2019-03-13', '18:55:00');
INSERT INTO `bitacoramovimientos` VALUES (216, 'Alta de usuario', 'sadmin', '2019-03-13', '18:58:00');
INSERT INTO `bitacoramovimientos` VALUES (217, 'Alta de usuario', 'sadmin', '2019-03-13', '19:01:00');

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `idChat` int(255) NOT NULL AUTO_INCREMENT,
  `mensaje` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idUsuario` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idTipo` int(255) NOT NULL,
  `fechahora` datetime(0) NOT NULL,
  `status` bit(1) NOT NULL,
  `de` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `para` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idChat`) USING BTREE,
  INDEX `fk_chat_usuario`(`idUsuario`) USING BTREE,
  CONSTRAINT `fk_chat_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 223 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (193, 'Hola Marco', 'gob', 'plan', 9, '2019-02-15 11:48:00', b'0', 'gob', 'sadmin');
INSERT INTO `chat` VALUES (194, 'Hola Daniel', 'gob', 'plan', 9, '2019-02-15 11:48:00', b'0', 'gob', 'sadmin1');
INSERT INTO `chat` VALUES (195, 'Hola Eduardo', 'gob', 'plan', 9, '2019-02-15 11:48:00', b'0', 'gob', 'sadmin2');
INSERT INTO `chat` VALUES (196, 'Hola Armando', 'sadmin', 'plan', 9, '2019-02-15 11:50:00', b'0', 'sadmin', 'lider1');
INSERT INTO `chat` VALUES (197, 'Hola buen dia', 'sadmin', 'plan', 9, '2019-02-15 11:50:00', b'0', 'sadmin', 'gob');
INSERT INTO `chat` VALUES (198, 'Hola Armando', 'cap1', 'plan', 9, '2019-02-15 11:57:00', b'0', 'cap1', 'lider1');
INSERT INTO `chat` VALUES (199, 'Hola Angel', 'lider1', 'plan', 9, '2019-02-15 11:57:00', b'0', 'lider1', 'cap1');
INSERT INTO `chat` VALUES (208, 'Hola Daniel', 'gob', 'objetivo', 62, '2019-02-15 13:54:00', b'0', 'gob', 'sadmin1');
INSERT INTO `chat` VALUES (209, 'Hola Eduardo', 'gob', 'objetivo', 62, '2019-02-15 13:57:00', b'0', 'gob', 'sadmin2');
INSERT INTO `chat` VALUES (210, 'Hola Marco', 'gob', 'objetivo', 62, '2019-02-15 14:01:00', b'0', 'gob', 'sadmin');
INSERT INTO `chat` VALUES (215, 'Hola desde objetivo de prueba', 'sadmin', 'objetivo', 62, '2019-02-15 15:56:00', b'0', 'sadmin', 'gob');
INSERT INTO `chat` VALUES (216, 'Hola Armando desde Objetivo de prueba', 'sadmin', 'objetivo', 62, '2019-02-15 15:56:00', b'0', 'sadmin', 'lider1');
INSERT INTO `chat` VALUES (217, 'Hola Gob desde prueba marco', 'sadmin', 'objetivo', 63, '2019-02-15 15:57:00', b'1', 'sadmin', 'gob');
INSERT INTO `chat` VALUES (218, 'Hola Armando desde prueba marco', 'sadmin', 'objetivo', 63, '2019-02-15 15:57:00', b'1', 'sadmin', 'lider1');
INSERT INTO `chat` VALUES (219, 'Hola Jesus desde objetivo de prueba', 'lider1', 'objetivo', 62, '2019-02-15 17:12:00', b'0', 'lider1', 'cap1');
INSERT INTO `chat` VALUES (220, 'Hola Marco respuesta desde obj de prueba', 'lider1', 'objetivo', 62, '2019-02-15 17:12:00', b'0', 'lider1', 'sadmin');
INSERT INTO `chat` VALUES (221, 'Hola', 'sadmin', 'plan', 9, '2019-02-21 16:12:00', b'1', 'sadmin', 'gob');
INSERT INTO `chat` VALUES (222, 'Hola Armando', 'sadmin', 'plan', 10, '2019-02-21 16:44:00', b'1', 'sadmin', 'lider1');

-- ----------------------------
-- Table structure for indicadores
-- ----------------------------
DROP TABLE IF EXISTS `indicadores`;
CREATE TABLE `indicadores`  (
  `idIndicadores` int(255) NOT NULL AUTO_INCREMENT,
  `nombreIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `idPlan` int(255) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idIndicadores`) USING BTREE,
  INDEX `fk_indicador_plan`(`idPlan`) USING BTREE,
  CONSTRAINT `fk_indicador_plan` FOREIGN KEY (`idPlan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicadores
-- ----------------------------
INSERT INTO `indicadores` VALUES (91, 'Financiero', 50.00, 100.00, 0.00, 29, b'1', 0.00);
INSERT INTO `indicadores` VALUES (92, 'Físico', 1.00, 100.00, 0.00, 29, b'1', 0.00);
INSERT INTO `indicadores` VALUES (93, 'Financiero', 0.00, 300.00, 0.00, 30, b'1', 0.00);
INSERT INTO `indicadores` VALUES (94, 'Físico', 1.00, 100.00, 0.00, 30, b'1', 0.00);
INSERT INTO `indicadores` VALUES (95, 'Financiero', 0.00, 100.00, 75.00, 31, b'1', 75.00);
INSERT INTO `indicadores` VALUES (96, 'Físico', 0.00, 350.00, 30.00, 31, b'1', 8.57);

-- ----------------------------
-- Table structure for indicadores_temp
-- ----------------------------
DROP TABLE IF EXISTS `indicadores_temp`;
CREATE TABLE `indicadores_temp`  (
  `idIndicadores` int(255) NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `status` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for indicadorespdfs
-- ----------------------------
DROP TABLE IF EXISTS `indicadorespdfs`;
CREATE TABLE `indicadorespdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraIndicador` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idIndicador` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraIndicador`) USING BTREE,
  INDEX `fk_pdf_kr`(`idIndicador`) USING BTREE,
  CONSTRAINT `indicadorespdfs_idBitacoraIndicador` FOREIGN KEY (`idBitacoraIndicador`) REFERENCES `bitacoraindicadores` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `indicadorespdfs_idIndicador` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for keyresult
-- ----------------------------
DROP TABLE IF EXISTS `keyresult`;
CREATE TABLE `keyresult`  (
  `idKeyResult` int(255) NOT NULL AUTO_INCREMENT,
  `idObjetivo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metrica` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `orden` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `medicioncomienzo` float(10, 2) NOT NULL,
  `medicionfinal` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idKeyResult`) USING BTREE,
  INDEX `kf_kr_objetivos`(`idObjetivo`) USING BTREE,
  CONSTRAINT `kf_kr_objetivos` FOREIGN KEY (`idObjetivo`) REFERENCES `objetivos` (`idObjetivo`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of keyresult
-- ----------------------------
INSERT INTO `keyresult` VALUES (78, 62, 'Kr de prueba', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '0000-00-00');
INSERT INTO `keyresult` VALUES (79, 62, 'Key resutl test pruebas', 'Porcentaje', 'Ascendente', 1.00, 100.00, 25.00, b'1', 25.00, '2019-02-28');
INSERT INTO `keyresult` VALUES (80, 63, 'Nueva', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00, '0000-00-00');
INSERT INTO `keyresult` VALUES (81, 62, 'Nueva', 'Financiero', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00, '2019-02-26');
INSERT INTO `keyresult` VALUES (82, 65, 'as', 'Dicotomica', 's/o', 1.00, 100.00, 0.00, b'1', 0.00, '0000-00-00');
INSERT INTO `keyresult` VALUES (83, 66, 'Hola', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00, '0000-00-00');
INSERT INTO `keyresult` VALUES (84, 63, '1', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00, '0000-00-00');
INSERT INTO `keyresult` VALUES (85, 62, 'Okr Bimestral editado', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00, '2019-02-26');
INSERT INTO `keyresult` VALUES (86, 62, 'Nuevo', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-02-26');
INSERT INTO `keyresult` VALUES (87, 70, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis nat', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00, '2019-03-06');
INSERT INTO `keyresult` VALUES (92, 62, 'Nuevecito', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-03-13');
INSERT INTO `keyresult` VALUES (93, 62, 'Nuevecito', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-03-13');
INSERT INTO `keyresult` VALUES (94, 62, 'Nuevecito', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-03-13');
INSERT INTO `keyresult` VALUES (95, 62, 'Nuevecito', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-03-13');
INSERT INTO `keyresult` VALUES (96, 62, 'Nuevecito', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'0', 0.00, '2019-03-13');

-- ----------------------------
-- Table structure for krpdfs
-- ----------------------------
DROP TABLE IF EXISTS `krpdfs`;
CREATE TABLE `krpdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraKr` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idKr` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraKr`) USING BTREE,
  INDEX `fk_pdf_kr`(`idKr`) USING BTREE,
  CONSTRAINT `fk_pdf_bitacora_kr` FOREIGN KEY (`idBitacoraKr`) REFERENCES `bitacorakeyresult` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pdf_kr` FOREIGN KEY (`idKr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for minuta
-- ----------------------------
DROP TABLE IF EXISTS `minuta`;
CREATE TABLE `minuta`  (
  `idMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `idPlan` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time(0) NOT NULL,
  `codigo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `objetivo` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asunto` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  `visible` bit(1) NOT NULL,
  PRIMARY KEY (`idMinuta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 131 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of minuta
-- ----------------------------
INSERT INTO `minuta` VALUES (100, 9, '0000-00-00', '09:22:00', '', '1', '1', '1', 0.00, b'0', 0.00, b'1');
INSERT INTO `minuta` VALUES (104, 9, '0000-00-00', '01:05:00', 'xyz', '1', '1', 'asd', 0.00, b'0', 0.00, b'1');
INSERT INTO `minuta` VALUES (112, 10, '2019-02-28', '11:58:00', 'ccc28/02/2019', 'Puebla', 'Pruebilla', 'asd', 0.00, b'1', 0.00, b'1');
INSERT INTO `minuta` VALUES (120, 10, '2019-02-01', '11:39:00', 'ccc', 'Puebla', '1', '1ssss', 0.00, b'1', 0.00, b'1');
INSERT INTO `minuta` VALUES (121, 14, '2019-02-05', '05:24:00', '20071921205/02/2019', 'Puebla', 'a', 'a', 0.00, b'1', 0.00, b'1');
INSERT INTO `minuta` VALUES (126, 9, '2019-01-01', '10:40:00', 'xyzasdasdasdasd', 'Puebla', 'Objetivo acuerdo de prueba', 'Asunto de prueba', 0.00, b'1', 100.00, b'0');
INSERT INTO `minuta` VALUES (127, 9, '2019-01-01', '10:43:00', 'xyzasdasdasdasd', 'Puebla', 'Objetivo acuerdo de prueba', 'Asunto de prueba', 0.00, b'1', 100.00, b'0');
INSERT INTO `minuta` VALUES (128, 9, '2019-01-01', '10:45:00', 'xyzasdasdasdasd', '1', '1', '', 0.00, b'1', 35.00, b'0');
INSERT INTO `minuta` VALUES (129, 9, '2019-01-01', '10:46:00', 'xyzasdasdasdasd', 'Puebla', 'Plan de base ', 'Plan de base', 0.00, b'1', 5.00, b'0');
INSERT INTO `minuta` VALUES (130, 9, '2019-01-01', '11:22:00', 'mimamamemima', 'Puebla', 'Objetivo acuerdo de prueba', 'Asunto de prueba', 0.00, b'1', 20.00, b'0');

-- ----------------------------
-- Table structure for objetivos
-- ----------------------------
DROP TABLE IF EXISTS `objetivos`;
CREATE TABLE `objetivos`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `anual` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `fk_objetivos_mv` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objetivos
-- ----------------------------
INSERT INTO `objetivos` VALUES (62, '1', 9, 'Objetivo de prueba', 'Descripcion de prueba', '2019-02-05', '2019-02-28', 25.00, b'1', 8.33);
INSERT INTO `objetivos` VALUES (63, '0', 10, 'De prueba marco', 'Objetivo de prueba', '2019-02-19', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (64, '0', 10, 'De prueba marco1', 'Objetivo de prueba', '2019-02-19', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (65, '0', 10, 'Obj', 'Obj', '2019-02-04', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (66, '0', 9, 'Indicador financiero', 'Financiero', '2019-02-18', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (67, '0', 9, 'asd', 'asd', '2019-02-04', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (68, '1', 10, '1', '1', '2019-02-26', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (69, '1', 29, 'Objetivo de prueba', 'Objetido de prueba', '2019-02-04', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (70, '1', 31, 'Objetivo test', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,', '2019-03-01', '2019-03-31', 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for objetivos_temp
-- ----------------------------
DROP TABLE IF EXISTS `objetivos_temp`;
CREATE TABLE `objetivos_temp`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `objetivos_temp_ibfk_1` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pdf_plan
-- ----------------------------
DROP TABLE IF EXISTS `pdf_plan`;
CREATE TABLE `pdf_plan`  (
  `idPdfPlan` int(255) NOT NULL AUTO_INCREMENT,
  `idPlan` int(255) NOT NULL,
  `pdf` varchar(3000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`idPdfPlan`) USING BTREE,
  INDEX `fk_plan_pdf`(`idPlan`) USING BTREE,
  CONSTRAINT `fk_plan_pdf` FOREIGN KEY (`idPlan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pdf_plan
-- ----------------------------
INSERT INTO `pdf_plan` VALUES (1, 9, '(ranma one-half) akane tendo2019-02-28.png');
INSERT INTO `pdf_plan` VALUES (2, 9, 'file2019-02-28.pdf');
INSERT INTO `pdf_plan` VALUES (3, 9, 'bit12019-03-01.png');

-- ----------------------------
-- Table structure for pdfminutas
-- ----------------------------
DROP TABLE IF EXISTS `pdfminutas`;
CREATE TABLE `pdfminutas`  (
  `idPdfMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `pdf` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idMinuta` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idPdfMinuta`) USING BTREE,
  INDEX `fk_pdf_minuta`(`idMinuta`) USING BTREE,
  CONSTRAINT `fk_pdf_minuta` FOREIGN KEY (`idMinuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pdfminutas
-- ----------------------------
INSERT INTO `pdfminutas` VALUES (17, '190117 minuta reuniorn de gabinete ampliado2019-02-202019-02-21.docx', 104);

-- ----------------------------
-- Table structure for plan
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avanceIndicadores` float(10, 2) NOT NULL,
  `idReuniones` int(255) NOT NULL,
  `visible` bit(1) NOT NULL,
  `icono` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `beneficio` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `diagnostico` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `iestrategico` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metodoiestrategico` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `d_inversion2018` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `d_inversion2019` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE,
  INDEX `idReuniones_proyectos`(`idReuniones`) USING BTREE,
  CONSTRAINT `idReuniones_proyectos` FOREIGN KEY (`idReuniones`) REFERENCES `reuniones` (`idReunion`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO `plan` VALUES (9, 'Plan de prueba', 'xyzasdasdasdasd', 'Activo', 'Marco', '10000', 'Prueba', '1000', 'Proposito del plan', 'Población objetiva del plan', '2005', '5', '5', '5', '5', '5', '0000-00-00', '0000-00-00', 8.33, b'1', 8.33, 1, b'1', '', '', 'Hola', '1', '2', '3', '4');
INSERT INTO `plan` VALUES (10, 'COPIA', 'ccc', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00, 1, b'1', '', '', 'Ya estuvo', '', '', '', '');
INSERT INTO `plan` VALUES (11, 'Uno', NULL, 'Uno', 'Uno', 'Uno', 'Uno', 'v', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00, 2, b'1', '', '', 'Hol', '', '', '', '');
INSERT INTO `plan` VALUES (12, 'Dos', NULL, 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', '1', '1', '1', '1', '1', '1', '2019-02-28', '2019-03-27', 0.00, b'1', 0.00, 4, b'1', '', '', 'Hol', '', '', '', '');
INSERT INTO `plan` VALUES (14, 'Q', '200719212', 'q', 'q', 'q', 'q', 'q', 'q', 'q', '1', '2', '3', '4', '5', '6', '2019-02-28', '2019-03-03', 0.00, b'1', 0.00, 1, b'0', '', 'q', 'q', '', '', '', '');
INSERT INTO `plan` VALUES (29, 'Proyecto de prueba Marco', '200719212', 'Activo', 'Marco Antonio ', '200000', 'Probar', '1', 'Probar el modulo', '10', '1000', '2000', '3000', '4000', '5000', '6000', '2019-02-28', '2019-03-02', 0.00, b'1', 0.00, 1, b'0', '', 'Probarlo', 'Bueno', 'Nuevo indicador', 'Metodo unico', '1000', '2000');
INSERT INTO `plan` VALUES (30, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2019-03-27', '2019-03-31', 0.00, b'1', 0.00, 1, b'0', '', '1', '1', '1', '1', '1', '1');
INSERT INTO `plan` VALUES (31, 'Proyecto Test', 'P0918', 'Test', 'Test', '100000', 'Test', 'Test', 'Test', 'Test', '1000', '100', 'Indicador', '10000', '1000', 'Meta indicador', '2019-03-01', '2019-03-31', 0.00, b'1', 0.00, 1, b'1', '', 'Beneficios', 'Diagnóstico', 'Indicador estratégico', 'Método del indicador estratégico', '100000', '10000');

-- ----------------------------
-- Table structure for plan_temp
-- ----------------------------
DROP TABLE IF EXISTS `plan_temp`;
CREATE TABLE `plan_temp`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for reuniones
-- ----------------------------
DROP TABLE IF EXISTS `reuniones`;
CREATE TABLE `reuniones`  (
  `idReunion` int(255) NOT NULL AUTO_INCREMENT,
  `reunion` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`idReunion`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of reuniones
-- ----------------------------
INSERT INTO `reuniones` VALUES (1, 'Mesa de gabinete');
INSERT INTO `reuniones` VALUES (2, 'Mesa de proyectos estratégicos');
INSERT INTO `reuniones` VALUES (3, 'Mesas sectoriales');
INSERT INTO `reuniones` VALUES (4, 'Mesas transversales');

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol`  (
  `idRol` int(255) NOT NULL,
  `rol` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombrePuesto` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idRol`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES (1, 'lider', 'Gobernador');
INSERT INTO `rol` VALUES (2, 'sadmin', 'Superadmin');
INSERT INTO `rol` VALUES (3, 'admin', 'Lider');
INSERT INTO `rol` VALUES (4, 'capturista', 'Enlace');
INSERT INTO `rol` VALUES (5, 'acuerdo', 'Acuerdos');

-- ----------------------------
-- Table structure for sesiones
-- ----------------------------
DROP TABLE IF EXISTS `sesiones`;
CREATE TABLE `sesiones`  (
  `idSesiones` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idSesiones`) USING BTREE,
  INDEX `fk_sesiones_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_sesiones_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1129 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sesiones
-- ----------------------------
INSERT INTO `sesiones` VALUES (727, 'sadmin', '2019-02-14 15:27:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (728, 'sadmin1', '2019-02-14 15:28:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (729, 'gob', '2019-02-14 15:34:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (730, 'gob', '2019-02-14 15:36:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (731, 'sadmin', '2019-02-14 15:37:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (732, 'gob', '2019-02-14 15:37:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (733, 'sadmin1', '2019-02-14 15:39:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (734, 'sadmin', '2019-02-14 15:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (735, 'sadmin1', '2019-02-14 15:42:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (736, 'sadmin1', '2019-02-14 15:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (737, 'gob', '2019-02-14 15:48:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (738, 'sadmin', '2019-02-14 15:54:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (739, 'gob', '2019-02-14 15:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (740, 'sadmin', '2019-02-14 16:05:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (741, 'sadmin1', '2019-02-14 16:07:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (742, 'sadmin2', '2019-02-14 16:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (743, 'gob', '2019-02-14 16:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (744, 'sadmin2', '2019-02-14 16:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (745, 'sadmin', '2019-02-14 16:09:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (746, 'gob', '2019-02-14 16:10:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (747, 'sadmin', '2019-02-14 16:11:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (748, 'sadmin1', '2019-02-14 16:11:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (749, 'gob', '2019-02-14 16:11:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (750, 'sadmin', '2019-02-14 16:12:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (751, 'gob', '2019-02-14 16:12:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (752, 'sadmin', '2019-02-14 16:15:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (753, 'gob', '2019-02-14 18:14:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (754, 'sadmin', '2019-02-14 18:19:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (755, 'gob', '2019-02-14 20:09:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (756, 'sadmin', '2019-02-14 20:11:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (757, 'gob', '2019-02-14 21:04:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (758, 'sadmin', '2019-02-14 21:09:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (759, 'gob', '2019-02-14 21:10:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (760, 'sadmin1', '2019-02-14 21:19:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (761, 'gob', '2019-02-14 21:20:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (762, 'sadmin', '2019-02-14 21:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (763, 'lider1', '2019-02-14 21:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (764, 'gob', '2019-02-14 21:25:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (765, 'sadmin', '2019-02-14 21:26:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (766, 'sadmin2', '2019-02-14 21:26:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (767, 'gob', '2019-02-14 21:26:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (768, 'sadmin1', '2019-02-14 21:27:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (769, 'lider1', '2019-02-14 21:53:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (770, 'sadmin', '2019-02-15 09:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (771, 'lider1', '2019-02-15 09:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (772, 'sadmin', '2019-02-15 10:08:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (773, 'sadmin', '2019-02-15 11:05:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (774, 'cap1', '2019-02-15 11:07:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (775, 'sadmin', '2019-02-15 11:13:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (776, 'lider1', '2019-02-15 11:13:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (777, 'sadmin1', '2019-02-15 11:37:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (778, 'gob', '2019-02-15 11:45:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (779, 'sadmin', '2019-02-15 11:48:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (780, 'gob', '2019-02-15 11:50:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (781, 'lider1', '2019-02-15 11:56:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (782, 'cap1', '2019-02-15 11:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (783, 'lider1', '2019-02-15 11:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (784, 'gob', '2019-02-15 11:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (785, 'gob', '2019-02-15 11:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (786, 'cap1', '2019-02-15 11:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (787, 'gob', '2019-02-15 12:06:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (788, 'sadmin', '2019-02-15 12:27:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (789, 'gob', '2019-02-15 12:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (790, 'gob', '2019-02-15 12:45:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (791, 'gob', '2019-02-15 12:50:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (792, 'gob', '2019-02-15 12:51:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (793, 'gob', '2019-02-15 12:54:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (794, 'gob', '2019-02-15 12:57:00', 'Inicio de Sesion', '172.16.2.45');
INSERT INTO `sesiones` VALUES (795, 'gob', '2019-02-15 12:57:00', 'Inicio de Sesion', '172.16.2.23');
INSERT INTO `sesiones` VALUES (796, 'gob', '2019-02-15 12:57:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (797, 'gob', '2019-02-15 12:57:00', 'Inicio de Sesion', '172.16.2.45');
INSERT INTO `sesiones` VALUES (798, 'gob', '2019-02-15 12:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (799, 'sadmin', '2019-02-15 13:28:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (800, 'gob', '2019-02-15 13:31:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (801, 'sadmin', '2019-02-15 14:03:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (802, 'gob', '2019-02-15 15:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (803, 'sadmin', '2019-02-15 15:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (804, 'lider1', '2019-02-15 15:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (805, 'sadmin', '2019-02-15 16:00:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (806, 'lider1', '2019-02-15 16:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (807, 'sadmin', '2019-02-15 16:03:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (808, 'lider1', '2019-02-15 16:07:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (809, 'sadmin', '2019-02-15 16:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (810, 'lider1', '2019-02-15 16:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (811, 'sadmin', '2019-02-15 16:33:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (812, 'sadmin', '2019-02-15 16:34:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (813, 'sadmin', '2019-02-15 17:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (814, 'cap1', '2019-02-15 17:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (815, 'sadmin', '2019-02-15 17:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (816, 'cap1', '2019-02-15 17:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (817, 'gob', '2019-02-15 17:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (818, 'cap1', '2019-02-15 17:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (819, 'lider1', '2019-02-15 17:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (820, 'gob', '2019-02-15 17:42:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (821, 'sadmin', '2019-02-15 17:56:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (822, 'gob', '2019-02-15 18:03:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (823, 'gob', '2019-02-15 18:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (824, 'sadmin', '2019-02-15 18:41:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (825, 'sadmin', '2019-02-18 09:55:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (826, 'gob', '2019-02-18 09:58:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (827, 'sadmin', '2019-02-18 09:58:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (828, 'gob', '2019-02-18 10:06:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (829, 'sadmin', '2019-02-18 10:06:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (830, 'gob', '2019-02-18 10:07:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (831, 'sadmin', '2019-02-18 10:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (832, 'gob', '2019-02-18 12:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (833, 'sadmin', '2019-02-18 13:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (834, 'gob', '2019-02-18 13:05:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (835, 'gob', '2019-02-18 13:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (836, 'gob', '2019-02-18 15:50:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (837, 'sadmin', '2019-02-18 16:00:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (838, 'gob', '2019-02-18 16:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (839, 'sadmin', '2019-02-18 17:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (840, 'gob', '2019-02-18 17:44:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (841, 'sadmin', '2019-02-18 17:45:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (842, 'gob', '2019-02-18 17:48:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (843, 'sadmin', '2019-02-18 17:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (844, 'lider1', '2019-02-18 18:45:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (845, 'cap1', '2019-02-18 18:46:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (846, 'lider1', '2019-02-18 18:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (847, 'gob', '2019-02-18 19:33:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (848, 'gob', '2019-02-18 19:35:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (849, 'gob', '2019-02-18 19:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (850, 'sadmin', '2019-02-18 20:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (851, 'gob', '2019-02-19 09:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (852, 'gob', '2019-02-19 10:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (853, 'sadmin', '2019-02-19 10:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (854, 'gob', '2019-02-19 10:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (855, 'sadmin', '2019-02-19 12:45:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (856, 'gob', '2019-02-19 13:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (857, 'gob', '2019-02-19 16:09:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (858, 'sadmin', '2019-02-19 20:40:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (859, 'lider1', '2019-02-19 21:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (860, 'sadmin', '2019-02-19 21:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (861, 'gob', '2019-02-19 21:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (862, 'sadmin', '2019-02-19 21:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (863, 'gob', '2019-02-19 21:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (864, 'sadmin', '2019-02-19 22:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (865, 'gob', '2019-02-19 22:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (866, 'sadmin', '2019-02-19 22:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (867, 'sadmin', '2019-02-19 22:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (868, 'sadmin', '2019-02-19 22:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (869, 'gob', '2019-02-19 22:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (870, 'sadmin', '2019-02-19 22:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (871, 'gob', '2019-02-19 22:35:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (872, 'sadmin', '2019-02-19 22:53:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (873, 'sadmin', '2019-02-20 10:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (874, 'cap1', '2019-02-20 12:05:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (875, 'sadmin', '2019-02-20 12:35:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (876, 'gob', '2019-02-20 13:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (877, 'sadmin', '2019-02-20 13:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (878, 'gob', '2019-02-20 13:46:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (879, 'sadmin', '2019-02-20 13:58:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (880, 'cap1', '2019-02-20 13:58:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (881, 'sadmin', '2019-02-20 13:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (882, 'cap1', '2019-02-20 13:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (883, 'lider1', '2019-02-20 14:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (884, 'cap1', '2019-02-20 14:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (885, 'lider1', '2019-02-20 14:05:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (886, 'sadmin', '2019-02-20 14:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (887, 'lider1', '2019-02-20 14:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (888, 'sadmin', '2019-02-20 14:48:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (889, 'cap1', '2019-02-20 15:07:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (890, 'sadmin', '2019-02-20 15:07:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (891, 'cap1', '2019-02-20 15:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (892, 'sadmin', '2019-02-20 15:09:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (893, 'cap1', '2019-02-20 16:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (894, 'sadmin', '2019-02-20 16:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (895, 'gob', '2019-02-20 16:20:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (896, 'sadmin', '2019-02-20 18:10:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (897, 'lider1', '2019-02-20 18:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (898, 'gob', '2019-02-20 18:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (899, 'sadmin', '2019-02-20 19:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (900, 'cap1', '2019-02-20 19:29:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (901, 'cap1', '2019-02-20 20:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (902, 'cap1', '2019-02-20 20:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (903, 'cap1', '2019-02-20 20:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (904, 'cap1', '2019-02-20 20:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (905, 'cap1', '2019-02-20 20:32:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (906, 'lider1', '2019-02-20 21:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (907, 'sadmin', '2019-02-20 21:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (908, 'sadmin', '2019-02-20 21:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (909, 'sadmin', '2019-02-20 21:44:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (910, 'cap1', '2019-02-20 22:06:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (911, 'sadmin', '2019-02-20 22:10:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (912, 'cap1', '2019-02-20 22:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (913, 'sadmin', '2019-02-20 22:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (914, 'cap1', '2019-02-20 22:48:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (915, 'sadmin', '2019-02-20 22:48:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (916, 'sadmin', '2019-02-21 10:52:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (917, 'sadmin', '2019-02-21 12:52:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (918, 'sadmin', '2019-02-21 12:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (919, 'sadmin', '2019-02-21 16:10:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (920, 'lider1', '2019-02-21 18:37:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (921, 'cap1', '2019-02-21 18:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (922, 'sadmin', '2019-02-21 18:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (923, 'cap1', '2019-02-21 19:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (924, 'sadmin', '2019-02-21 19:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (925, 'gob', '2019-02-21 19:33:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (926, 'lider1', '2019-02-21 23:40:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (927, 'sadmin', '2019-02-21 23:42:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (928, 'cap1', '2019-02-22 00:02:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (929, 'sadmin', '2019-02-22 00:03:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (930, 'cap1', '2019-02-22 00:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (931, 'cap1', '2019-02-22 00:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (932, 'sadmin', '2019-02-22 00:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (933, 'cap1', '2019-02-22 00:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (934, 'sadmin', '2019-02-22 00:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (935, 'cap1', '2019-02-22 00:29:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (936, 'sadmin', '2019-02-22 00:29:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (937, 'cap1', '2019-02-22 00:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (938, 'sadmin', '2019-02-22 00:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (939, 'cap1', '2019-02-22 00:32:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (940, 'sadmin', '2019-02-22 00:32:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (941, 'cap1', '2019-02-22 00:34:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (942, 'sadmin', '2019-02-22 00:34:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (943, 'cap1', '2019-02-22 00:36:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (944, 'sadmin', '2019-02-22 00:36:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (945, 'cap1', '2019-02-22 00:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (946, 'sadmin', '2019-02-22 00:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (947, 'sadmin', '2019-02-22 10:40:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (948, 'sadmin', '2019-02-22 10:40:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (949, 'gob', '2019-02-22 13:02:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (950, 'sadmin', '2019-02-22 13:49:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (951, 'sadmin', '2019-02-22 14:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (952, 'gob', '2019-02-22 16:58:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (953, 'sadmin', '2019-02-22 17:20:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (954, 'gob', '2019-02-22 17:31:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (955, 'sadmin', '2019-02-22 17:33:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (956, 'gob', '2019-02-22 17:33:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (957, 'sadmin', '2019-02-22 18:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (958, 'sadmin', '2019-02-26 09:58:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (959, 'sadmin', '2019-02-26 09:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (960, 'cap1', '2019-02-26 09:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (961, 'sadmin', '2019-02-26 10:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (962, 'gob', '2019-02-26 10:14:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (963, 'sadmin', '2019-02-26 10:22:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (964, 'gob', '2019-02-26 11:17:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (965, 'gob', '2019-02-26 12:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (966, 'sadmin', '2019-02-26 12:34:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (967, 'sadmin', '2019-02-26 12:35:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (968, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (969, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (970, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (971, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (972, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (973, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (974, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (975, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (976, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (977, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (978, 'acuerdo1', '2019-02-26 17:11:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (979, 'acuerdo1', '2019-02-26 17:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (980, 'acuerdo1', '2019-02-26 17:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (981, 'acuerdo1', '2019-02-26 17:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (982, 'acuerdo1', '2019-02-26 17:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (983, 'acuerdo1', '2019-02-26 17:14:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (984, 'cap1', '2019-02-26 17:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (985, 'lider1', '2019-02-26 17:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (986, 'sadmin', '2019-02-26 17:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (987, 'acuerdo1', '2019-02-26 17:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (988, 'acuerdo1', '2019-02-26 17:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (989, 'acuerdo1', '2019-02-26 17:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (990, 'sadmin', '2019-02-26 17:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (991, 'acuerdo1', '2019-02-26 17:19:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (992, 'sadmin', '2019-02-26 17:23:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (993, 'acuerdo2', '2019-02-26 17:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (994, 'lider1', '2019-02-26 17:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (995, 'acuerdo1', '2019-02-26 17:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (996, 'acuerdo1', '2019-02-26 17:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (997, 'sadmin', '2019-02-26 17:31:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (998, 'gob', '2019-02-26 18:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (999, 'sadmin', '2019-02-27 09:59:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1000, 'sadmin', '2019-02-27 09:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1001, 'gob', '2019-02-27 10:02:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1002, 'sadmin', '2019-02-27 12:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1003, 'gob', '2019-02-27 17:33:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1004, 'sadmin', '2019-02-28 10:10:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1005, 'sadmin', '2019-02-28 11:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (1006, 'sadmin', '2019-02-28 11:21:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1007, 'sadmin', '2019-02-28 11:47:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1008, 'sadmin', '2019-03-01 09:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (1009, 'sadmin', '2019-03-01 09:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1010, 'cap1', '2019-03-01 13:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1011, 'sadmin', '2019-03-01 14:51:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1012, 'cap1', '2019-03-01 15:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1013, 'sadmin', '2019-03-01 16:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1014, 'cap1', '2019-03-01 16:41:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1015, 'sadmin', '2019-03-01 16:45:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1016, 'cap1', '2019-03-01 17:00:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1017, 'sadmin', '2019-03-01 17:00:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1018, 'sadmin', '2019-03-04 10:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1019, 'cap1', '2019-03-04 10:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1020, 'sadmin', '2019-03-04 10:06:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1021, 'sadmin', '2019-03-04 10:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1022, 'cap1', '2019-03-04 10:14:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1023, 'sadmin', '2019-03-04 10:14:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1024, 'cap1', '2019-03-04 10:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1025, 'sadmin', '2019-03-04 10:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1026, 'cap1', '2019-03-04 11:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1027, 'sadmin', '2019-03-04 11:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1028, 'cap1', '2019-03-04 11:51:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1029, 'sadmin', '2019-03-04 13:10:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1030, 'cap1', '2019-03-04 13:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1031, 'cap1', '2019-03-04 13:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1032, 'sadmin', '2019-03-04 15:42:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1033, 'cap1', '2019-03-04 16:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1034, 'sadmin', '2019-03-04 16:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1035, 'sadmin', '2019-03-04 17:08:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1036, 'sadmin', '2019-03-04 17:24:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1037, 'cap1', '2019-03-04 18:52:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1038, 'sadmin', '2019-03-05 10:06:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1039, 'sadmin', '2019-03-05 10:06:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1040, 'gob', '2019-03-05 10:07:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1041, 'sadmin', '2019-03-05 10:37:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1042, 'cap1', '2019-03-05 10:37:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1043, 'sadmin', '2019-03-05 10:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1044, 'sadmin', '2019-03-05 15:46:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1045, 'cap1', '2019-03-05 17:19:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1046, 'sadmin', '2019-03-05 17:19:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1047, 'sadmin', '2019-03-06 10:07:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (1048, 'sadmin', '2019-03-06 10:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1049, 'lider1', '2019-03-06 12:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1050, 'sadmin', '2019-03-06 12:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1051, 'sadmin', '2019-03-06 16:59:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1052, 'sadmin', '2019-03-06 17:00:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (1053, 'gob', '2019-03-06 17:04:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (1054, 'cap1', '2019-03-06 17:53:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1055, 'sadmin', '2019-03-06 17:53:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1056, 'gob', '2019-03-06 18:10:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1057, 'sadmin', '2019-03-06 18:34:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1058, 'cap1', '2019-03-06 18:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1059, 'lider1', '2019-03-06 18:47:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1060, 'sadmin', '2019-03-06 18:50:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1061, 'cap1', '2019-03-06 18:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1062, 'lider1', '2019-03-06 18:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1063, 'sadmin', '2019-03-06 19:03:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1064, 'sadmin', '2019-03-07 10:07:00', 'Inicio de Sesion', '172.16.2.187');
INSERT INTO `sesiones` VALUES (1065, 'sadmin', '2019-03-07 10:49:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1066, 'sadmin', '2019-03-07 10:49:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1067, 'cap1', '2019-03-07 10:50:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1068, 'lider1', '2019-03-07 10:50:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1069, 'sadmin', '2019-03-07 10:51:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1070, 'lider1', '2019-03-07 10:51:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1071, 'sadmin', '2019-03-07 10:51:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1072, 'cap1', '2019-03-07 10:52:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1073, 'lider1', '2019-03-07 10:52:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1074, 'sadmin', '2019-03-07 10:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1075, 'cap1', '2019-03-07 10:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1076, 'lider1', '2019-03-07 10:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1077, 'sadmin', '2019-03-07 11:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1078, 'lider1', '2019-03-07 13:37:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1079, 'cap1', '2019-03-07 13:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1080, 'sadmin', '2019-03-07 13:41:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1081, 'lider1', '2019-03-07 13:41:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1082, 'cap1', '2019-03-07 13:56:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1083, 'sadmin', '2019-03-07 13:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1084, 'cap1', '2019-03-07 13:57:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1085, 'sadmin', '2019-03-08 11:15:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1086, 'sadmin', '2019-03-08 11:15:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1087, 'sadmin', '2019-03-08 16:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1088, 'gob', '2019-03-08 17:12:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1089, 'sadmin', '2019-03-11 10:17:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1090, 'sadmin', '2019-03-11 10:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1091, 'gob', '2019-03-11 15:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1092, 'sadmin', '2019-03-11 17:03:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1093, 'sadmin', '2019-03-12 10:20:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1094, 'sadmin', '2019-03-12 10:20:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1095, 'sadmin', '2019-03-12 15:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1096, 'sadmin', '2019-03-12 17:22:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (1097, 'sadmin', '2019-03-13 10:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1098, 'gob', '2019-03-13 12:04:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1099, 'sadmin', '2019-03-13 12:05:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1100, 'gob', '2019-03-13 12:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1101, 'sadmin', '2019-03-13 12:16:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1102, 'gob', '2019-03-13 12:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1103, 'sadmin', '2019-03-13 12:17:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1104, 'sadmin', '2019-03-13 12:18:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1105, 'gob', '2019-03-13 13:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1106, 'sadmin', '2019-03-13 13:26:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1107, 'lider1', '2019-03-13 16:25:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1108, 'sadmin', '2019-03-13 16:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1109, 'cap1', '2019-03-13 16:27:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1110, 'sadmin', '2019-03-13 16:28:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1111, 'cap1', '2019-03-13 16:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1112, 'sadmin', '2019-03-13 16:38:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1113, 'cap1', '2019-03-13 16:54:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1114, 'sadmin', '2019-03-13 16:55:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1115, 'cap1', '2019-03-13 17:30:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1116, 'sadmin', '2019-03-13 17:33:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1117, 'cap1', '2019-03-13 17:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1118, 'sadmin', '2019-03-13 17:39:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1119, 'cap1', '2019-03-13 17:42:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1120, 'sadmin', '2019-03-13 17:42:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1121, 'cap1', '2019-03-13 17:45:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1122, 'sadmin', '2019-03-13 17:46:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1123, 'cap1', '2019-03-13 17:47:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1124, 'sadmin', '2019-03-13 17:47:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1125, 'cap1', '2019-03-13 18:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1126, 'sadmin', '2019-03-13 18:01:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1127, 'sadmin', '2019-03-13 18:13:00', 'Inicio de Sesion', '172.16.2.21');
INSERT INTO `sesiones` VALUES (1128, 'sadmin', '2019-03-14 10:23:00', 'Inicio de Sesion', '172.16.2.21');

-- ----------------------------
-- Table structure for updateplan
-- ----------------------------
DROP TABLE IF EXISTS `updateplan`;
CREATE TABLE `updateplan`  (
  `idCambio` int(255) NOT NULL AUTO_INCREMENT,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `idProyecto` int(255) NOT NULL,
  PRIMARY KEY (`idCambio`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of updateplan
-- ----------------------------
INSERT INTO `updateplan` VALUES (10, '200', '200', '200', '2200', '200', '200', '2019-02-20', 'cap1', b'1', 9);
INSERT INTO `updateplan` VALUES (11, '2005', '5', '5', '5', '5', '5', '2019-02-20', 'cap1', b'1', 9);

-- ----------------------------
-- Table structure for usuariokeyresult
-- ----------------------------
DROP TABLE IF EXISTS `usuariokeyresult`;
CREATE TABLE `usuariokeyresult`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kr` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`kr`) USING BTREE,
  CONSTRAINT `fk_registro_kr` FOREIGN KEY (`kr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_user` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuariokeyresult
-- ----------------------------
INSERT INTO `usuariokeyresult` VALUES (61, 'cap1', 78);
INSERT INTO `usuariokeyresult` VALUES (62, 'cap1', 80);

-- ----------------------------
-- Table structure for usuarioplanes
-- ----------------------------
DROP TABLE IF EXISTS `usuarioplanes`;
CREATE TABLE `usuarioplanes`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `plan` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`plan`) USING BTREE,
  CONSTRAINT `usuarioplanes_ibfk_1` FOREIGN KEY (`plan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `usuarioplanes_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarioplanes
-- ----------------------------
INSERT INTO `usuarioplanes` VALUES (79, 'acuerdo1', 9);
INSERT INTO `usuarioplanes` VALUES (80, 'acuerdo1', 10);
INSERT INTO `usuarioplanes` VALUES (81, 'acuerdo2', 10);
INSERT INTO `usuarioplanes` VALUES (82, 'acuerdo2', 14);
INSERT INTO `usuarioplanes` VALUES (85, 'lider1', 9);
INSERT INTO `usuarioplanes` VALUES (86, 'lider1', 10);
INSERT INTO `usuarioplanes` VALUES (87, 'cap1', 9);
INSERT INTO `usuarioplanes` VALUES (88, 'cap1', 29);
INSERT INTO `usuarioplanes` VALUES (89, 'cap1', 10);

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clave` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idRol` int(255) NOT NULL,
  `nombre` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoP` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoM` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `celular` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `puesto` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `correo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(1) NOT NULL,
  `acuerdos` bit(1) NOT NULL,
  PRIMARY KEY (`user`) USING BTREE,
  INDEX `fk_usuario_rol`(`idRol`) USING BTREE,
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('acuerdo1', 'acuerdo1', 5, 'Marco', 'Antonio', 'Antonio', '2221346270', '2221346270', 'acuerdo1', '2019-02-26', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('acuerdo2', 'acuerdo2', 5, 'Marco', 'Antonio', 'Antonio', '2221346270', '2221346270', 'acuerdo2', '2019-02-26', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('cap1', 'cap1', 4, 'Angel de Jesus', 'Cardona', 'Benitez', '2221346270', '2221346270', 'Marco', '0000-00-00', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('gob', '1234', 1, 'Martín', 'Orozco', '', '', '', '', '0000-00-00', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider1', 'lider1', 3, 'Armando', 'Sanchez', '1', '1', '1', '1', '2019-02-14', 'marco.antonio.cb@hotmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('sadmin', 'sadmin', 2, 'Marco Antonio', 'Cardona', 'Benitez', 'Superadmin', 'Superadmin', 'Superadmin', '0000-00-00', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('sadmin1', 'sadmin', 2, 'Daniel', 'Contreras', 'Espiritu', '', '', '', '0000-00-00', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('sadmin2', 'sadmin', 2, 'Eduardo', 'Youshimatz', 'Moreno', '', '', '', '0000-00-00', '', 1, b'1');

SET FOREIGN_KEY_CHECKS = 1;

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class IndicadoresTempController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('IndicadorTempModel');
		$this->load->model('IndicadorModel');

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
	}

	public function insert(){
		$data = $this->input->post();
		$data['fecha'] = date("Y-m-d H:i");
		$result = $this->IndicadorTempModel->insert($data);
		echo ($result != 0) ? 1 : 0;
	}

	public function validaAprobado(){
		$idIndicador = $this->input->post('idIndicador');
		$response = $this->IndicadorTempModel->validaAprobado($idIndicador);
		if (count($response)==1) {
			#Nunca se ha metido
			if ($response[0]->status == 0) {
				#No esta aprobado no se puede hacer nada
				echo 0;
			} else {
				#Continuamos como normalmente
				echo 1;
			}
		}else if(count($response)==0){
			echo 1;
		}
	}

	public function cambio($idIndicador){
		$response = $this->IndicadorTempModel->getOneByIdIndicadorCambio($idIndicador);
		$data=array(
			"inicio"=>$response[0]->inicio,
			"final"=>$response[0]->final,
			"avance"=>0
		);
		$response = $this->IndicadorModel->cambio($idIndicador, $data);
		$data = array(
			'status'=>1
		);
		$response = $this->IndicadorTempModel->update($idIndicador, $data);
		echo 1;
	}


	/*

	public function editarAvance(){
		$data = $this->input->post();
		$dataOperacion= $this->IndicadorModel->getById($data['idIndicador']);
		$porcentaje = ($data['avance']*100)/$dataOperacion[0]->final;
		$response = $this->IndicadorModel->updateAvance($data['idIndicador'], $data['avance']);

		echo $porcentaje;
	}

	public function edit($idIndicador){
		var_dump($idIndicador);
	}


	public function update($idPlan = null){
		if(isset($idPlan)) {
			$data = $this->input->post();
			$result = $this->PlanesModel->update($idPlan, $data);
			echo ($result != 0) ? 1 : 0;
		}
	}

	/*


	public function detalle($idObjetivo){
		$response = $this->objetivosMenu();
		$dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);
		$dataKeyResult = $this->KeyResultModel->getByObjetivos($idObjetivo);
		$dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);

		$data = array(
			'admin' => true,
			'objetivos' => $response,
			'objetivo' => $dataObjetivo,
			'keyresult' => $dataKeyResult,
			'mv'=> $dataMv
		);
		$this->load->view('detalle_objetivo', $data);
	}


	#Funciones independientes
	public function objetivosMenu(){
		$dataObjetivos = $this->ObjetivosModel->get();
		foreach ($dataObjetivos as $objetivos){
			#Hacemos consulta sobre las key result de ese objetivo
			$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
			$progresoIndividual = 0;
			if(count($dataKeyResult) > 0) {
				foreach ($dataKeyResult as $kr) {
					if ($kr->metrica != 'Porcentaje') {
						$progreso = (($kr->avance) * 100) / $kr->medicionfinal;
					} else {
						$progreso = $kr->avance;
					}
					$kr->progreso = $progreso;
					$progresoIndividual = $progresoIndividual + $progreso;
				}

				$progresoObjetivo = ($progresoIndividual * 100) / (count($dataKeyResult) * 100);
				$objetivos->progreso = $progresoObjetivo;
			}else{
				$objetivos->progreso = 0;
			}
			$objetivos->kr = $dataKeyResult;
		}

		return $dataObjetivos;
	}*/



}

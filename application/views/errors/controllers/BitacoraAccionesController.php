<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class BitacoraAccionesController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel');
        $this->load->model('BitacoraAccionesModel');
        $this->load->model('PlanesModel');
        $this->load->model('AccionesModel');
        $this->load->model('UsuariosModel');
        $this->load->model('KeyResultModel');
        $this->load->model('AnexosModel');
        $this->load->model('ObjetivosModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');


    }

    public function index()
    {
        $this->load->view('login');
    }


    public function insert()
    {
        $data = $this->input->post();
        if ($this->session->userdata('tipo') == 'admin' || $this->session->userdata('tipo') == 'superadmin') {
            $sumaAvanceAcciones = 0;
            $sumaAvanceKr = 0;
            $sumaPonderacion = 0;
            $idAccion = $data["idKeyResult"];
            $dataInsert = array(
                "idKeyResult" => $data["idKeyResult"],
                "descripcion" => $data["descripcion"],
                "ultimoAvance" => $data["ultimoAvance"],
                "avance" => $data["avance"],
                "user" => $data["user"],
                "aprobado" => 1,
            );
            $dataInsert['fecha'] = date('Y-m-d H:i');
            #Insertamos el avance como aprobado en la bitacora de las acciones
            $this->BitacoraAccionesModel->insert($dataInsert);
            #Hacemos un update del avance aprobado directo por ser usuario admin en el id de la accion
            $this->AccionesModel->updateAvance($idAccion, $data["avance"]);
            #obtenemos la informacion de la accion para obtener la kr principal
            $dataAcciones = $this->AccionesModel->getByIdAccion($idAccion);

            /*#Obtenemos la ponderacion de la kr interna
            $pondera = $dataAcciones[0]->ponderacion;
            #Obtenemos el avance de la ponderacion
            $avancePonderacion = ($pondera * $data["avance"])/100;
            #Añadimos el avance de la ponderacion a la kr
            $this->AccionesModel->updateAvancePonderado($idAccion, $avancePonderacion);*/

            #Obtenemos la Kr principal
            $krPrincipal = $dataAcciones[0]->idKr;
            #Obtenemos todas las acciones relacionadas con esa kr activas
            $dataAcciones = $this->AccionesModel->getByIdKrActivos($krPrincipal);
            foreach ($dataAcciones as $acciones) {
                #realizamos la suma de los avances de las acciones
                $sumaAvanceAcciones += $acciones->avance;
                //$sumaPonderacion += $acciones->avancePonderacion;
            }
            #Obtenemos el primedio de las acciones
            $promedioKr = $sumaAvanceAcciones / count($dataAcciones);
            //$promedioKr = $sumaPonderacion;

            #Modificamos el avance de la Kr principal asi como su porcentaje
            $this->KeyResultModel->updateAvance($krPrincipal, $promedioKr);
            $this->KeyResultModel->updateAvancePorcentaje($krPrincipal, $promedioKr);

            #Obtenemos la infomacion de la kr para obtener el objetivo
            $dataKeyResult = $this->KeyResultModel->getById($krPrincipal);

            #Obtenemos la ponderacion del KR principal y su avance en la ponderacion
            $pondera = $dataKeyResult[0]->ponderacion;
            $avancePondera = ($pondera *  $promedioKr)/100;
            #Hacemos update en el avance de la ponderacion de kr principal
            $this->KeyResultModel->updateAvancePorcentajePonder($krPrincipal, $avancePondera);

            #Obtenemos el id del objetivo principal
            $objetivoPrincipal = $dataKeyResult[0]->idObjetivo;
            $dataKeyResult = $this->KeyResultModel->getByIdObj($objetivoPrincipal);
            foreach ($dataKeyResult as $kr) {
                #realizamos la suma de los avances de los OKR
                //$sumaAvanceKr += $kr->avancePorcentaje;
                $sumaAvanceKr += $kr->porcentajePonderacion;
            }
            //$promedioObjetivo = $sumaAvanceKr / count($dataKeyResult);
            $promedioObjetivo = $sumaAvanceKr;
            $dataUpdateObj = array(
                "avance" => $promedioObjetivo,
                "avancePorcentaje" => $promedioObjetivo
            );
            $this->ObjetivosModel->update($objetivoPrincipal, $dataUpdateObj);


            $dataObj = $this->ObjetivosModel->getById($dataKeyResult[0]->idObjetivo);
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($dataObj[0]->idmv);
            $promPlan = 0;
            foreach ($dataObjetivos as $objetivos){
                $promPlan += $objetivos->avancePorcentaje;
            }
            $promPlan = $promPlan/count($dataObjetivos);

            /*$dataInsertPlan = array("avanceIndicadores"=>$promPlan);
            $this->PlanesModel->update($dataObj[0]->idmv, $dataInsertPlan);*/



            #Provisional
            $dataInsertPlan = array("avanceIndicadores"=>$promedioObjetivo);
            $this->PlanesModel->update($dataObj[0]->idmv, $dataInsertPlan);


            $dataResponse = array(
                "promedio" => $promedioKr,
                "promedioObjetivo" => $promedioObjetivo,
                "objetivo" => $objetivoPrincipal,
                "kr" => $krPrincipal
            );
            echo json_encode($dataResponse);
        } else {
            $dataInsert = array(
                "idKeyResult" => $data["idKeyResult"],
                "descripcion" => $data["descripcion"],
                "ultimoAvance" => $data["ultimoAvance"],
                "avance" => $data["avance"],
                "user" => $data["user"],
                "aprobado" => $data["aprobado"],
            );
            $dataInsert['fecha'] = date('Y-m-d H:i');
            #Insertamos el avance como aprobado en la bitacora de las acciones
            $this->BitacoraAccionesModel->insert($dataInsert);
        }

    }


    public function uploadFileTemp()
    {
        $upload_folder = 'pdfstemp';
        foreach ($_FILES as $i) {
            $nombre_archivo = $i['name'];
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, $archivador);
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }

    public function borrarArchivoTemporal()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfstemp';
        unlink($upload_folder . '/' . $archivo);
    }

    public function validaAprobado()
    {
        $idAccion = $this->input->post('idAccion');
        $response = $this->BitacoraAccionesModel->validaAprobado($idAccion);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ( $response[0]->aprobado == 0 || $response[0]->aprobado == 2 ) {
                #No esta aprobado no se puede hacer nada
                echo 0;
            } else {
                #Continuamos como normalmente
                echo 1;
            }
        } else if (count($response) == 0) {
            echo 1;
        }
    }

    public function cancelado()
    {
        $idKr = $this->input->post('idKeyResult');
        $response = $this->BitacoraAccionesModel->cancelado($idKr);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ($response[0]->aprobado == 2) {
                #No esta aprobado no se puede hacer nada
                $r = $this->UsuariosModel->getByUser($response[0]->user);
                $response[0]->userCancel = $r[0]->nombre;
                echo json_encode($response);
            } else {
                #Continuamos como normalmente
                echo 0;
            }
        } else if (count($response) == 0) {
            echo 0;
        }
    }


    public function getById()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $response = $this->BitacoraAccionesModel->getById($idBitacora);
        /*foreach ($response as $bit) {
            $anexos = $this->AnexosModel->getByIdKr($idBitacora);
            $bit->anexos = $anexos;
        }*/
        $r = $this->UsuariosModel->getByUser($response[0]->user);
        $response[0]->capturista = $r[0]->nombre;
        echo json_encode($response);
    }

    public function aprobar()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $this->BitacoraAccionesModel->aprobar($idBitacora);
        #cambiamos los pdf a que sean visibles
        //$this->AnexosModel->aprobar($idBitacora);

        echo $idBitacora;
    }

    public function rechazar()
    {
        $idBitacora = $this->input->post('idBitacora');
        $data = $this->input->post();
        $dataUpdate = array(
            "aprobado" => 2,
            "motivo" => $data['motivo'],
            "userNoAutorizo" => $data['userNoAutorizo'],
        );
        $this->BitacoraAccionesModel->rechazar($idBitacora, $dataUpdate);
        #cambiamos los pdf a que sean visibles
        //$this->AnexosModel->rechazar($idBitacora);
    }

    public function validaCancelado()
    {
        $idBitacora = $this->input->post('idBitacoraEnvio');
        $response = $this->BitacoraAccionesModel->validaCancelado($idBitacora);
        echo $response;
    }


    public function getByidAccion()
    {
        $idAccion = $data = $this->input->post('idAccion');
        $dataBitacora = $this->BitacoraAccionesModel->getByIdAccion($idAccion);
        if (count($dataBitacora) > 0) {
            foreach ($dataBitacora as $bitacora) {
                /*$indicadorResponse = $this->IndicadorModel->getById($bitacora->idIndicador);
                $bitacora->tituloIndicador = $indicadorResponse[0]->nombreIndicador;*/
                $bitacora->tituloIndicador = "Ejemplo";

                switch ($bitacora->aprobado) {
                    case 0:
                        $bitacora->aprobado = "Por autorizar";
                        break;
                    case 1:
                        $bitacora->aprobado = "Autorizado";
                        break;
                    case 2:
                        $bitacora->aprobado = "No autorizado";
                        break;
                    case 3:
                        $bitacora->aprobado = "Visto no autorizado";
                        break;
                }

                if (!isset($bitacora->motivo)) {
                    $bitacora->motivo = "------";
                }

                if (isset($bitacora->user)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->user);
                    $bitacora->user = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->user = "------";
                }

                if (isset($bitacora->userNoAutorizo)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userNoAutorizo);
                    $bitacora->userNoAutorizo = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userNoAutorizo = "------";
                }
                if (isset($bitacora->userAprobado)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userAprobado);
                    $bitacora->userAprobado = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userAprobado = "------";
                }

            }
            echo json_encode($dataBitacora);
        } else {
            echo 0;
        }

    }





}

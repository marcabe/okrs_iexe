<?php //require_once 'complementos/head.php' ?>
<?php
header('Access-Control-Allow-Origin: *');


?>


<script src="<?php echo base_url(); ?>assets/build/js/detalle_plan.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/detalle_plan.css" rel="stylesheet">
<style>
    #modalAdjuntos, #modalDiagnostico {
        z-index: 10000000 !important;
    }

    #th_indicador {
        background: #0094CA;
        color: white;
        width: 20%;
    }

    #th_progreso {
        background: #A3D65C;
        color: white;
    }

    #th_vi {
        background: #00B2A9;
        color: white;
    }

    #th_vf {
        background: #003B5C;
        color: white;
    }

    select {
        border-radius: 50px !important;
        border-color: #00B2A9 !important;
        color: #00B2A9 !important;
        font-size: 12px;
    }

    .pf {
        padding: 10px 15px;
        background: #f5f5f5;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #00B2A9;
        border-left: 1px solid #00B2A9;
        border-right: 1px solid #00B2A9;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }

    .panel {
        margin-bottom: 0 !important;
    }

    /*****************Chat******************/
    .panel-primary {
        border-color: #00B2A9 !important;
    }

    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #00B2A9;
        border-color: #00B2A9;
    }

    .enviarChat {
        background: #00B2A9;
    }

    .PlanChat {
        height: 207px !important;
        /*background-image: url('assets/build/images/chat.jpg');*/
        background: #cedce7 !important;
        overflow: scroll;
    }

    .mensajeChatDer, .mensajeChatIzq {
        margin-right: 0px !important;
        background: white !important;
        padding: 5px 10px !important;
        margin-bottom: 10px !important;
        border-radius: 7px !important;
    }

    .mensajeChatDer {
        float: right !important;
    }

    .mensajeChatDer label {
        color: #FF6A13;
    }

    .mensajeChatIzq label {
        color: crimson;
    }

    ul {
        list-style-type: none;

    }

    /***************************************************/

    .tituloPlanes {
        cursor: pointer !important;
        background: #00B2A9 !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 10px 0px 10px 0px !important;
    }

    .tituloPlanes h2 {
        color: white !important;
    }

    .acordingPlanes {
        cursor: pointer !important;
        background: #00B2A9 !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 0px 0px 0px 0px !important;
    }

    .acordingPlanes h4 {
        color: white !important;
    }

</style>
</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">-->
<?php //require_once 'complementos/menu.php' ?>
<?php //require_once 'complementos/topnavigation.php' ?>
<!-- /top navigation -->
<!--<div class="right_col" role="main">-->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <!--<img src="<?php echo base_url(); ?>assets/build/images/mp.png" class="img-responsive">-->
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- start accordion -->
                    <input id="idPlan" value="<?php echo $pl[0]->idMv; ?>" style="display: none">
                    <div class="tituloPlanes row accordion-toggle" data-toggle="collapse" href="#">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-xs-12">
                                <h2><b>Proyecto:</b> <?php echo $pl[0]->mv ?></h2>
                            </div>
                        </div>
                        <!--<div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="project_progress">
                                <small>2% Complete</small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar"
                                         data-transitiongoal="3" aria-valuenow="56"
                                         style="width: 100%;"></div>
                                </div>
                            </div>
                        </div>-->

                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes"
                         style="margin-top: 15px; margin-top: 45px;">
                        <div class="col-md-8 ico" data-toggle="collapse" href="#objetivosa">
                            <h5>
                                <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                   aria-hidden="true"></i>
                                Objetivo Anual / Comunicación
                            </h5>
                        </div>
                    </div>
                    <div id="objetivosa" class="accordion-body collapse in" aria-expanded="true">


                        <div class="col-md-12 subtitulos">
                            <div id="objetivoAnual" class="col-md-5">
                                <div class="clearfix"></div>
                                <?php
                                if ($objAnual != 0) {
                                    foreach ($objAnual as $oa) {
                                        ?>
                                        <div style="cursor: pointer; margin-top: 20px; margin-left: 0%;"
                                             title="<?php echo $oa->idObjetivo; ?>"
                                             class="col-md-12 col-md-offset-1 col-sm-12 col-xs-12 bg-white text-center graficasP objetivoss">
                                            <div id='anual<?php echo $oa->idObjetivo; ?>' class="2000x1600px"></div>
                                            <h5 style="font-weight: normal; font-size: 12px;"><?php echo (strlen($oa->objetivo) > 100) ? substr($oa->objetivo, 0, 100) . "..." : $oa->objetivo; ?></h5>
                                            <h5><?php echo $oa->ffinal; ?></h5>

                                        </div>
                                    <?php }
                                } else {
                                    ?>
                                    <h2>Sin objetivo registrado</h2>
                                <?php } ?>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="col-md-12" style="float: right">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading" style="height: 90px;">
                                                <div class="col-md-12 text-center"
                                                     style="font-weight: bold; margin-bottom: 10px; width: 100%; text-align: center;">
                                                    Comunicación <i class="fa fa-envelope" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-12" style="float: left;">
                                                    <select class="form-control listaLider"
                                                            id="listaLider<?php echo $pl[0]->idMv; ?>"
                                                            name="<?php echo $pl[0]->idMv; ?>">
                                                        <?php foreach ($usuariosAdmin as $usChat) { ?>
                                                            <option value="<?php echo $usChat->user; ?>"><?php echo $usChat->nombre; ?>
                                                                (<?php echo $usChat->nombrePuesto; ?>)
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body PlanChat" id="PlanChat<?php echo $pl[0]->idMv; ?>">
                                            <ul id="chatPlan<?php echo $pl[0]->idMv; ?>">
                                                <?php foreach ($chat as $ch) { ?>
                                                    <li>
                                                        <div class="col-md-12 col-xs-12 mensajeChatDer">
                                                            <label><?php echo $ch->nombre; ?></label>
                                                            <p><?php echo $ch->mensaje; ?></p>
                                                            <small class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-row align-items-center">
                                                <div class="col-sm-8 my-1">
                                                    <input type="text" class="form-control"
                                                           id="mensajeChat<?php echo $pl[0]->idMv; ?>"
                                                           placeholder="Escribir mensaje">
                                                </div>
                                                <div class="col-auto my-1">
                                                    <button class="btn btn-primary enviarChat" text="">
                                                        Enviar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes"
                         style="margin-top: 15px; margin-top: 45px;">
                        <div class="col-md-8 ico" data-toggle="collapse" href="#objetivosc">
                            <h5>
                                <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                   aria-hidden="true"></i>
                                Objetivo clave
                            </h5>
                        </div>
                    </div>
                    <div id="objetivosc" class="accordion-body collapse">
                        <div class="clearfix"></div>
                        <?php
                        if ($krAnual != 0)
                            foreach ($krAnual as $kra) {
                                ?>
                                <div style="cursor: pointer; margin-top: 20px;"
                                     title="<?php echo $kra->idKeyResult; ?>"
                                     class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas krInt">
                                    <div id='kranual<?php echo $kra->idKeyResult; ?>' class="2000x1600px"></div>
                                    <h5 style="font-weight: normal; font-size: 12px;">
                                        <?php echo (strlen($kra->descripcion) > 125) ? substr($kra->descripcion, 0, 125) . "..." : $kra->descripcion; ?>
                                    </h5>
                                    <h5><?php echo $kra->fecha; ?></h5>
                                </div>
                            <?php } ?>
                    </div>

                    <div id="">
                        <div class="clearfix"></div>
                        <?php if (count($obj) > 0) { ?>
                            <!--Informacion de la ficha tecnica-->
                            <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes"
                                 style="margin-top: 15px; margin-top: 45px;">
                                <div class="col-md-8 ico" data-toggle="collapse" href="#objetivos">
                                    <h5>
                                        <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                           aria-hidden="true"></i>
                                        Objetivos
                                    </h5>
                                </div>
                            </div>
                            <div id="objetivos" class="accordion-body collapse">
                                <div class="clearfix"></div>
                                <?php
                                foreach ($obj as $objetivo) {
                                    ?>
                                    <div style="cursor: pointer; margin-top: 20px;"
                                         title="<?php echo $objetivo->idObjetivo; ?>"
                                         class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas objetivoss">
                                        <div id='prueba<?php echo $objetivo->idObjetivo; ?>' class="2000x1600px"></div>
                                        <h5><?php echo $objetivo->descripcion; ?></h5>
                                    </div>
                                <?php } ?>
                            </div>
                            <div id="objetivos" class="accordion-body collapse">
                                <div class="clearfix"></div>
                                <?php
                                foreach ($obj as $objetivo) {
                                    ?>
                                    <div style="cursor: pointer; margin-top: 20px;"
                                         title="<?php echo $objetivo->idObjetivo; ?>"
                                         class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas objetivoss">
                                        <div id='prueba<?php echo $objetivo->idObjetivo; ?>' class="2000x1600px"></div>
                                        <h5><?php echo $objetivo->descripcion; ?></h5>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>

                        <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes"
                             style="margin-top: 15px; margin-top: 45px;">
                            <div class="col-md-8 ico" data-toggle="collapse" href="#indicadores">
                                <h5>
                                    <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                       aria-hidden="true"></i>
                                    Indicadores
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-12 subtitulos accordion-body collapse" id="indicadores">

                            <div class="col-md-12">
                                <table class="table table-striped col-md-12">
                                    <tr>
                                        <th style="width: 40%!important;"
                                            class="text-left th_planes movilmostrar" id="th_indicador"><i
                                                    class="fa fa-caret-square-o-right"
                                                    aria-hidden="true"></i>
                                            Indicador
                                        </th>
                                        <th style="width: 30%!important;"
                                            class="text-left th_planes movilnomostrar" id="th_avance"><i
                                                    class="fa fa-line-chart"
                                                    aria-hidden="true"></i>
                                            Avance
                                        </th>
                                        <th style="width: 10%!important;"
                                            class="text-left th_planes movilmostrar" id="th_progreso"><i
                                                    class="fa fa-gear" aria-hidden="true"></i>
                                            Progreso
                                        </th>
                                        <th style="width: 10%!important;"
                                            class="text-left th_planes movilnomostrar" id="th_vi"><i
                                                    class="fa fa-percent"
                                                    aria-hidden="true"></i>
                                            Valor
                                            Inicial
                                        </th>
                                        <th style="width: 10%!important;"
                                            class="text-left th_planes movilnomostrar" id="th_vf"><i
                                                    class="fa fa-percent"
                                                    aria-hidden="true"></i>
                                            Valor
                                            Final
                                        </th>
                                    </tr>
                                    <?php foreach ($indicadores as $ind) { ?>
                                        <tr>
                                            <td class="movilmostrar"><?php echo $ind->nombreIndicador; ?></td>
                                            <td class="movilnomostrar" id="avance<?php echo $ind->idIndicadores; ?>"><?php echo $ind->avancePorcentaje; ?></td>
                                            <td class="movilmostrar">
                                                <div class="project_progress">

                                                    <small
                                                            id="small<?php echo $ind->idIndicadores; ?>"><?php echo number_format($ind->avancePorcentaje, 2); ?>
                                                        %
                                                    </small>
                                                    <div class="progress progress_sm">
                                                        <div
                                                                id="indicadorI<?php echo $ind->idIndicadores; ?>"
                                                                class="progress-bar bg-green"
                                                                role="progressbar"
                                                                data-transitiongoal="<?php echo $ind->avancePorcentaje; ?>"
                                                                aria-valuenow="56"
                                                                style="width: <?php echo $ind->avancePorcentaje; ?>%;"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="movilnomostrar"><?php echo $ind->inicio; ?></td>
                                            <td class="movilnomostrar"><?php echo $ind->final; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>




                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 acordingPlanes"
                             style="margin-top: 15px; margin-top: 45px;">
                            <div class="col-md-12 ico">
                                <div class="col-md-6">
                                    <h5 data-toggle="collapse"
                                        href="#detalle">
                                        <i class="fa fa-plus-circle iconito" style="font-size: 15px;"
                                           aria-hidden="true"></i>
                                        Descripción del proyecto
                                    </h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-4 text-right">
                                        <button id="vp"
                                                style="background: #ff920a; border-color: #ff920a; margin-top: 10px;"
                                                class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"></i> Ver
                                            Policy memos
                                        </button>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button id="va"
                                                style="background: orangered; border-color: orangered; margin-top: 10px;"
                                                class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"></i> Ver
                                            adjuntos
                                        </button>
                                    </div>

                                    <div class="col-md-4 text-right">
                                        <button id="vd"
                                                style="background: #3f4c6b; border-color: #3f4c6b; margin-top: 10px;"
                                                class="btn btn-xs btn-danger"><i class="fa fa-file-pdf-o"></i> Ver
                                            diagnóstico
                                        </button>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 subtitulos accordion-body collapse" id="detalle">
asdasdasdasd
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <h4>Líder del proyecto</h4>
                                    <p><?php echo $pl[0]->lider; ?></p>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h4>Fin</h4>
                                        <p><?php echo $pl[0]->fin ?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <h4>Propósito</h4>
                                    <p><?php echo $pl[0]->proposito; ?></p>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <h4>Población potencial</h4>
                                    <p><?php echo $pl[0]->poblacionPotencial ?></p>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <h4>Población objetivo</h4>
                                    <p><?php echo $pl[0]->poblacionObjetivo; ?></p>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-12">
                                    <h4>Beneficio</h4>
                                    <p><?php echo $pl[0]->beneficio; ?></p>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <h4>Inversión total</h4>
                                <p>$<?php echo $pl[0]->inversionT ?></p>
                            </div>

                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">

                            </div>


                            <div class="col-md-6">
                                <h4>Indicador estratégico</h4>
                                <p><?php echo $pl[0]->iestrategico ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Método del indicador estratégico</h4>
                                <p><?php echo $pl[0]->metodoiestrategico ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Inversión 2018</h4>
                                <p><?php echo $pl[0]->d_inversion2018; ?></p>
                            </div>
                            <div class="col-md-6">
                                <h4>Inversión 2019</h4>
                                <p><?php echo $pl[0]->d_inversion2019; ?></p>
                            </div>
                            <div class="col-md-6">

                            </div>

                            <br><br><br><br><br>
                            <div class="col-md-4">
                                <h4>Presupuesto 2018</h4>
                                <p>$<?php echo $pl[0]->presupuesto2018; ?></p>
                            </div>
                            <div class="col-md-4">
                                <h4>Presupuesto 2019</h4>
                                <p><?php echo $pl[0]->presupuesto2019; ?></p>
                            </div>
                            <div class="col-md-4">
                                <h4>Indicador estratégico</h4>
                                <p>$<?php echo $pl[0]->indicadorEstrategico; ?></p>
                            </div>

                            <div class="col-md-4">
                                <h4>Inversión 2018</h4>
                                <p>$<?php echo $pl[0]->inversion2018; ?></p>
                            </div>
                            <div class="col-md-4">
                                <h4>Inversión 2019</h4>
                                <p>$<?php echo $pl[0]->inversion2019; ?></p>
                            </div>
                            <div class="col-md-4">
                                <h4>Meta del indicador</h4>
                                <p><?php echo $pl[0]->metaIndicador; ?></p>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- end of accordion -->
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="modalDiagnostico" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="text-center"><b>Proyecto:</b> <?php echo $pl[0]->mv ?></h2>
                <h2 class="text-center"><b>Diagnóstico</b></h2>
            </div>

            <div class="col-md-12 col-xs-12 form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <div class="col-md-1 col-xs-1"></div>
                    <label class="col-md-10 col-xs-10 text-justify" id="diagnostico"></label>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-xs-12 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalAdjuntos" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="text-center"><b>Proyecto:</b> <?php echo $pl[0]->mv ?></h2>
                <h2 class="text-center"><b>Adjuntos</b></h2>

            </div>

            <div class="col-md-12 col-xs-12 form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <div class="col-md-1 col-xs-1"></div>
                    <div class="col-md-10 col-xs-10 text-justify">
                        <table id="adjuntos" class="table table-bordered">
                            <tr>
                                <td class="text-center">No.</td>
                                <td class="text-center">Archivo</td>
                            </tr>
                            <tbody id="add">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-xs-12 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
       style="display: none;">
<input id="puestoLogueado" value="<?php echo $this->session->userdata('puesto'); ?>"
       style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
</div>
<!--</div>-->
<?php //require_once 'complementos/footer.php' ?>

<script>
    $(document).ready(function () {

        if( $(window).width()<768){
            $(".movilnomostrar").hide();
            $("#movilmostrar").show();
        }else{
            $(".movilnomostrar").show();
            $("#movilmostrar").show();
        }

        $("#va").click(function () {
            var tipo = 'd';
            $("#add").empty();
            var idPlan = $("#idPlan").val();
            var i = 0;
            $.ajax({
                url: "pdfPlanesController/getByIdPlan/" + idPlan,
                data: {
                    tipo: tipo
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var dataResponse = JSON.parse(response);
                        for (i = 0; i < dataResponse.length; i++) {
                            if(dataResponse[i].length>30){
                                dataResponse[i] = dataResponse[i][29];
                            }
                            $("#add").append("<tr><td>" + (i + 1) + "</td><td><a href='pdfPlan/" + dataResponse[i].pdf + "' target='_blank'><i class='fa fa-file-pdf-o fa-2x' style='color: red;'></i> " + dataResponse[i].pdf + " </td></tr>");
                        }
                        $("#modalAdjuntos").modal();
                        $("#modalAdjuntos").css({"z-index": "9999 !important"});

                    }

                }
            });
        });
        $("#vp").click(function () {
            var tipo = 'p';
            $("#add").empty();
            var idPlan = $("#idPlan").val();
            var i = 0;
            $.ajax({
                url: "pdfPlanesController/getByIdPlan/" + idPlan,
                data: {
                    tipo: tipo
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var dataResponse = JSON.parse(response);
                        for (i = 0; i < dataResponse.length; i++) {
                            $("#add").append("<tr><td>" + (i + 1) + "</td><td><a href='pdfPlan/" + dataResponse[i].pdf + "' target='_blank'><i class='fa fa-file-pdf-o fa-2x' style='color: red;'></i> " + dataResponse[i].pdf + " </td></tr>");
                        }
                        $("#modalAdjuntos").modal();
                        $("#modalAdjuntos").css({"z-index": "9999 !important"});

                    }

                }
            });
        });
        $("#vd").click(function () {
            var idPlan = $("#idPlan").val();
            $.ajax({
                url: "PlanesController/getInfo/" + idPlan,
                data: {},
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        var dataResponse = JSON.parse(response);
                        console.log(dataResponse);
                        $("#diagnostico").text(dataResponse[0].diagnostico);
                        $("#modalDiagnostico").modal();
                        $("#modalDiagnostico").css({"z-index": "9999 !important"});

                    }

                }
            });
        });



        <?php
        if($krAnual != 0)
        foreach ($krAnual as $kra) {
        ?>
        var g = new JustGage({
            id: "kranual<?php echo $kra->idKeyResult;?>",
            value:<?php echo $kra->avancePorcentaje;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            customSectors:
                [{
                    color : "#931623",
                    lo : 0,
                    hi : 24.999
                },{
                    color : "#f07622",
                    lo : 25,
                    hi : 49.99
                },{
                    color : "#f7b21d",
                    lo:50,
                    hi:74.99
                },{
                    color : "#3eb049",
                    lo:75,
                    hi:100
                }],
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        <?php
        foreach ($obj as $objetivo) {
        ?>
        var g = new JustGage({
            id: "prueba<?php echo $objetivo->idObjetivo;?>",
            value:<?php echo $objetivo->avance;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            customSectors:
                [{
                    color : "#931623",
                    lo : 0,
                    hi : 24.999
                },{
                    color : "#f07622",
                    lo : 25,
                    hi : 49.99
                },{
                    color : "#f7b21d",
                    lo:50,
                    hi:74.99
                },{
                    color : "#3eb049",
                    lo:75,
                    hi:100
                }],
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        <?php
        if($objAnual != 0)

        foreach ($objAnual as $oa) {
        ?>
        var g = new JustGage({
            id: "anual<?php echo $oa->idObjetivo;?>",
            value:<?php echo $oa->avancePorcentaje;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            customSectors:
                [{
                    color : "#931623",
                    lo : 0,
                    hi : 24.999
                },{
                    color : "#f07622",
                    lo : 25,
                    hi : 49.99
                },{
                    color : "#f7b21d",
                    lo:50,
                    hi:74.99
                },{
                    color : "#3eb049",
                    lo:75,
                    hi:100
                }],
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        $(".acordingPlanes").click(function () {
            if ($(this).find("i").hasClass('fa fa-plus-circle iconito')) {
                $(this).find("i").removeClass('fa fa-plus-circle iconito');
                $(this).find("i").addClass('fa fa-arrow-circle-up iconito');
            } else if ($(this).find("i").hasClass('fa fa-arrow-circle-up iconito')) {
                $(this).find("i").removeClass('fa fa-arrow-circle-up iconito');
                $(this).find("i").addClass('fa fa-plus-circle iconito');
            }

        });


        $(".objetivoss").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_objetivo/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');

                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

        $(".krInt").click(function () {
            $.ajax({
                type: "POST",
                url: 'KeyResultController/detalleKrPrincipal/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });


    });
</script>

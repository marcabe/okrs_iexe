<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class ObjetivosController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MvModel');
        $this->load->model('AccionesModel');

        $this->load->model('ObjetivosModel');
        $this->load->model('UsuariosKrModel');
        $this->load->model('BitacoraMovimientosModel');
        $this->load->model('BitacoraAccionesModel');
        $this->load->model('UsuariosModel');

        $this->load->model('UsuariosPlanesModel');


        $this->load->model('KeyResultModel');
        $this->load->model('BitacoraKrModel');
        $this->load->model('PlanesModel');
        $this->load->model('ChatModel');

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index()
    {
        #obtenemos los kr que le corresponden solamente
        $today = date("Y-m-d");

        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $tipo = $this->session->userdata('tipo');
            if ($tipo == 'capturista') {
                $today = new DateTime($today);

                $krArray = array();
                $us = $this->session->userdata('idUser');
                $kruser = $this->UsuariosKrModel->getKrByUser($us);
                foreach ($kruser as $kru) {
                    $kr = $kru->kr;
                    $response = $this->KeyResultModel->getObjetivoById($kr);
                    $arrayObjetivos[] = $response[0]->idObjetivo;
                }
                if (isset($arrayObjetivos))
                    $arrayObjetivos = array_unique($arrayObjetivos);

                if (isset($arrayObjetivos)) {
                    $objetivosUsuarios = array();
                    foreach ($arrayObjetivos as $obj) {
                        $dataObjetivos = $this->ObjetivosModel->getById($obj);
                        foreach ($dataObjetivos as $objetivos) {
                            $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);


                            #$dataChats = $this->ChatModel->getChatByIdObjetivo($objetivos->idObjetivo);
                            #

                            $dataUsers = $this->UsuariosModel->getByIdRol(3);
                            $dataChats = $this->ChatModel->getMeParaObj($dataUsers[0]->user, $objetivos->idObjetivo, $this->session->userdata('idUser'));
                            $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesEnlace($objetivos->idmv);
                            $objetivos->UsuariosChat = $dataUsuarios;

                            $objetivos->chat = $dataChats;

                            $progresoIndividual = 0;
                            if (count($dataKeyResult) > 0) {
                                $vencer=0;

                                foreach ($dataKeyResult as $kr) {
                                    if ($kr->metrica != 'Porcentaje') {
                                        $progreso = (($kr->avance) * 100) / $kr->medicionfinal;
                                    } else {
                                        $progreso = $kr->avance;
                                    }
                                    $kr->progreso = $progreso;
                                    $progresoIndividual = $progresoIndividual + $progreso;

                                    #Verificamos si hay algun Kr Con avance
                                    $dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
                                    $kr->bitacora = $dataBitacoraKr;
                                    $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                                    $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                                    if (count($dataAcciones) > 0) {
                                        $kr->acciones = $dataAcciones;
                                        $vencerInt = 0;
                                        foreach ($kr->acciones as $acciones) {
                                            $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                            $acciones->bitacoraA = $dataBitacoraKrInterno;


                                            $fechaInt = $acciones->fechaVence;
                                            $fechaInt = new DateTime($fechaInt);
                                            $diffInt = $today->diff($fechaInt);
                                            if($diffInt->days == 1 && $diffInt->invert==0){
                                                $vencerInt++;
                                                $pintafila = 1;
                                            }else{
                                                $pintafila = 0;
                                            }
                                            $acciones->pintafila= $pintafila;
                                            $kr->vencerInt=$vencerInt;


                                        }
                                    }


                                    $fecha = $kr->fecha;
                                    $fecha = new DateTime($fecha);
                                    $diff = $today->diff($fecha);
                                    if($diff->days == 1 && $diff->invert==0){
                                        $vencer++;
                                    }
                                    $objetivos->vencerOkrBi=$vencer;
                                }

                                $progresoObjetivo = ($progresoIndividual * 100) / (count($dataKeyResult) * 100);
                                $objetivos->progreso = $progresoObjetivo;
                            } else {
                                $objetivos->progreso = 0;
                            }
                            $objetivos->kr = $dataKeyResult;
                            $objetivosUsuarios[] = $objetivos;
                        }
                        $planesData = $this->PlanesModel->getById($objetivos->idmv);
                        $objetivos->proyecto = $planesData[0]->mv;
                    }

                    $data = array(
                        'objetivos' => $objetivosUsuarios,
                    );
                } else {
                    $data = array(
                        'objetivos' => null
                    );
                }
            } elseif ($tipo == 'superadmin') {
                $today = new DateTime($today);

                $dataObjetivos = $this->ObjetivosModel->get();
                $totalActualizar = 0;
                foreach ($dataObjetivos as $objetivos) {


                    $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesSadmin($objetivos->idmv);
                    $objetivos->UsuariosChat = $dataUsuarios;

                    $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    $dataChats = $this->ChatModel->getChatByIdObjGobSadmin($objetivos->idObjetivo, $this->session->userdata('idUser'));

                    $dataTotal = $this->ObjetivosModel->totalActualiza($objetivos->idObjetivo);
                    $totalActualizar += count($dataTotal);
                    //var_dump($dataChats);
                    $objetivos->chat = $dataChats;
                    $progresoIndividual = 0;
                    if (count($dataKeyResult) > 0) {
                        $vencer=0;
                        foreach ($dataKeyResult as $kr) {
                            $dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
                            $kr->bitacora = $dataBitacoraKr;

                            $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                            if (count($dataAcciones) > 0) {
                                $kr->acciones = $dataAcciones;
                                $vencerInt = 0;
                                foreach ($kr->acciones as $acciones) {
                                    $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                    $acciones->bitacoraA = $dataBitacoraKrInterno;

                                    $fechaInt = $acciones->fechaVence;
                                    $fechaInt = new DateTime($fechaInt);
                                    $diffInt = $today->diff($fechaInt);
                                    if($diffInt->days == 1 && $diffInt->invert==0){
                                        $vencerInt++;
                                        $pintafila = 1;
                                    }else{
                                        $pintafila = 0;
                                    }
                                    $acciones->pintafila= $pintafila;
                                    $kr->vencerInt=$vencerInt;
                                }
                            }
                            $fecha = $kr->fecha;
                            $fecha = new DateTime($fecha);
                            $diff = $today->diff($fecha);
                            if($diff->days == 1 && $diff->invert==0){
                                $vencer++;
                            }
                        $objetivos->vencerOkrBi=$vencer;
                        }
                    }
                    $objetivos->kr = $dataKeyResult;
                    //var_dump($dataObjetivos);

                    $objetivos->totalActualizar = $totalActualizar;
                    $totalActualizar = 0;


                    $planesData = $this->PlanesModel->getById($objetivos->idmv);
                    $objetivos->proyecto = $planesData[0]->mv;
                }


                $data = array(
                    'objetivos' => $dataObjetivos,
                );
            } else if ($tipo == 'admin') {
                $today = new DateTime($today);

                $dataObjetivos = $this->ObjetivosModel->getSuperAdmin($this->session->userdata('idUser'));
                $totalActualizar = 0;
                foreach ($dataObjetivos as $objetivos) {

                    $dataUsers = $this->UsuariosModel->getByIdRol(2);
                    $dataChats = $this->ChatModel->getMeParaObj($dataUsers[0]->user, $objetivos->idObjetivo, $this->session->userdata('idUser'));
                    $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesAdmin($objetivos->idmv);
                    $dataSadmins = $this->UsuariosModel->getByIdRol(2);
                    //var_dump($dataSadmins);die;
                    foreach ($dataSadmins as $sadmin) {
                        $dataObject = new \stdClass();
                        $dataObject->nombre = $sadmin->nombre;
                        $dataObject->apellidoP = $sadmin->apellidoP;
                        $dataObject->apellidoM = $sadmin->apellidoM;
                        $dataObject->nombrePuesto = $sadmin->nombrePuesto;
                        $dataObject->obj = $objetivos->idObjetivo;
                        $dataObject->user = $sadmin->user;
                        array_push($dataUsuarios, $dataObject);
                    }
                    $objetivos->UsuariosChat = $dataUsuarios;


                    $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    //$dataChats = $this->ChatModel->getChatByIdObjetivo($objetivos->idObjetivo);

                    $dataTotal = $this->ObjetivosModel->totalActualiza($objetivos->idObjetivo);
                    $totalActualizar += count($dataTotal);
                    //var_dump($dataChats);
                    $objetivos->chat = $dataChats;
                    $progresoIndividual = 0;
                    if (count($dataKeyResult) > 0) {
                        $vencer = 0;
                        foreach ($dataKeyResult as $kr) {
                            $dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
                            $kr->bitacora = $dataBitacoraKr;

                            $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                            $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                            if (count($dataAcciones) > 0) {
                                $kr->acciones = $dataAcciones;
                                $vencerInt = 0;
                                foreach ($kr->acciones as $acciones) {
                                    $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                    $acciones->bitacoraA = $dataBitacoraKrInterno;

                                    $fechaInt = $acciones->fechaVence;
                                    $fechaInt = new DateTime($fechaInt);
                                    $diffInt = $today->diff($fechaInt);
                                    if($diffInt->days == 1 && $diffInt->invert==0){
                                        $vencerInt++;
                                        $pintafila = 1;
                                    }else{
                                        $pintafila = 0;
                                    }
                                    $acciones->pintafila= $pintafila;
                                    $kr->vencerInt=$vencerInt;

                                }
                            }
                            $fecha = $kr->fecha;
                            $fecha = new DateTime($fecha);
                            $diff = $today->diff($fecha);
                            if($diff->days == 1 && $diff->invert==0){
                                $vencer++;
                            }
                            $objetivos->vencerOkrBi=$vencer;
                        }
                        if (isset($kr->acciones)) {
                            foreach ($kr->acciones as $acciones) {
                                $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                $acciones->bitacoraA = $dataBitacoraKrInterno;
                            }
                        }
                    } else {
                        /*$objetivos->progreso = 0;*/
                    }
                    $objetivos->kr = $dataKeyResult;
                    $objetivos->totalActualizar = $totalActualizar;
                    $totalActualizar = 0;
                    $planesData = $this->PlanesModel->getById($objetivos->idmv);
                    $objetivos->proyecto = $planesData[0]->mv;
                }
                $data = array(
                    'objetivos' => $dataObjetivos,
                );
            }
            $this->load->view('lista_objetivos', $data);
        }
    }

    public function alta()
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
            if ($this->session->userdata('idRol') == 3) {
                $us = $this->session->userdata('idUser');
                $pluser = $this->UsuariosPlanesModel->getPlanesByUser($us);
                foreach ($pluser as $plu) {
                    $pl = $plu->plan;
                    $response = $this->PlanesModel->getById($pl);
                    $dataPlan[] = $response[0];
                }
            } else {
                $dataPlan = $this->PlanesModel->get();
            }
            $data = array(
                'dataMv' => $dataPlan,
                'admin' => false,
            );
            $this->load->view('nuevo_objetivo', $data);
        } else {
            redirect(base_url());
        }
    }

    public function edita($idObjetivo)
    {
        $dataMv = $this->MvModel->get();
        $dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);

        $fi = explode("-", $dataObjetivo[0]->finicio);
        $dataObjetivo[0]->finicio = $fi[2] . "/" . $fi[1] . "/" . $fi[0];

        $ff = explode("-", $dataObjetivo[0]->ffinal);
        $dataObjetivo[0]->ffinal = $ff[2] . "/" . $ff[1] . "/" . $ff[0];

        $data = array(
            'dataMv' => $dataMv,
            'objetivo' => $dataObjetivo,
            'idObjetivo' => $idObjetivo,
            'admin' => false,
        );
        $this->load->view('edita_objetivo', $data);
    }

    public function elimina($idObjetivo)
    {
        $data = $this->input->post();
        $response = $this->ObjetivosModel->deleteById($idObjetivo, $data);
        echo $response;
    }


    public function insert()
    {
        $data = json_decode($this->input->post('obj'));
        $data->estado = 1;

        $porciones = explode("/", $data->finicio);
        $data->finicio = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

        $porciones = explode("/", $data->ffinal);
        $data->ffinal = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

        $result = $this->ObjetivosModel->insert($data);
        $data = array(
            'movimiento' => 'Alta de objetivo',
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($data);
        echo ($result != null) ? $result : 0;
    }


    public function update($idObjetivo = null)
    {
        if (isset($idObjetivo)) {
            $data = json_decode($this->input->post('obj'));

            $porciones = explode("/", $data->finicio);
            $data->finicio = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

            $porciones = explode("/", $data->ffinal);
            $data->ffinal = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

            $result = $this->ObjetivosModel->update($idObjetivo, $data);
            echo ($result != 0) ? $idObjetivo : 0;
        }
    }

    public function detalle($idObjetivo)
    {
        $vencerTotal = 0;
        $today = new DateTime(date("Y-m-d"));

        if ($this->session->userdata('usuario') != null) {
            $dataUsers = $this->UsuariosModel->getByIdRol(2);

            $response = $this->ElementosMenu();
            $dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);


            $dataChat = $this->ChatModel->getChatByIdObjetivo($idObjetivo);
            $dataChat = $this->ChatModel->getMeParaObj('sadmin', $idObjetivo, $this->session->userdata('idUser'));

            $dataKeyResult = $this->KeyResultModel->getByObjetivos($idObjetivo);

            foreach ($dataKeyResult as $kr){
                $fecha = $kr->fecha;
                $fecha = new DateTime($fecha);
                $diff = $today->diff($fecha);
                if($diff->days == 3 && $diff->invert==0){
                    $vencer = 1;
                    $vencerTotal++;
                }else{
                    $vencer = 0;
                }
                $kr->vencer = $vencer;

                $fecha = explode("-", $kr->fecha);
                switch ($fecha[1]){
                    case '01': $mes= "enero";break;
                    case '02': $mes= "febrero";break;
                    case '03': $mes= "marzo";break;
                    case '04': $mes= "abril";break;
                    case '05': $mes= "mayo";break;
                    case '06': $mes= "junio";break;
                    case '07': $mes= "julio";break;
                    case '08': $mes= "agosto";break;
                    case '09': $mes= "septiembre";break;
                    case '10': $mes= "cotubre";break;
                    case '11': $mes= "nomviembre";break;
                    case '12': $mes= "diciembre";break;
                }
                $kr->fechanew = $fecha[2]." de ".$mes." de ".$fecha[0];
            }

            $dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);

            $data = array(
                'vencerTotal' => $vencerTotal,
                'admin' => true,
                'chat' => $dataChat,
                'planes' => $response,
                'objetivo' => $dataObjetivo,
                'keyresult' => $dataKeyResult,
                'mv' => $dataMv,
                'usuariosAdmin' => $dataUsers
            );
            $this->load->view('detalle_objetivo', $data);
        } else {
            redirect(base_url());
        }
    }


    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }


}

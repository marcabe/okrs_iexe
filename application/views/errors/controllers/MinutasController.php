<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class MinutasController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("PlanesModel");
        $this->load->model("AcuerdosModel");
        $this->load->model('BitacoraAcuerdoModel');
        $this->load->model('UsuariosPlanesModel');

        $this->load->model("ObjetivosModel");
        $this->load->model('KeyResultModel');
        $this->load->model('UsuariosModel');

        $this->load->model('MinutasModel');
        $this->load->model('PdfMinutaModel');


        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->caracterNoDeseado = array(
            0 => 'á',
            1 => 'é',
            2 => 'í',
            3 => 'ó',
            4 => 'ú'
        );
    }

    public function index()
    {
        $today = date("Y-m-d");

        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $tipo = $this->session->userdata('tipo');
            $dataUser = $this->UsuariosModel->getByUser($this->session->userdata('idUser'));
            if ($dataUser[0]->acuerdos == 1) {
                $dataPlan = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
                if ($tipo == 'capturista') {
                    $today = new DateTime($today);

                    $arrayMinutas = array();
                    $dataPlan = $this->UsuariosPlanesModel->get_Planes_User($dataUser[0]->user);
                    foreach ($dataPlan as $plan) {
                        $venceTotal=0;
                        $dataMinutas = $this->MinutasModel->getByIdPlan($plan->plan);
                        if (count($dataMinutas) > 0) {
                            foreach ($dataMinutas as $minutas) {
                                $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                                $minutas->plan = $dataPlanes[0]->mv;
                                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                                $minutas->acuerdos = $dataAcuerdos;
                                if (count($dataAcuerdos) > 0) {
                                    $vencer = 0;

                                    foreach ($dataAcuerdos as $Ac) {
                                        #Verificamos si hay algun Acuerdo Con avance
                                        $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                        $Ac->bitacora = $dataBitacoraAcuerdo;


                                        $fecha = $Ac->fecha;
                                        $fecha = new DateTime($fecha);
                                        $diff = $today->diff($fecha);
                                        if ($diff->days == 3 && $diff->invert == 0) {
                                            $vencer++;
                                            $venceTotal ++;
                                            $pintafila = 1;
                                        } else {
                                            $pintafila = 0;
                                        }
                                        $Ac->pintafila = $pintafila;
                                    }
                                }
                                $minutas->vencer = $vencer;

                                array_push($arrayMinutas, $minutas);
                            }
                        }
                        $plan->minutas = $dataMinutas;
                        $plan->vencer = $venceTotal;


                    }
                    $data = array(
                        'planes' => $dataPlan,
                    );
                    $this->load->view('lista_minutas', $data);
                } elseif ($tipo == 'superadmin') {
                    $today = new DateTime($today);

                    $dataPlanesw = $this->PlanesModel->get();

                    foreach ($dataPlanesw as $pl) {
                        $venceTotal = 0;
                        //echo "<br>Inicia para un plan: ".$pl->mv." total: ".$venceTotal;

                        $dataMinutas = $this->MinutasModel->getByIdPlan($pl->idMv);
                        if (count($dataMinutas) > 0) {
                            foreach ($dataMinutas as $minutas) {
                                $arrayResponsables = array();
                                $arrayFechas = array();
                                if ($minutas->idPlan != 0) {
                                    $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                                    $minutas->plan = $dataPlanes[0]->mv;
                                    $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                                    $minutas->acuerdos = $dataAcuerdos;
                                    if (count($dataAcuerdos) > 0) {
                                        $vencer = 0;
                                        foreach ($dataAcuerdos as $Ac) {
                                            //echo "<br>Acuerdo: ".$Ac->actividad."<br>";
                                            #Verificamos si hay algun Acuerdo Con avance
                                            $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                            $Ac->bitacora = $dataBitacoraAcuerdo;

                                            array_push($arrayResponsables, $Ac->responsable);
                                            $arrayResponsables = array_unique($arrayResponsables);

                                            array_push($arrayFechas, $Ac->fecha);
                                            $arrayFechas = array_unique($arrayFechas);

                                            $fecha = $Ac->fecha;
                                            $fecha = new DateTime($fecha);
                                            $diff = $today->diff($fecha);
                                            if ($diff->days == 3 && $diff->invert == 0) {
                                                $vencer++;
                                                $venceTotal ++;
                                                $pintafila = 1;
                                            } else {
                                                $pintafila = 0;
                                            }
                                            $Ac->pintafila = $pintafila;
                                            //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                                        }
                                    }
                                    $minutas->listaResponsable = $arrayResponsables;
                                    $minutas->listaFechas = $arrayFechas;
                                    $minutas->vencer = $vencer;

                                }
                            }
                        }
                        //echo "<br>".$venceTotal."<br>";
                        $pl->minutas = $dataMinutas;
                        $pl->vencer = $venceTotal;
                    }
                    $data = array(
                        'planes' => $dataPlanesw,
                    );
                    $this->load->view('lista_minutas', $data);
                } else {
                    $today = new DateTime($today);

                    $arrayMinutas = array();
                    $arrayResponsables = array();
                    $dataPlan = $this->UsuariosPlanesModel->get_Planes_User($dataUser[0]->user);
                    foreach ($dataPlan as $plan) {
                        $venceTotal = 0;
                        $dataMinutas = $this->MinutasModel->getByIdPlan($plan->plan);
                        if (count($dataMinutas) > 0) {
                            foreach ($dataMinutas as $minutas) {
                                $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                                $minutas->plan = $dataPlanes[0]->mv;
                                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                                $minutas->acuerdos = $dataAcuerdos;
                                if (count($dataAcuerdos) > 0) {
                                    $vencer = 0;
                                    foreach ($dataAcuerdos as $Ac) {
                                        #Verificamos si hay algun Acuerdo Con avance
                                        $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                        $Ac->bitacora = $dataBitacoraAcuerdo;

                                        $fecha = $Ac->fecha;
                                        $fecha = new DateTime($fecha);
                                        $diff = $today->diff($fecha);
                                        if ($diff->days == 3 && $diff->invert == 0) {
                                            $vencer++;
                                            $venceTotal ++;
                                            $pintafila = 1;
                                        } else {
                                            $pintafila = 0;
                                        }
                                        $Ac->pintafila = $pintafila;
                                    }

                                }
                                $minutas->vencer = $vencer;

                                array_push($arrayMinutas, $minutas);
                            }
                        }
                        $plan->minutas = $dataMinutas;
                        $plan->vencer = $venceTotal;

                    }
                    $data = array(
                        'planes' => $dataPlan,
                    );
                    $this->load->view('lista_minutas', $data);
                }
            } else {
                $data = array(
                    'minutas' => null
                );
                $this->load->view('lista_minutas', $data);

            }
        } else {
            redirect(base_url());
        }
    }

    public function gobernador($idMinuta)
    {
        $dataPlanes = $this->PlanesModel->get();

        $response = $this->ElementosMenu();
        $responseMinutas = $this->ElemntosAcuerdos();


        $dataMinutas = $this->MinutasModel->getById($idMinuta);
        foreach ($dataMinutas as $minutas) {
            $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
            $minutas->plan = $dataPlanes[0]->mv;

            $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
            $minutas->acuerdos = $dataAcuerdos;
        }
        $data = array(
            'planes' => $response,
            'planesGraf' => $dataPlanes,
            'minutas' => $dataMinutas,
            'm' => $responseMinutas,
        );
        $this->load->view('lista_minutasGob', $data);
    }


    public function alta()
    {
        if ($this->session->userdata('usuario') != null) {
            if ($this->session->userdata('idRol') == 3 || $this->session->userdata('idRol') == 5) {
                $us = $this->session->userdata('idUser');
                $pluser = $this->UsuariosPlanesModel->getPlanesByUser($us);
                foreach ($pluser as $plu) {
                    $pl = $plu->plan;
                    $response = $this->PlanesModel->getById($pl);
                    $dataPlan[] = $response[0];
                }
            } else {
                $dataPlan = $this->PlanesModel->get();
            }
            foreach ($dataPlan as $planes) {
                $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
                $planes->objetivos = $dataObjetivos;
                foreach ($dataObjetivos as $objetivos) {
                    $dataKr = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    $objetivos->kr = $dataKr;
                }
            }
            $data = array(
                'planes' => $dataPlan
            );
            $this->load->view('nueva_minuta', $data);
        } else {
            redirect(base_url());
        }
    }

    public function compara($cadena)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);

    }

    public function upFile()
    {
        $upload_folder = 'pdfsminutas';
        foreach ($_FILES as $i) {
            $archi = $this->compara($i['name']);
            $nombre_archivo = $archi;
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, utf8_encode($archivador));
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }


    public function deleteFile()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfsminutas';
        unlink($upload_folder . '/' . $archivo);
    }

    public function deleteFileMin()
    {
        $idPdfMinuta = $this->input->post('idPdfMinuta');
        $this->PdfMinutaModel->delete($idPdfMinuta);
        echo 1;
    }

    public function deleteFileData()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfsminutas';
        //unlink($upload_folder.'/'.$archivo);
        $this->PdfMinutaModel->deletByPdf($archivo);
    }

    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataInsert['hora'] = str_replace(' ', '', $dataInsert['hora']);
        $porciones = explode(":", $dataInsert['hora']);
        $dataInsert['hora'] = $porciones[0] . ":" . $porciones[1];

        $porciones = explode("/", $dataInsert['fecha']);
        $dataInsert['fecha'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];
        if (isset($dataInsert['acuerdos'])) {
            $acuerdos = $dataInsert['acuerdos'];
            $banderaAcuerdo = 1;
        } else {
            $banderaAcuerdo = 0;
        }
        if (isset($dataInsert['anexsos'])) {
            $anexos = $dataInsert['anexsos'];
            $banderaAnexo = 1;
        } else {
            $banderaAnexo = 0;
        }

        unset($dataInsert['acuerdos']);
        unset($dataInsert['anexsos']);
        $dataInsert['status'] = 1;

        $dataMInutas = $this->MinutasModel->insert($dataInsert);

        if ($banderaAcuerdo == 1)
            for ($i = 0; $i < count($acuerdos); $i++) {
                $fechaAcuerdos = $acuerdos[$i]['fecha'];
                $fechaAcuerdos = explode("/", $fechaAcuerdos);
                $fechaAcuerdos = $fechaAcuerdos[2] . "-" . $fechaAcuerdos[1] . "-" . $fechaAcuerdos[0];

                $dataAcuerdos = array(
                    "idMInuta" => $dataMInutas,
                    "actividad" => $acuerdos[$i]['actividad'],
                    "responsable" => $acuerdos[$i]['responsable'],
                    "fecha" => $fechaAcuerdos,
                    "fechaAlta" => date("Y-m-d"),
                    "avance" => 0,
                    "status" => 1
                );
                $this->AcuerdosModel->insert($dataAcuerdos);

            }

        if ($banderaAnexo == 1)
            for ($i = 0; $i < count($anexos); $i++) {
                $dataAnexos = array(
                    "idMInuta" => $dataMInutas,
                    "pdf" => $anexos[$i]
                );
                $this->PdfMinutaModel->insert($dataAnexos);
            }
        echo 1;

    }

    public function deleteAcuerdo()
    {
        $idAcuerdo = $this->input->post('idAcuerdo');
        $this->AcuerdosModel->deleteByIdAcuerdo($idAcuerdo);
        echo 1;
    }

    public function edita($idMinuta)
    {
        if ($this->session->userdata('usuario') != null) {
            $dataPlan = $this->PlanesModel->get();
            $dataMinuta = $this->MinutasModel->getByIdEdit($idMinuta);
            $dataPdfMinutas = $this->PdfMinutaModel->getByIdMinuta($idMinuta);
            $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($idMinuta);
            $data = array(
                'planes' => $dataPlan,
                'minuta' => $dataMinuta,
                'pdf' => $dataPdfMinutas,
                'acuerdos' => $dataAcuerdos
            );
            $this->load->view('edita_minuta', $data);
        } else {
            redirect(base_url());
        }

    }


    public function elimina($idMinuta)
    {
        echo $idMinuta;
        $response = $this->MinutasModel->delete($idMinuta);
        echo $response;
    }

    public function edit($idMinuta)
    {
        $this->PdfMinutaModel->deleteAllByIdMunuta($idMinuta);
        $this->AcuerdosModel->deleteAllByIdMunuta($idMinuta);
        $this->MinutasModel->deleteAllByIdMinuta($idMinuta);
        $dataInsert = $this->input->post();
        $dataInsert['hora'] = str_replace(' ', '', $dataInsert['hora']);
        $porciones = explode(":", $dataInsert['hora']);
        $dataInsert['hora'] = $porciones[0] . ":" . $porciones[1];

        $porciones = explode("/", $dataInsert['fecha']);
        if (count($porciones) > 1) {
            $dataInsert['fecha'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];
        }

        if (isset($dataInsert['acuerdos'])) {
            $acuerdos = $dataInsert['acuerdos'];
            $banderaAcuerdo = 1;
        } else {
            $banderaAcuerdo = 0;
        }
        if (isset($dataInsert['anexsos'])) {
            $anexos = $dataInsert['anexsos'];
            $banderaAnexo = 1;
        } else {
            $banderaAnexo = 0;
        }

        unset($dataInsert['acuerdos']);
        unset($dataInsert['anexsos']);
        $dataInsert['status'] = 1;
        $dataMInutas = $this->MinutasModel->insert($dataInsert);


        if ($banderaAcuerdo == 1) {
            for ($i = 0; $i < count($acuerdos); $i++) {
                $fechaAcuerdos = $acuerdos[$i]['fecha'];
                $fechaAcuerdos = explode("/", $fechaAcuerdos);
                $fechaAcuerdos = $fechaAcuerdos[2] . "-" . $fechaAcuerdos[1] . "-" . $fechaAcuerdos[0];

                $dataAcuerdos = array(
                    "idMInuta" => $dataMInutas,
                    "actividad" => $acuerdos[$i]['actividad'],
                    "responsable" => $acuerdos[$i]['responsable'],
                    "fecha" => $fechaAcuerdos,
                    "fechaAlta" => date("Y-m-d"),
                    "avance" => $acuerdos[$i]['avance'],
                    "status" => 1
                );
                $this->AcuerdosModel->insert($dataAcuerdos);

            }
        }

        if ($banderaAnexo == 1) {
            for ($i = 0; $i < count($anexos); $i++) {
                $dataAnexos = array(
                    "idMInuta" => $dataMInutas,
                    "pdf" => $anexos[$i]
                );
                $this->PdfMinutaModel->insert($dataAnexos);
            }
        }


        $dataAcuerdo = $this->AcuerdosModel->getByidMinutas($dataMInutas);
        $promAcuerdos = 0;
        foreach ($dataAcuerdo as $acuerdos) {
            $promAcuerdos += $acuerdos->avance;
        }
        $promAcuerdos = $promAcuerdos / count($dataAcuerdo);

        $this->MinutasModel->setPorcentaje($promAcuerdos, $dataMInutas);

        echo $dataMInutas;
    }

    public function getPdfByIdMinuta()
    {
        $dataMInuta = $this->input->post("idMinuta");
        $dataPdfMinutas = $this->PdfMinutaModel->getByIdMinuta($dataMInuta);
        foreach ($dataPdfMinutas as $minuta){
            $pdf = $minuta->pdf;
            if(strlen($pdf)>15){
                $pdf = substr($pdf, 0, 15)."...";
            }
            $minuta->pdf = $pdf;
        }
        echo json_encode($dataPdfMinutas);

    }

    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }

    public function ElemntosAcuerdos()
    {
        $dataMinutas = $this->MinutasModel->get();
        return $dataMinutas;
    }

    public function getByIdInfo()
    {
        $idMinuta = $this->input->post('idMinuta');
        $dataResponse = $this->MinutasModel->getById($idMinuta);
        echo json_encode($dataResponse);
    }

    public function editVisible($idMinuta)
    {
        $visible = $this->input->post('visible');
        $data = array(
            "visible" => intval($visible)
        );
        $this->MinutasModel->update($idMinuta, $data);
        echo 1;
    }


}

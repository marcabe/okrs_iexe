<?php

class pdfPlanesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "pdf_plan";
	}
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

    public function getByIdPlan($idPlan, $tipo){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idPlan", $idPlan);
        $this->db->where("tipo", $tipo);

        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getAllByIdPlan($idPlan){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idPlan", $idPlan);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function delete($idpdfplan){
        $this->db->where('idPdfPlan', $idpdfplan);
        $this->db->delete($this->tabla);
    }


}

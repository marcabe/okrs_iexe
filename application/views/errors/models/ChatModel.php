<?php

class ChatModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "chat";
	}
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getChatByIdPlan($idPlan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idTipo', $idPlan);
		$this->db->where('tipo', 'plan');
        $this->db->where('status', 1);
        $consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	#Meotodo para los chats del Gob en su panel
	public function getChatByIdPlanGob($idPlan){
        $this->db->select('chat.idChat,	chat.mensaje, chat.idUsuario, chat.tipo, chat.idTipo, chat.fechahora, chat.`status`, usuarios.nombre, rol.idRol, rol.nombrePuesto');
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".idUsuario = usuarios.user");
        $this->db->join("rol", " usuarios.idRol = rol.idRol");
        $this->db->where("chat.tipo", "plan");
        $this->db->where("chat.status", 1);
        $this->db->where('chat.idTipo', $idPlan);
        $this->db->group_start();
        $this->db->where("rol.idRol", 1);
        $this->db->or_where("rol.idRol", 2);
        $this->db->group_end();
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    #Metodo para los chats entre Sadmin y Gob en los planes
    public  function getChatByIdPlanGobSadmin($idPlan, $idUsuario){
        $this->db->select('chat.idChat,	chat.mensaje, chat.idUsuario, chat.tipo, chat.idTipo, chat.fechahora, chat.`status`, usuarios.nombre, rol.idRol, rol.nombrePuesto');
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".idUsuario = usuarios.user");
        $this->db->join("rol", " usuarios.idRol = rol.idRol");
        $this->db->where("chat.tipo", "plan");
        $this->db->where('chat.idTipo', $idPlan);
        $this->db->where("chat.status", 1);
        $this->db->group_start();
        $this->db->where("rol.idRol", 1);
        $this->db->or_where("rol.idRol", 2);
        $this->db->where("chat.idUsuario",$idUsuario);
        $this->db->group_end();
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    #Metodo para los chats con el gob y el sadmin en los objetivos
    public  function getChatByIdObjGobSadmin($idObjetivo, $idUsuario){
        $this->db->select('chat.idChat,	chat.mensaje, chat.idUsuario, chat.tipo, chat.idTipo, chat.fechahora, chat.`status`, usuarios.nombre, rol.idRol, rol.nombrePuesto');
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".idUsuario = usuarios.user");
        $this->db->join("rol", " usuarios.idRol = rol.idRol");
        $this->db->where("chat.tipo", "objetivo");
        $this->db->where('chat.idTipo', $idObjetivo);
        $this->db->where("chat.status", 1);
        $this->db->group_start();
        $this->db->where("rol.idRol", 1);
        $this->db->or_where("rol.idRol", 2);
        $this->db->where("chat.idUsuario",$idUsuario);
        $this->db->group_end();
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    #Metodo para los chats entre lider y sadmin individual
    public function getMeParaPlaen($usuario, $idPlan, $me){

        $query = $this->db->query(
            "SELECT
	            `chat`.`idChat`,
                `chat`.`mensaje`,
                `chat`.`idUsuario`,
                `chat`.`tipo`,
                `chat`.`idTipo`,
                `chat`.`fechahora`,
                `chat`.`status`,
                `chat`.`fechahora`,
                `usuarios`.`nombre`,
                `rol`.`idRol`,
                `rol`.`nombrePuesto` 
            FROM
                `chat`
                JOIN `usuarios` ON `chat`.`idUsuario` = `usuarios`.`user`
                JOIN `rol` ON `usuarios`.`idRol` = `rol`.`idRol` 
            WHERE
                `chat`.`tipo` = 'plan' 
                AND chat.status = 1
                AND `chat`.`idTipo` = '".$idPlan."' 
                AND ((`chat`.`de` = '".$usuario."' AND `chat`.`para` = '".$me."') OR (`chat`.`de` = '".$me."' AND `chat`.`para` = '".$usuario."'))
	    ");
        return $query->result();
    }


    public function getMeParaObj($usuario, $idPlan, $me){

        $query = $this->db->query(
            "SELECT
	            `chat`.`idChat`,
                `chat`.`mensaje`,
                `chat`.`idUsuario`,
                `chat`.`tipo`,
                `chat`.`idTipo`,
                `chat`.`fechahora`,
                `chat`.`status`,
                `chat`.`fechahora`,
                `usuarios`.`nombre`,
                `rol`.`idRol`,
                `rol`.`nombrePuesto` 
            FROM
                `chat`
                JOIN `usuarios` ON `chat`.`idUsuario` = `usuarios`.`user`
                JOIN `rol` ON `usuarios`.`idRol` = `rol`.`idRol` 
            WHERE
                `chat`.`tipo` = 'objetivo' 
                AND chat.status = 1
                AND `chat`.`idTipo` = '".$idPlan."' 
                AND ((`chat`.`de` = '".$usuario."' AND `chat`.`para` = '".$me."') OR (`chat`.`de` = '".$me."' AND `chat`.`para` = '".$usuario."'))
	    ");
        return $query->result();
    }

	public function getChatByIdObjetivo($idObjetivo){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->join('usuarios', 'usuarios.user = '.$this->tabla.".idUsuario");
		$this->db->where($this->tabla.'.idTipo', $idObjetivo);
		$this->db->where($this->tabla.'.tipo', 'objetivo');
        $this->db->where($this->tabla.'.status', 1);

        $consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getChatByIdKey($idKey){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idTipo', $idKey);
		$this->db->where('tipo', 'kr');
        $this->db->where('status', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function deleteObjetivo($idTipo){
        $this->db->where('idTipo', $idTipo);
        $this->db->where('tipo', "objetivo");
        if($this->db->update($this->tabla, array('status' => 0)))
            return 1;
        else
            return 0;
    }

    public function deletePlan($idPlan){
        $this->db->where('idTipo', $idPlan);
        $this->db->where('tipo', "plan");
        if($this->db->update($this->tabla, array('status' => 0)))
            return 1;
        else
            return 0;
    }





}

<?php

class UsuariosPlanesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "usuarioplanes";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

    public function get(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".usuario = usuarios.`user`");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function getPlanesByUser($user){
		$this->db->select('plan');
		$this->db->from($this->tabla);
		$this->db->where("usuario", $user);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function get_Planes_User($user){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->join("plan", $this->tabla.".plan = plan.idMv");
        $this->db->where($this->tabla.".usuario", $user);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }


	public function deleteByUser($user){
		$this->db->where('usuario', $user);
		$this->db->delete($this->tabla);
	}


    public function getAllUsuariosByIdPlanesGob($idPlan){
        $this->db->select("usuarios.nombre, usuarios.apellidoP, usuarios.apellidoM, rol.nombrePuesto, usuarioplanes.plan, usuarios.user");
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".usuario = usuarios.`user`");
        $this->db->join("rol", "usuarios.idRol = rol.idRol");
        $this->db->where("usuarioplanes.plan", $idPlan);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }
    public function getAllUsuariosByIdPlanesSadmin($idPlan){
        $this->db->select("usuarios.nombre, usuarios.apellidoP, usuarios.apellidoM, rol.nombrePuesto, usuarioplanes.plan, usuarios.user");
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".usuario = usuarios.`user`");
        $this->db->join("rol", "usuarios.idRol = rol.idRol");
        $this->db->where("usuarioplanes.plan", $idPlan);
        $this->db->group_start();
        $this->db->or_where("usuarios.idRol", 3);
        $this->db->or_where("usuarios.idRol", 2);
        $this->db->group_end();
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getAllUsuariosByIdPlanesAdmin($idPlan){
        $this->db->select("usuarios.nombre, usuarios.apellidoP, usuarios.apellidoM, rol.nombrePuesto, usuarioplanes.plan, usuarios.user");
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".usuario = usuarios.`user`");
        $this->db->join("rol", "usuarios.idRol = rol.idRol");
        $this->db->where("usuarioplanes.plan", $idPlan);
        $this->db->group_start();
        $this->db->where("usuarios.idRol", 4);
        $this->db->or_where("usuarios.idRol", 3);
        $this->db->group_end();
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getAllUsuariosByIdPlanesEnlace($idPlan){
        $this->db->select("usuarios.nombre, usuarios.apellidoP, usuarios.apellidoM, rol.nombrePuesto, usuarioplanes.plan, usuarios.user");
        $this->db->from($this->tabla);
        $this->db->join("usuarios", $this->tabla.".usuario = usuarios.`user`");
        $this->db->join("rol", "usuarios.idRol = rol.idRol");
        $this->db->where("usuarioplanes.plan", $idPlan);
        $this->db->where("usuarios.idRol", 3);

        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }


}

<?php

class ReunionesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "reuniones";
	}

    public function get(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }
    /*
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return true;
		else
			return null;
	}

    public function getById($idAcuerdo){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function getByidMinutas($idMinuta){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idMinuta', $idMinuta);
        $this->db->where('status', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getByIdAcuerdos($idAcuerdo){
        $this->db->select('idMInuta');
        $this->db->from($this->tabla);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->where('status', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function deleteByIdAcuerdo($idAcuerdo){
        $this->db->set('status', '0', FALSE);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->update($this->tabla);
    }
    public function deleteAllByIdMunuta($idMinuta){
        $this->db->where('idMinuta', $idMinuta);
        $this->db->delete($this->tabla);
    }

    public function updateAvance($idAcuerdo, $avance){
        $this->db->set('avance', $avance);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->update($this->tabla);
        return 1;
    }
*/

}

<?php

class BitacoraMovimientosModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "bitacoramovimientos";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}



}

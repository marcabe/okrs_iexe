<?php

class IndicadorTempModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "indicadores_temp";
	}
	public function insert($data)
	{
		if ($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getOneByIdIndicador($idInd){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicadores', $idInd);
		$this->db->where('status', 0);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}


	public function getOneByIdIndicadorCambio($idInd){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicadores', $idInd);
		$this->db->where('status', 0);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);

		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function validaAprobado($idIndicador){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicadores', $idIndicador);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function update($idIndicador, $data){
		$this->db->where('idIndicadores', $idIndicador);
		if($this->db->update($this->tabla, $data))
			return 1;
		else
			return 0;
	}

	/*public function getIndicadoresByIdPlan($idPlan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idPlan', $idPlan);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}



	public function updateAvance($idIndicdor, $avance){
		$this->db->set('avance', $avance);
		$this->db->where('idIndicadores', $idIndicdor);
		$this->db->update($this->tabla);
		return 1;
	}

	/*public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	*/

}

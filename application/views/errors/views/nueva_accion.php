<?php require_once 'complementos/head.php' ?>

<script src="<?php echo base_url(); ?>assets/build/js/nueva_accion.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/agrega_kr.css" rel="stylesheet">

</head>


<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url(); ?>assets/build/images/500.gif"
         style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Módulo Acciones</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="form-horizontal form-label-left" id="step-1">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Acción
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                  id="accion"></textarea>
                                        <small id="msj_accion"></small>
                                    </div>
                                </div>
                                <div class="form-group cfo">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Objetivo<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control has-feedback-left" id="objetivos">
                                            <option value=0>Selecciona un objetivo</option>
                                            <?php foreach ($objetivo as $obj) { ?>
                                                <option value="<?php echo $obj->idObjetivo; ?>"><?php echo $obj->objetivo; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="fa fa-sort form-control-feedback left"
                                              aria-hidden="true"></span>
                                        <small id="msj_objetivos"></small>
                                    </div>
                                </div>
                                <div class="form-group cfo">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Key Result<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control has-feedback-left" id="kr">
                                            <option value=0>Debes seleccionar primero un objetivo</option>
                                        </select>
                                        <span class="fa fa-sort form-control-feedback left"
                                              aria-hidden="true"></span>
                                        <small id="msj_kr"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success btn-sm" id="guardar"><i class="fa fa-plus"></i> Guardar</button>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button class="btn btn-danger btn-sm"><i class="fa fa-arrow-circle-left"></i> Regresar</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body text-center">
                        <p>Key Result agregado correctamente.</p>
                        <p>¿Deseas agregar otro?</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar">Finalizar</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php require_once 'complementos/footer.php' ?>

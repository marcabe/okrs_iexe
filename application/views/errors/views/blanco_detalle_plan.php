<?php require_once 'complementos/head.php' ?>
<?php header('Access-Control-Allow-Origin: *'); ?>


<!--<script src="<?php echo base_url(); ?>assets/build/js/detalle_objetivo.js"></script>-->
<link href="<?php echo base_url(); ?>assets/build/css/detalle_plan.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Modulo Plan</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2><i class="fa fa-align-left"></i> Plan a Detalle</h2>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<!-- start accordion -->
								<div class="tituloObjetivos row accordion-toggle" data-toggle="collapse" href="#">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="col-md-6">
											<h2><?php echo $pl[0]->mv ?></h2>
										</div>
									</div>
									<!--<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="project_progress">
											<small>2% Complete</small>
											<div class="progress progress_sm">
												<div class="progress-bar bg-blue" role="progressbar"
													 data-transitiongoal="3" aria-valuenow="56"
													 style="width: 100%;"></div>
											</div>
										</div>
									</div>-->

								</div>
								<div id="">
									<div class="col-md-12 subtitulos">

										<div class="col-md-6">
											<h4>Nombre del Proyecto</h4>
											<p><?php echo $pl[0]->mv; ?></p>
										</div>
										<div class="col-md-6">
											<h4>Status</h4>
											<p><?php echo $pl[0]->status ?></p>
										</div>
										<!--Informacion de la ficha tecnica-->
										<div class="col-md-6">
											<h4>Lider del proyecto</h4>
											<p><?php echo $pl[0]->lider; ?></p>
										</div>
										<div class="col-md-6">
											<h4>Inversion total</h4>
											<p>$<?php echo $pl[0]->inversionT ?></p>
										</div>
										<div class="col-md-6">
											<h4>Fin</h4>
											<p><?php echo $pl[0]->fin ?></p>
										</div>
										<div class="col-md-6">
											<h4>Poblacion potencial</h4>
											<p><?php echo $pl[0]->poblacionPotencial ?></p>
										</div>
										<div class="col-md-6">
											<h4>Propósito</h4>
											<p><?php echo $pl[0]->proposito; ?></p>
										</div>
										<div class="col-md-6">
											<h4>Población objetivo</h4>
											<p><?php echo $pl[0]->poblacionObjetivo; ?></p>
										</div>
									</div>

									<div class="col-md-12 subtitulos">
										<div class="col-md-4">
											<h4>Presupuesto 2018</h4>
											<p>$<?php echo $pl[0]->presupuesto2018; ?></p>
										</div>
										<div class="col-md-4">
											<h4>Presupuesto 2019</h4>
											<p><?php echo $pl[0]->presupuesto2019; ?></p>
										</div>
										<div class="col-md-4">
											<h4>Indicador estratégico</h4>
											<p>$<?php echo $pl[0]->indicadorEstrategico; ?></p>
										</div>

										<div class="col-md-4">
											<h4>Inversion 2018</h4>
											<p>$<?php echo $pl[0]->inversion2018; ?></p>
										</div>
										<div class="col-md-4">
											<h4>Inversión 2019</h4>
											<p>$<?php echo $pl[0]->inversion2019; ?></p>
										</div>
										<div class="col-md-4">
											<h4>Meta del indicador</h4>
											<p><?php echo $pl[0]->metaIndicador; ?></p>
										</div>
									</div>

									<div class="col-md-12 subtitulos">
										<div class="col-md-6">
											<h4>Avance financiero</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Avance físico</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Gestión de Inversion</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Efectividad financiera</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Efectividad administrativa</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Efectividad jurídica</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Resprogramación presupuestaria</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-6">
											<h4>Satisfacción ciudadana</h4>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="30" aria-valuenow="56"
													 style="width: 57%;"></div>
											</div>
										</div>
										<div class="col-md-12 text-justify">
											<h4>Resultados del mes</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting
												industry. Lorem Ipsum has been the industry's standard dummy text ever
												since the 1500s, when an unknown printer took a galley of type and
												scrambled it to make a type specimen book. It has survived not only five
												centuries, but also the leap into electronic typesetting, remaining
												essentially unchanged. It was popularised in the 1960s with the release
												of Letraset sheets containing Lorem Ipsum passages, and more recently
												with desktop publishing software like Aldus PageMaker including versions
												of Lorem Ipsum.</p>
										</div>
									</div>

									<!--Informacion de la ficha tecnica-->
									<div class="clearfix"></div>
									<div class="col-md-12 subtitulos text-center">
										<h3>OKR's</h3>
									</div>
									<div>
										<table class="table table-striped table-condensed">
											<thead>
											<tr>
												<th>Objetivo</th>
												<th>Descripcion</th>

											</tr>
											</thead>
											<tbody>
											<?php
											if (count($pl) > 0)
												foreach ($obj as $objetivo) {
													?>
													<tr>
														<td><?php echo $objetivo->objetivo; ?></td>
														<td><?php echo $objetivo->descripcion; ?></td>
													</tr>
												<?php } ?>
											</tbody>
											<tfoot>
											<tr>
												<th>Descripción</th>
												<th>Metrica</th>
											</tr>
											</tfoot>
										</table>
									</div>
									<!--<?php
									if (count($keyresult) > 0)
										foreach ($keyresult as $kr) {
											?>
											<div id="graficas" class="col-md-3">
												<div id='<?php echo "gauge" . $kr->idKeyResult; ?>'
													 class="2000x1600px"></div>
											</div>
										<?php } ?>-->
								</div>
								<!-- end of accordion -->
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require_once 'complementos/footer.php' ?>

		<script>
			$(document).ready(function () {
				<?php if (count($keyresult) > 0)
				foreach ($keyresult as $kr) { ?>
				var g = new JustGage({
					id: "<?php echo 'gauge' . $kr->idKeyResult?>",
					value: <?php echo $kr->avance?>,
					min: <?php echo $kr->medicioncomienzo?>,
					max: <?php echo $kr->medicionfinal?>,
					title: "<?php echo $kr->descripcion;?>",
					titlePosition: "below",
					valueFontColor: "#3f4c6b",
					pointer: true,
					pointerOptions: {
						toplength: -15,
						bottomlength: 10,
						bottomwidth: 12,
						color: '#8e8e93',
						stroke: '#ffffff',
						stroke_width: 3,
						stroke_linecap: 'round'
					},
					relativeGaugeSize: true,

				});
				<?php } ?>

			});
		</script>

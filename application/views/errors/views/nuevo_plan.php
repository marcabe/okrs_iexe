<?php require_once 'complementos/head.php' ?>

<script src="assets/build/js/alta_plan.js"></script>
<link href="assets/build/css/nuevo_plan.css" rel="stylesheet">
<link href="assets/build/css/nuevo_plan_dom.css" rel="stylesheet">


<link href="assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="assets/build/datepicker/js/i18n/datepicker.es.js"></script>

</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url(); ?>assets/build/images/500.gif"
         style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--<img src="<?php echo base_url(); ?>assets/build/images/mp.png" class="img-responsive">-->
                        <!--<h3>Módulo Planes</h3>-->
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Nuevo Proyecto</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="bloques">
                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="proyecto"
                                                       placeholder="Ingresar el nombre del proyecto">
                                                <span class="fa fa-folder-open form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_proyecto"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="status"
                                                       placeholder="Status del proyecto">
                                                <span class="fa fa-bookmark form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_status"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="lider"
                                                       placeholder="Líder del proyecto">
                                                <span class="fa fa-user form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_lider"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="inversion"
                                                       placeholder="Inversión total">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_inversion"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="finalidad"
                                                       placeholder="Finalidad del proyecto">
                                                <span class="fa fa-thumbs-up form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_finalidad"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="poblacionP"
                                                       placeholder="Población potencial">
                                                <span class="fa fa-users form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_poblacionP"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left" id="proposito"
                                                       placeholder="Propósito">
                                                <span class="fa fa-star form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_proposito"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="poblacionO"
                                                       placeholder="Poblacion objetivo">
                                                <span class="fa fa-child form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_poblacionO"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="finicio" data-position='top left' data-language='es'
                                                       placeholder="Fecha inicial del proyecto">
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_finicio"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="ffin" data-position='top left' data-language='es'
                                                       placeholder="Fecha final del proyecto">
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_ffin"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="beneficio" data-position='top left'
                                                       placeholder="Beneficios">
                                                <span class="fa fa-archive form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_beneficio"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="diagnostico" data-position='top left'
                                                       placeholder="Diagnóstico">
                                                <span class="fa fa-archive form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_diagnostico"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <select type="text"
                                                        class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                        id="reunion" data-position='top left'
                                                        placeholder="Beneficios">
                                                    <?php foreach ($reuiniones as $re) { ?>
                                                        <option value="<?php echo $re->idReunion; ?>"><?php echo $re->reunion; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="fa fa-archive form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_beneficio"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="codigo" data-position='top left'
                                                       placeholder="Código">
                                                <span class="fa fa-barcode form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_codigo"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="ie" data-position='top left'
                                                       placeholder="Indicador estratégico">
                                                <span class="fa fa-thumbs-up form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_ie"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="mie" data-position='top left'
                                                       placeholder="Método del indicador estratégico">
                                                <span class="fa fa-thumbs-up form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_mie"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row filaGrupo">
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="i2018" data-position='top left'
                                                       placeholder="Inversión 2018">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_i2018"></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text"
                                                       class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                       id="i2019" data-position='top left'
                                                       placeholder="Inversión 2019">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_i2019"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="bloques">

                                <div class="row filaGrupo">
                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="presupuesto2018"
                                                       placeholder="Presupuesto 2018">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_presupuesto2018"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="presupuesto2019"
                                                       placeholder="Presupuesto 2019">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_presupuesto2019"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="indicadorEstrategico"
                                                       placeholder="Indicador estratégico">
                                                <span class="fa fa-exclamation form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_indicadorEstrategico"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row filaGrupo">
                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="inversion2018"
                                                       placeholder="Inversión 2018">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_inversion2018"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="inversion2019"
                                                       placeholder="Inversión 2019">
                                                <span class="fa fa-dollar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_inversion2019"></small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group row text-right">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control has-feedback-left"
                                                       id="metaIndicador"
                                                       placeholder="Meta del indicador">
                                                <span class="fa fa-line-chart form-control-feedback left"
                                                      aria-hidden="true"></span>
                                                <small id="msj_metaIndicador"></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="row" id="seccionBotones">
                                <div class="col-md-12 text-center" >
                                    <button class="btn btn-success btn-xs" id="add_indicador">Agregar indicador</button>
                                </div>
                            </div>

                            <div class="bloquesI" id="fila_indicadores" style="margin-top: 10px;">



                            </div>



                            <!-- End SmartWizard Content -->
                            <div class="row" id="seccionBotones">
                                <div class="col-md-12 text-center" >
                                    <button class="btn btn-success" id="btnGuardar">Guardar</button>
                                </div>
                            </div>
                            <!-- End SmartWizard Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <?php require_once 'complementos/footer.php' ?>

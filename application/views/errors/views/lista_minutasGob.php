
<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("America/Mexico_City");

?>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"
      rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/lista_minutas.css" rel="stylesheet">

</head>

<div class="">
    <div class="page-title">
        <div class="title_left">
            <!--<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">-->
            <h3>Módulo Acuerdos</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="clearfix"></div>
                </div>

                <?php
                if ($minutas == null){
                    echo "<h4>Sin asignación.</h4>";
                }
                else
                foreach ($minutas

                as $m){ ?>
                <div class="x_content listaMinutas<?php echo $m->idMinuta; ?>">
                    <!-- start accordion -->
                    <div class="col-md-10">
                        <div class="tituloObjetivos row">
                            <div class="col-md-12 col-sm-12 col-xs-12 centrado">
                                <div class="col-md-10 ico" data-toggle="collapse"
                                     href="#<?php echo $m->idMinuta; ?>">
                                    <h2><i class="fa fa-arrow-circle-down iconito" style="font-size: 25px;"
                                           aria-hidden="true"></i>
                                        <!--<?php echo $m->idMinuta; ?> - --><?php echo $m->objetivo; ?>
                                    </h2>
                                </div>
                                <div class="col-md-2 text-center" style="margin-top: 5px;">
                                    <button class="btn btn-primary btn-xs verAcuerdos" id="acuerdos<?php echo $m->idMinuta; ?>" value="<?php echo $m->idMinuta; ?>">
                                        <i class="fa fa-edit" aria-hidden="true"></i> Archivos adjuntos
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <h6>Progreso Acuerdos</h6>
                        <div id="gauge<?php echo $m->idMinuta; ?>" class="grafi"></div>
                    </div>

                    <div class="col-md-12">
                        <div id="<?php echo $m->idMinuta; ?>" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <?php if ($this->session->userdata('tipo') != 'capturista'){ ?>
                                <div class="col-md-12">
                                    <?php }else{ ?>
                                    <div class="col-md-12">
                                        <?php } ?>
                                        <table class="table table-striped table-condensed resp_table demo">
                                            <thead>
                                            <tr>
                                                <th class="text-left th_planes azul" id="th_indicador"><i
                                                            class="fa fa-caret-square-o-right"
                                                            aria-hidden="true"> </i> Actividad
                                                </th>
                                                <!--th class="text-left th_planes" id="th_indicador">Metrica</th>-->
                                                <th class="text-left th_planes" id="th_avance"><i
                                                            class="fa fa-line-chart" aria-hidden="true"></i>
                                                    Responsable
                                                </th>
                                                <th class="text-left th_planes verde" id="th_progreso"><i
                                                            class="fa fa-gear" aria-hidden="true"></i> Fecha
                                                </th>
                                                <th class="text-left th_planes chicle" id="th_progreso"><i
                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                    Cumplimiento
                                                </th>
                                                <th class="text-left th_planes azul" id="th_progreso"><i
                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                    Progreso
                                                </th>
                                                <th class="text-left th_planes" id="th_progreso"><i
                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                    Plan
                                                </th>
                                                <th></th>
                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                    <th></th>
                                                <?php } ?>
                                                <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                    <th></th>
                                                <?php } ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (count($m->acuerdos) > 0)
                                                foreach ($m->acuerdos as $acuerdos) {
                                                    ?>
                                                    <tr class="listaKr<?php echo $acuerdos->idAcuerdo; ?>">
                                                        <td><?php echo $acuerdos->actividad; ?></td>
                                                        <td id="avance<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->responsable; ?></td>
                                                        <td id="fecha<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->fechaAlta; ?></td>
                                                        <td id="fecha<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->fecha; ?></td>

                                                        <td>
                                                            <div class="project_progress">
                                                                <small
                                                                        id="small<?php echo $acuerdos->idAcuerdo; ?>"><?php echo number_format($acuerdos->avance, 2); ?>
                                                                    %
                                                                </small>
                                                                <div class="progress progress_sm">
                                                                    <div class="progress-bar bg-green"
                                                                         id="barraProgreso<?php echo $acuerdos->idAcuerdo; ?>"
                                                                         role="progressbar"
                                                                         data-transitiongoal="<?php echo $acuerdos->avance; ?>"
                                                                         aria-valuenow="56"
                                                                         style="width: 57%;"></div>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td id="keyresultinicio<?php echo $acuerdos->idAcuerdo; ?>">
                                                            <?php echo $m->plan; ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of accordion -->
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAcuerdos" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="indicadorT">Bitácora del indicador: <strong id="tIndicador"></strong></h4>
            </div>

            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <table class="table table-striped table-bordered" id="tablaBitacora">
                            <thead style="font-size: 12px;">
                            <td>Acuerdo</td>
                            </thead>
                            <tbody style="font-size: 10px;">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
       style="display: none;">



<script>
    $(document).ready(function () {
        $(".verAcuerdos").click(function () {
            var i = 0;
            $.ajax({
                url: 'MinutasController/getPdfByIdMinuta',
                data: {
                    idMinuta: $(this).val(),
                },
                type: 'POST',
                success: function (response) {
                    if (response != 0) {
                        $("#tablaBitacora tbody tr").remove();
                        var dataResponse = JSON.parse(response);
                        console.log(dataResponse);
                        if(dataResponse.length>0) {
                            $("#tIndicador").html(dataResponse[0].tituloAcuerdo);
                            for (i = 0; i < dataResponse.length; i++) {
                                $("#tablaBitacora tbody").append(
                                    "<tr>" +
                                    "<td style='font-size: 15px;'>" +
                                    "<a download href='pdfsminutas/" + dataResponse[i].pdf + "'><i style='color: red;' class='fa fa-file-pdf-o fa-2x'></i> " + dataResponse[i].pdf + "</a>" +
                                    "</td>" +
                                    "</tr>"
                                );
                            }
                            $("#modalAcuerdos").modal();
                        }else{
                            $("#tablaBitacora tbody tr").remove();
                            $("#indicadorT").html("Sin bitácora");
                            $("#tIndicador").html("");
                            $("#modalAcuerdos").modal();
                        }
                    }
                }
            });
        });


        <?php
        if($minutas != null){
        foreach ($minutas as $m){ ?>

        g = new JustGage({
            id: 'gauge<?php echo $m->idMinuta;?>',
            value: <?php echo $m->avancePorcentaje;?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });

        <?php } }?>

    });

</script>
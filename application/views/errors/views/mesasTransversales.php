<?php require_once 'complementos/head.php' ?>

<script src="<?php echo base_url(); ?>assets/build/js/agrega_usuario.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/agrega_usuario.css" rel="stylesheet">

</head>
<script>
    $(document).ready(function () {

        if ($(window).width() < 768) {
            $(".movilnomostrar").hide();
        } else {
            $(".movilnomostrar").show();
        }

        <?php
        foreach ($planes as $pl) {
        if (count($pl->minutas) > 0) {
        foreach ($pl->minutas as $minutas) {?>

        $('#tams<?php echo $minutas->idMinuta;?>').DataTable({
            "paging": false,
            "ordering": true,
            "info": false,
            "searching": false
        });
        <?php } } } ?>
    });
</script>
<style>
    .vencidas {
        border-radius: 25px !important;
        background: #f07622;
        color: white;
        font-weight: bold;
        padding: 5px;
    }



    .pintafila {
        background: #f07622 !important;
        opacity: 0.7;
        color: white !important;
    }


    .tituloPlanes {
        margin-top: 10px;
        background: #00B2A9 !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 10px 0px 10px 0px !important;
    }

    .tituloObjetivos {
        cursor: pointer;
        margin-top: 25px;
        background: #cedce7;
        border-radius: 4px;
        color: #003B5C;
        padding: 10px 0px 10px 0px;
        /*height: 100px;*/
        align-items: center;
    }

    .rojo {
        background: #931623;
        color: white;
        width: 50%;
    }

    .amarillo {
        background: #f7b21d;
        color: white;
        width: 20%;
    }

    .azul {
        background: #0094c9;
        color: white;
        width: 20%;
    }

    .verde {
        background: #3eb049 ;
        color: white;
        width: 10%;
    }

    h2 {
        font-size: 14px;
    }


</style>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url(); ?>assets/build/images/500.gif"
         style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--<h3>Módulo Usuarios</h3>-->
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Mesas Transversales</h2>
                                <div class="clearfix"></div>
                            </div>
                            <?php foreach ($planes as $pl) { ?>
                                <?php if (count($pl->minutas) > 0) { ?>

                                    <div class="col-md-12 col-xs-12 tituloPlanes">
                                        <div class="row accordion-toggle acordingPlanes" data-toggle="collapse"
                                             href="#minuta<?php echo $pl->idMv; ?>">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-1">
                                                    <?php if (isset($pl->venceTotal) && $pl->venceTotal > 0) { ?>
                                                        <div class="col-md-6 text-center vencidas">
                                                            <?php echo $pl->venceTotal; ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <h2 style="cursor: pointer"><i
                                                                class="fa fa-plus-circle iconito"></i> <?php echo $pl->mv; ?>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-body collapse x_content"
                                         id="minuta<?php echo $pl->idMv; ?>">
                                        <?php foreach ($pl->minutas as $minutas) { ?>
                                            <div class="col-md-10 col-xs-12">
                                                <div class="tituloObjetivos row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 centrado">
                                                        <div class="col-md-6 ico" data-toggle="collapse"
                                                             href="#<?php echo $minutas->idMinuta; ?>">
                                                            <div class="col-md-12">
                                                                <div class="col-md-2">
                                                                    <?php if (isset($minutas->vencer) && $minutas->vencer > 0) { ?>
                                                                        <div class="col-md-6 text-center vencidas">
                                                                            <?php echo $minutas->vencer; ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="col-md-3 text-center"
                                                                     style="margin-top: 5px;">
                                                                    <button class="btn btn-primary btn-xs verOrden"
                                                                            id="acuerdos<?php echo $minutas->idMinuta; ?>"
                                                                            value="<?php echo $minutas->idMinuta; ?>">
                                                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                                                        Orden del día
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-3 text-center"
                                                                     style="margin-top: 5px;">
                                                                    <button class="btn btn-primary btn-xs verAcuerdos"
                                                                            style="background: #f7b21d; border-color: #f7b21d;"
                                                                            id="acuerdos<?php echo $minutas->idMinuta; ?>"
                                                                            value="<?php echo $minutas->idMinuta; ?>">
                                                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                                                        Archivos
                                                                        adjuntos
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-11 col-md-offset-1 col-xs-10 col-xs-offset-2">
                                                                    <h2><b>Lugar: </b>
                                                                        <?php echo $minutas->lugar; ?>
                                                                    </h2>
                                                                </div>
                                                                <div class="col-md-1 col-xs-2">
                                                                    <i
                                                                            class="fa fa-arrow-circle-down iconito"
                                                                            style="font-size: 25px;"
                                                                            aria-hidden="true"></i>
                                                                </div>
                                                                <div class="col-md-11 col-xs-10">
                                                                    <h2>
                                                                        <b>Fecha: </b>
                                                                        <?php echo $minutas->fecha; ?>
                                                                    </h2>
                                                                </div>
                                                                <div class="col-md-11 col-md-offset-1 col-xs-offset-2 col-xs-10">
                                                                    <h2>
                                                                        <b>Objetivo: </b>
                                                                        <?php echo $minutas->objetivo; ?>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <h6>Progreso Acuerdos</h6>
                                                <div id="gauge<?php echo $minutas->idMinuta; ?>" class="grafi"></div>
                                            </div>

                                            <div class="col-md-12">
                                                <div id="<?php echo $minutas->idMinuta; ?>"
                                                     class="accordion-body collapse">
                                                    <div class="accordion-inner">
                                                        <div class="col-md-12">
                                                            <table id="tams<?php echo $minutas->idMinuta;?>" class="table table-striped table-condensed resp_table demo">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-left th_planes rojo"
                                                                        id="th_indicador"><i
                                                                                class="fa fa-caret-square-o-right"
                                                                                aria-hidden="true"> </i> Actividad
                                                                    </th>
                                                                    <th class="text-left th_planes amarillo movilnomostrar"
                                                                        id="th_avance"><i
                                                                                class="fa fa-line-chart"
                                                                                aria-hidden="true"></i>
                                                                        Responsable
                                                                    </th>
                                                                    <th class="text-left th_planes azul"
                                                                        id="th_avance"><i
                                                                                class="fa fa-line-chart"
                                                                                aria-hidden="true"></i>
                                                                        Fecha
                                                                    </th>
                                                                    <th class="text-left th_planes verde"
                                                                        id="th_progreso"><i
                                                                                class="fa fa-gear"
                                                                                aria-hidden="true"></i>
                                                                        Progreso
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php foreach ($minutas->acuerdos as $acuerdos) { ?>
                                                                    <tr class="<?php echo ($acuerdos->pintafila == 1) ? 'pintafila' : '' ?>">
                                                                        <td>
                                                                            <?php echo $acuerdos->actividad; ?>
                                                                        </td>
                                                                        <td class="movilnomostrar">
                                                                            <?php echo $acuerdos->responsable; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $acuerdos->fecha; ?>
                                                                        </td>
                                                                        <td>
                                                                            <div class="project_progress">
                                                                                <small
                                                                                        id="small<?php echo $acuerdos->idAcuerdo; ?>"><?php echo number_format($acuerdos->avance, 2); ?>
                                                                                    %
                                                                                </small>
                                                                                <div class="progress progress_sm">
                                                                                    <div class="progress-bar bg-green"
                                                                                         role="progressbar"
                                                                                         data-transitiongoal="<?php echo $acuerdos->avance; ?>"
                                                                                         aria-valuenow="56"
                                                                                         style="width: 57%;"></div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalOrdenden" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="text-center">Orden del día</h3>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label>
                                <p id="odd"></p>
                            </label>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalAcuerdos" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="indicadorT">Archivos adjuntos <strong
                                    id="tIndicador"></strong></h4>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-xs-offset-1 col-xs-10 text-center">
                                <table class="table table-striped table-bordered" id="tablaBitacora">
                                    <thead style="font-size: 12px;">
                                    <td>Acuerdo</td>
                                    </thead>
                                    <tbody style="font-size: 10px;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 col-xs-12 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once 'complementos/footer.php' ?>
        <script>
            $(document).ready(function () {

                $(".verAcuerdos").click(function () {
                    var i = 0;
                    $.ajax({
                        url: 'MinutasController/getPdfByIdMinuta',
                        data: {
                            idMinuta: $(this).val(),
                        },
                        type: 'POST',
                        success: function (response) {
                            if (response != 0) {
                                $("#tablaBitacora tbody tr").remove();
                                var dataResponse = JSON.parse(response);
                                console.log(dataResponse.length);
                                console.log(dataResponse);
                                if (dataResponse.length > 0) {
                                    $("#tIndicador").html(dataResponse[0].tituloAcuerdo);
                                    for (i = 0; i < dataResponse.length; i++) {
                                        $("#tablaBitacora tbody").append(
                                            "<tr>" +
                                            "<td style='font-size: 15px;'>" +
                                            "<a download href='pdfsminutas/" + dataResponse[i].pdf + "'><i style='color: red;' class='fa fa-file-pdf-o fa-2x'></i> " + dataResponse[i].pdf + "</a>" +
                                            "</td>" +
                                            "</tr>"
                                        );
                                    }
                                    $("#indicadorT").html("Archivos adjuntos");
                                    $("#modalAcuerdos").modal();
                                } else {
                                    $("#tablaBitacora tbody tr").remove();
                                    $("#indicadorT").html("Archivos adjuntos");
                                    $("#tIndicador").html("");
                                    $("#modalAcuerdos").modal();
                                }
                            }
                        }
                    });
                });


                $(".ico").click(function () {
                    var iconito = $(this).find(".iconito");
                    if (iconito.hasClass('fa fa-arrow-circle-down iconito')) {
                        iconito.removeClass('fa fa-arrow-circle-down iconito');
                        iconito.addClass('fa fa-arrow-circle-up iconito');
                    } else if (iconito.hasClass('fa fa-arrow-circle-up iconito')) {
                        iconito.removeClass('fa fa-arrow-circle-up iconito');
                        iconito.addClass('fa fa-arrow-circle-down iconito');
                    }
                });


                $(".acordingPlanes").click(function () {
                    if ($(this).find("i").hasClass('fa fa-plus-circle iconito')) {
                        $(this).find("i").removeClass('fa fa-plus-circle iconito');
                        $(this).find("i").addClass('fa fa-arrow-circle-up iconito');
                    } else if ($(this).find("i").hasClass('fa fa-arrow-circle-up iconito')) {
                        $(this).find("i").removeClass('fa fa-arrow-circle-up iconito');
                        $(this).find("i").addClass('fa fa-plus-circle iconito');
                    }

                });

                $(".verOrden").click(function () {
                    var minutaId = $(this).val();
                    $.ajax({
                        url: "MinutasController/getByIdInfo",
                        data: {
                            idMinuta: minutaId
                        },
                        type: "POST",
                        success: function (response) {
                            if (response != 0) {
                                var dataResponse = JSON.parse(response);
                                console.log(dataResponse);
                                $("#modalOrdenden").modal();
                                $("#modalOrdenden").css({"z-index": 9999});

                                $("#odd").text(dataResponse[0].asunto);
                            }
                        }
                    });
                });
            });
            <?php foreach ($planes as $pl) { ?>
            <?php foreach ($pl->minutas as $minutas) { ?>

            g = new JustGage({
                id: 'gauge<?php echo $minutas->idMinuta;?>',
                value: <?php echo $minutas->avancePorcentaje;?>,
                min: 0,
                max: 100,
                titlePosition: "below",
                valueFontColor: "#3f4c6b",
                pointer: true,
                pointerOptions: {
                    toplength: -15,
                    bottomlength: 10,
                    bottomwidth: 12,
                    color: '#8e8e93',
                    stroke: '#ffffff',
                    stroke_width: 3,
                    stroke_linecap: 'round'
                },
                relativeGaugeSize: true,

            });
            <?php } } ?>
        </script>
<!DOCTYPE html>
<html>
<head>
    <title>Tablero</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/build/css/style.css" rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--/web-fonts-->
</head>
<script>
    $(document).ready(function () {
        $("#recuperar").click(function () {
            let avanza = 1;
            let dato = $("#dato").val();
            if (dato == '') {
                $("#msj_usuario").html("Debe ingresar un usuario o correo valido");
                $("#msj_usuario").show();
                $("#msj_usuario").fadeOut(3000);
                avanza = 0;
            }
            if(avanza==1){
                $.ajax({
                    url: "LoginController/recuperar_clave",
                    data:{
                        dato : dato
                    },
                    type: "POST",
                    success:function (response) {
                        $("#dato").val('');
                    }
                });
            }

        });
    });
</script>
<body>
<div class="main">


    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" id="formulario">
                <div class="col-sm-6 col-sm-6 text-center" id="logoimg">
                    <img src="assets/build/images/logo.png" class="img-responsive">
                </div>
                <div class="col-sm-6 col-sm-6" id="form1">
                    <div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
                        <i class="fa fa-user"></i><input class="datos" type="text"
                                                         Placeholder="Ingresar usuario o correo" id="dato" required>
                        <div>
                            <small id="msj_usuario" style="color: red; display: none;"></small>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12" style="margin-top: 25px;">
                        <button id="recuperar">Recuperar</button>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 text-center" style="margin-top: 25px;">
                    <label><a href="<?php echo base_url(); ?>">Regresar</a></label>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>



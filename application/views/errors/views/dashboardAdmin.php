<?php require_once 'complementos/head.php';
$admin = true;
?>

<link href="<?php echo base_url(); ?>assets/build/css/dashboard.css" rel="stylesheet">
</head>

<div id="fondoLoader" style="background: #003B5C; opacity: 0.5; !important; display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url(); ?>assets/build/images/500.gif"
         style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<body class="nav-md" onload="deshabilitaRetroceso()">
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>
        <?php require_once 'complementos/topnavigation.php' ?>
        <div class="right_col" role="main">


            <div class="boxinfo">
                <div class="boxdato"></div>
            </div>

            <div class="" id="carga">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Mis proyectos prioritarios</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">

                            <div class="row x_title">
                                <div class="col-md-6">
                                    <!--<h3>Graficas Generales de los Planes
                                    </h3>-->
                                </div>
                            </div>

                            <div class="countainer-fluid">
                                <div class="col-xs-12" style="margin-top: 10px;">
                                    <?php
                                    if (count($planesGraf) > 0)
                                        foreach ($planesGraf as $pl) {
                                            ?>
                                            <div class="col-xs-0 col-md-1" style="margin-top: 20px;"></div>
                                            <div class="col-xs-12 col-md-3 text-center planess" style="margin-top: 20px;cursor: pointer;" title="<?php echo $pl->idMv; ?>">
                                                <div class="col-md-12 col-xs-12" style="background: #F7F7F7; border-width: 2px; border-radius: 10px 10px 0px 0px; border-color: #03847d; border-style: solid; padding: 0;">
                                                    <div class="col-md-12 col-xs-12" style="background: #03847d; color: white; margin-top: 0px; border-radius: 8px 8px 0px 0px; height: 35px; font-size: 12px;">
                                                        <label style="margin-top: 10px;">
                                                            <?php if (isset($pl->vencerTotal) && $pl->vencerTotal > 0) { ?>
                                                                <?php echo $pl->mv; ?><sup  style="background: #f07622  ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px;"><?php echo $pl->vencerTotal;?></sup>
                                                            <?php }else{ ?>
                                                                <?php echo $pl->mv; }?>
                                                        </label>
                                                    </div>
                                                    <div class="col-xs-12 col-xs-offset-0" id='<?php echo "gauge" . $pl->idMv; ?>'></div>
                                                </div>
                                                <div class="col-md-12 col-xs-12" style="background: #F7F7F7; border-width: 2px; border-radius: 0px 0px 10px 10px; border-color: #03847d;border-style: solid; padding: 0;">
                                                    <div class="col-md-12 col-xs-12" style="background: #03847d; color: white; margin-top: 0px; border-radius: 0px 0px 8px 8px; height: 35px; font-size: 12px;">
                                                        <label><?php echo $pl->mv; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <br/>


            </div>
        </div>


        <?php require_once 'complementos/footer.php' ?>


        <script>
            $(document).ready(function () {

                <?php if (count($planesGraf) > 0)
                foreach ($planesGraf as $pl) { ?>
                var g = new JustGage({
                    id: "<?php echo 'gauge' . $pl->idMv?>",
                    value: <?php echo $pl->avanceIndicadores?>,
                    min: 0,
                    max: 100,
                    titlePosition: "below",
                    valueFontColor: "#003B5C",
                    pointer: true,
                    customSectors:
                        [{
                            color: "#931623",
                            lo: 0,
                            hi: 24.999
                        }, {
                            color: "#f07622",
                            lo: 25,
                            hi: 49.99
                        }, {
                            color: "#f7b21d",
                            lo: 50,
                            hi: 74.99
                        }, {
                            color: "#3eb049",
                            lo: 75,
                            hi: 100
                        }],
                    levelColors: [
                        "#A3D65C",
                        "#A3D65C",
                        "#A3D65C"
                    ],
                    pointerOptions: {
                        toplength: -20,
                        bottomlength: 6,
                        bottomwidth: 6,
                        color: '#00B2A9',
                        stroke: '#00B2A9',
                        stroke_width: 1,
                        stroke_linecap: 'round'
                    },
                    relativeGaugeSize: true,

                });
                <?php } ?>


                $(".planess").click(function () {
                    $.ajax({
                        type: "POST",
                        url: 'detalle_plan/' + $(this).attr('title'),
                        data: {},
                        success: function (datos) {
                            $('#carga').html(datos);
                            $('html, body').animate({scrollTop: 0}, 'slow');

                        },
                        xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            xhr.onloadstart = function (e) {
                                $("#fondoLoader").show();
                                console.log("Esta cargando");
                            };
                            xhr.onloadend = function (e) {
                                $("#fondoLoader").fadeOut(500);
                                console.log("Termino de cargar");
                            }
                            return xhr;
                        }
                    });
                });


            });
        </script>

<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');

?>
<script src="<?php echo base_url(); ?>assets/build/js/lista_usuarios.js" charset="UTF-8"></script>
<link href="<?php echo base_url(); ?>assets/build/css/lista_usuarios.css" rel="stylesheet">

</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<!--<h3>Modulo Usuarios</h3>-->
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2><i class="fa fa-align-left"></i> Lista de Usuarios</h2>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
								<table id="table-usuarios" class="table table-striped table-condensed resp_table demo">
									<thead>
									<tr>
                                        <th class="azul"><i class="fa fa-flag"></i> Tipo</th>
                                        <th class=""><i class="fa fa-user"></i> Usuario</th>
										<th class="verde"><i class="fa fa-dot-circle-o"></i> Contraseña</th>

										<th class="chicle"><i class="fa fa-user"></i> Nombre</th>
										<th class="azulf"><i class="fa fa-envelope-o"></i> Correo</th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($usuarios as $user) { ?>
										<tr>
                                            <td><?php echo $user->nombrePuesto;?></td>
                                            <td><?php echo $user->user;?></td>
											<td><?php echo $user->clave;?></td>
											<td><?php echo $user->nombre." ".$user->apellidoP." ".$user->apellidoM;?></td>
											<td><?php echo $user->correo;?></td>
											<td><button class="btn btn-primary btn-xs editar" value="<?php echo $user->user;?>"><i class="fa fa-edit"></i> Editar</button></td>
											<td><button class="btn btn-danger btn-xs eliminar" value="<?php echo $user->user;?>"><i class="fa fa-trash-o"></i> Eliminar</button></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- end of accordion -->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Eliminar usuario</h4>

                </div>

				<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
					<div class="col-md-6 text-right">
						<button class="btn btn-primary" id="btnAceptar">Aceptar</button>
					</div>
					<div class="col-md-6 text-left">
						<button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
								aria-label="Close" id="btnFinalizar">Cancelar
						</button>
					</div>
				</div>
			</div>

		</div>
	</div>






    <script src="<?php echo base_url(); ?>assets/js/basictable/jquery.basictable.min.js" charset="UTF-8"></script>
    <link href="<?php echo base_url(); ?>assets/js/basictable/basictable.css" rel="stylesheet">

    <script>
        $('#table-usuarios').basictable();
    </script>

	<?php require_once 'complementos/footer.php' ?>

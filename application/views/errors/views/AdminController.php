<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");


class AdminController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PlanesModel');
        $this->load->model('ObjetivosModel');
        $this->load->model('KeyResultModel');
        $this->load->model('IndicadorModel');
        $this->load->model('ReunionesModel');
        $this->load->model('PlanesModel');
        $this->load->model('AcuerdosModel');

        $this->load->model('MinutasModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->reunionMenu = 0;
    }

    public function index()
    {
        $today = new DateTime(date("Y-m-d"));

        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $dataPlanes = $this->PlanesModel->getVisibles();

            foreach ($dataPlanes as $pl) {
                $vencerTotal = 0;
                $dataObjetivo = $this->ObjetivosModel->getObjetivosByPlan($pl->idMv);
                if (count($dataObjetivo) > 0) {
                    $dataKrAnual = $this->KeyResultModel->getByObjetivos($dataObjetivo[0]->idObjetivo);
                    foreach ($dataKrAnual as $kr) {
                        $fecha = $kr->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $vencer = 1;
                            $vencerTotal++;
                        } else {
                            $vencer = 0;
                        }
                        $kr->vencer = $vencer;
                    }
                }
                $pl->vencerTotal = $vencerTotal;
            }

            $response = $this->ElementosMenu();
            $responseMinutas = $this->ElemntosAcuerdos();
            $this->reunionMenu = 0;
            $data = array(
                'vencerTotal' => $vencerTotal,
                'planes' => $response,
                'm' => $responseMinutas,
                'planesGraf' => $dataPlanes,
                'reunionMenu' => $this->reunionMenu
            );
            $this->load->view('dashboardAdmin', $data);
        } else {
            redirect(base_url());
        }
    }

    public function reuniones()
    {
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu' => $this->reunionMenu
        );
        $this->load->view('reuniones', $data);
    }


    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->getVisibles();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }

    public function ElemntosAcuerdos()
    {
        $dataMinutas = $this->MinutasModel->get();
        return $dataMinutas;
    }

    public function general()
    {
        $this->reunionMenu = 1;
        $today = new DateTime(date('Y-m-d'));
        $dataMG = $this->PlanesModel->getByReunion(1);
        $totalGeneralGM = 0;
        foreach ($dataMG as $planes) {
            $venceTotal = 0;
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
                if (count($dataAcuerdos) > 0) {
                    $vencer = 0;
                    foreach ($dataAcuerdos as $Ac) {
                        $fecha = $Ac->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $vencer++;
                            $venceTotal++;
                            $pintafila = 1;
                        } else {
                            $pintafila = 0;
                        }
                        $Ac->pintafila = $pintafila;
                        //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                    }
                }
                $minutas->vencer = $vencer;
            }
            $planes->minutas = $dataMinutas;
            $planes->venceTotal = $venceTotal;
            $totalGeneralGM += $venceTotal;
        }


        $mesaPE = $this->PlanesModel->getByReunion(2);
        $totalGeneralPE = 0;
        foreach ($mesaPE as $planes) {
            $venceTotal = 0;
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
                if (count($dataAcuerdos) > 0) {
                    //echo "kk<br>";
                    $vencer = 0;
                    foreach ($dataAcuerdos as $Ac) {
                        $fecha = $Ac->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            //echo "El acuerdo que vence es:".$Ac->idAcuerdo."<br>";
                            $vencer++;
                            $venceTotal++;
                            $pintafila = 1;
                        } else {
                            $pintafila = 0;
                        }
                        $Ac->pintafila = $pintafila;
                        //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                    }
                }
                //echo "<br>".$vencer;
                $minutas->vencer = $vencer;
            }
            $planes->minutas = $dataMinutas;
            $planes->venceTotal = $venceTotal;
            $totalGeneralPE += $venceTotal;
        }

        $mesasSectoriales = $this->PlanesModel->getByReunion(3);
        $totalGeneralMS = 0;
        foreach ($mesasSectoriales as $planes) {
            $venceTotal = 0;
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
                if (count($dataAcuerdos) > 0) {
                    $vencer = 0;
                    foreach ($dataAcuerdos as $Ac) {
                        $fecha = $Ac->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $vencer++;
                            $venceTotal++;
                            $pintafila = 1;
                        } else {
                            $pintafila = 0;
                        }
                        $Ac->pintafila = $pintafila;
                        //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                    }
                }
                $minutas->vencer = $vencer;
            }
            $planes->minutas = $dataMinutas;
            $planes->venceTotal = $venceTotal;
            $totalGeneralMS += $venceTotal;
        }

        $mesasTransversales = $this->PlanesModel->getByReunion(4);
        $totalGeneralMT = 0;
        foreach ($mesasTransversales as $planes) {
            $venceTotal = 0;
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
                if (count($dataAcuerdos) > 0) {
                    $vencer = 0;
                    foreach ($dataAcuerdos as $Ac) {
                        $fecha = $Ac->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $vencer++;
                            $venceTotal++;
                            $pintafila = 1;
                        } else {
                            $pintafila = 0;
                        }
                        $Ac->pintafila = $pintafila;
                        //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                    }
                }
                $minutas->vencer = $vencer;
            }
            $planes->minutas = $dataMinutas;
            $planes->venceTotal = $venceTotal;
            $totalGeneralMT += $venceTotal;
        }

        $data = array(
            'reunionMenu' => $this->reunionMenu,
            'dataMG' => $dataMG,
            'addMG' => $totalGeneralGM,
            'mesaPE' => $mesaPE,
            'addPE' => $totalGeneralPE,
            'mesasSectoriales' => $mesasSectoriales,
            'addMS' => $totalGeneralMS,
            'mesasTransversales' => $mesasTransversales,
            'addMT' => $totalGeneralMT
        );
        $this->load->view('general', $data);
    }


    public function mesaGabinete()
    {
        $today = new DateTime(date('Y-m-d'));
        $dataPlanes = $this->PlanesModel->getByReunion(1);
        foreach ($dataPlanes as $planes) {
            $venceTotal=0;
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
                if (count($dataAcuerdos) > 0) {
                    $vencer = 0;
                    foreach ($dataAcuerdos as $Ac) {
                        $fecha = $Ac->fecha;
                        $fecha = new DateTime($fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $vencer++;
                            $venceTotal++;
                            $pintafila = 1;
                        } else {
                            $pintafila = 0;
                        }
                        $Ac->pintafila = $pintafila;
                        //echo "Total a vencer de este acuerdo: ".$vencer."<br>";
                    }
                }
                $minutas->vencer = $vencer;
            }
            $planes->minutas = $dataMinutas;
            $planes->venceTotal = $venceTotal;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu' => $this->reunionMenu,
            'planes' => $dataPlanes
        );
        $this->load->view('mesaGabinete', $data);
    }

    public function mesaProyectosEstrategicos()
    {
        $dataPlanes = $this->PlanesModel->getByReunion(2);
        foreach ($dataPlanes as $planes) {
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu' => $this->reunionMenu,
            'planes' => $dataPlanes
        );
        $this->load->view('mesaProyectosEstrategicos', $data);
    }

    public function mesasSectoriales()
    {
        $dataPlanes = $this->PlanesModel->getByReunion(3);
        foreach ($dataPlanes as $planes) {
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu' => $this->reunionMenu,
            'planes' => $dataPlanes
        );
        $this->load->view('mesasSectoriales', $data);
    }

    public function mesasTransversales()
    {
        $dataPlanes = $this->PlanesModel->getByReunion(4);
        foreach ($dataPlanes as $planes) {
            $dataMinutas = $this->MinutasModel->getByIdPlanVisible($planes->idMv);
            foreach ($dataMinutas as $minutas) {
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu' => $this->reunionMenu,
            'planes' => $dataPlanes
        );
        $this->load->view('mesasTransversales', $data);
    }

}

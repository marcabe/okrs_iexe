<?php require_once 'complementos/head.php' ?>
<?php header('Access-Control-Allow-Origin: *'); ?>


<script src="<?php echo base_url(); ?>assets/build/js/detalle_objetivo.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/detalle_objetivos.css" rel="stylesheet">

<style>

    select{
        border-radius: 50px !important;
        border-color: #00B2A9 !important;
        color: #00B2A9 !important;
        font-size: 12px;
    }

	.pf{
		padding: 10px 15px;
		background: #f5f5f5;
		border-top: 1px solid #ddd;
		border-bottom: 1px solid #00B2A9;
		border-left: 1px solid #00B2A9;
		border-right: 1px solid #00B2A9;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;
	}
	.panel{
		margin-bottom: 0 !important;
	}

	/*****************Chat******************/
	.panel-primary {
		border-color: #00B2A9 !important;
	}
	.panel-primary>.panel-heading {
		color: #fff;
		background-color: #00B2A9;
		border-color: #00B2A9;
	}
	.enviarChat{
		background: #00B2A9;
	}

	.PlanChat{
		height: 350px!important;
		/*background-image: url('assets/build/images/chat.jpg');*/
        background: #cedce7 !important;
		overflow: scroll;
	}
	.mensajeChatDer, .mensajeChatIzq{
		margin-right: 0px !important;
		background: white !important;
		padding: 5px 10px !important;
		margin-bottom: 10px !important;
		border-radius: 7px !important;
	}
	.mensajeChatDer{
		float: right !important;
	}
	.mensajeChatDer label{
		color: #FF6A13;
	}
	.mensajeChatIzq label{
		color: crimson;
	}
	ul{
		list-style-type: none;

	}


	/***************************************************/

	.tituloPlanes{
		cursor: pointer !important;
		background: #00B2A9 !important;
		border-radius: 4px !important;
		color: white !important;
		padding: 10px 0px 10px 0px !important;
	}



	.tituloPlanes h2{
		color: white !important;
	}

</style>

</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php //require_once 'complementos/menu.php' ?>

		<?php //require_once 'complementos/topnavigation.php' ?>


		<div class="right_col" role="main">-->
<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
	   style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i');?>" style="display: none;">
			<div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Mis proyectos prioritarios</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">

                            <div class="row x_title">
                                <div class="col-md-6">
                                    <!--<h3>Graficas Generales de los Planes
                                    </h3>-->
                                </div>
                            </div>

                            <?php
                            if (count($planesGraf) > 0)
                                foreach ($planesGraf as $pl) {
                                    ?>
                                    <div style="cursor: pointer; margin-top: 25px;" title="<?php echo $pl->idMv;?>" class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas planess">
                                        <div id='<?php echo "gauge" . $pl->idMv; ?>'></div>
                                        <h5  style="font-weight: normal; font-size: 12px;"><?php echo $pl->mv;?></h5>

                                    </div>
                                <?php } ?>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <br/>

            </div>
		<!--</div>
		<?php //require_once 'complementos/footer.php' ?>-->

		<script>
			$(document).ready(function () {
				<?php if (count($keyresult) > 0)
				foreach ($keyresult as $kr) { ?>
				var g = new JustGage({
					id: "<?php echo 'gauge' . $kr->idKeyResult?>",
					value: <?php echo $kr->avance?>,
					min: <?php echo $kr->medicioncomienzo?>,
					max: <?php echo $kr->medicionfinal?>,
					titlePosition: "below",
					valueFontColor: "#3f4c6b",
					pointer: true,
                    customSectors:
                        [{
                            color: "#931623",
                            lo: 0,
                            hi: 24.999
                        }, {
                            color: "#f07622",
                            lo: 25,
                            hi: 49.99
                        }, {
                            color: "#f7b21d",
                            lo: 50,
                            hi: 74.99
                        }, {
                            color: "#3eb049",
                            lo: 75,
                            hi: 100
                        }],
					pointerOptions: {
						toplength: -15,
						bottomlength: 10,
						bottomwidth: 12,
						color: '#8e8e93',
						stroke: '#ffffff',
						stroke_width: 3,
						stroke_linecap: 'round'
					},
					relativeGaugeSize: true,

				});
				<?php } ?>

                <?php if($objetivo[0]->anual!=1){ ?>
				$(".krr").click(function () {
					$.ajax({
						type: "POST",
						url: 'detalle_keyresult/'+$(this).attr('title'),
						data:{},
						success: function(datos){
							$('#carga').empty();
							$('#carga').html(datos);
						},
                        xhr: function(){
                            var xhr = $.ajaxSettings.xhr() ;
                            xhr.onloadstart = function(e) {
                                $("#fondoLoader").show();
                                console.log("Esta cargando");
                            };
                            xhr.onloadend = function (e) {
                                $("#fondoLoader").fadeOut(500);
                                console.log("Termino de cargar");
                            }
                            return xhr ;
                        }
					});
				});
				<?php }else{ ?>
                $(".krr").click(function () {
                    $.ajax({
                        type: "POST",
                        url: 'detalle_keyresultI/'+$(this).attr('title'),
                        data:{},
                        success: function(datos){
                            $('#carga').empty();
                            $('#carga').html(datos);
                        },
                        xhr: function(){
                            var xhr = $.ajaxSettings.xhr() ;
                            xhr.onloadstart = function(e) {
                                $("#fondoLoader").show();
                                console.log("Esta cargando");
                            };
                            xhr.onloadend = function (e) {
                                $("#fondoLoader").fadeOut(500);
                                console.log("Termino de cargar");
                            }
                            return xhr ;
                        }
                    });
                });

                <?php } ?>

			});
		</script>

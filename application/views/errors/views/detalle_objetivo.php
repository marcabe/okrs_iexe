<?php require_once 'complementos/head.php' ?>
<?php header('Access-Control-Allow-Origin: *'); ?>


<script src="<?php echo base_url(); ?>assets/build/js/detalle_objetivo.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/detalle_objetivos.css" rel="stylesheet">

<style>

    select {
        border-radius: 50px !important;
        border-color: #00B2A9 !important;
        color: #00B2A9 !important;
        font-size: 12px;
    }

    .pf {
        padding: 10px 15px;
        background: #f5f5f5;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #00B2A9;
        border-left: 1px solid #00B2A9;
        border-right: 1px solid #00B2A9;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }

    .panel {
        margin-bottom: 0 !important;
    }

    /*****************Chat******************/
    .panel-primary {
        border-color: #03847D !important;
    }

    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #03847D;
        border-color: #03847D;
    }

    .enviarChat {
        background: #03847D;
    }

    .PlanChat {
        height: 350px !important;
        /*background-image: url('assets/build/images/chat.jpg');*/
        background: #cedce7 !important;
        overflow: scroll;
    }

    .mensajeChatDer, .mensajeChatIzq {
        margin-right: 0px !important;
        background: white !important;
        padding: 5px 10px !important;
        margin-bottom: 10px !important;
        border-radius: 7px !important;
    }

    .mensajeChatDer {
        float: right !important;
    }

    .mensajeChatDer label {
        color: #FF6A13;
    }

    .mensajeChatIzq label {
        color: crimson;
    }

    ul {
        list-style-type: none;

    }

    /***************************************************/

    .tituloPlanes {
        cursor: pointer !important;
        background: #03847D !important;
        border-radius: 4px !important;
        color: white !important;
        padding: 10px 0px 10px 0px !important;
    }

    .tituloPlanes h2 {
        color: white !important;
    }

</style>

</head>

<!--<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php //require_once 'complementos/menu.php' ?>

		<?php //require_once 'complementos/topnavigation.php' ?>


		<div class="right_col" role="main">-->
<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
       style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
<div class="">
    <div class="page-title">
        <div class="title_left">
            <!--<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">-->
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-xs-12 text-center">
                        <button style="background: #003B5C; color: white; border-radius: 7px; border-color: #003B5C; " class="btn planess btn-xs" title="<?php echo $mv[0]->idMv;?>"> Regresar</button>
                    </div>
                    <div class="clearfix"></div>


                    <div class="tituloPlanes row accordion-toggle" data-toggle="collapse" href="#"
                         style="margin-top: 20px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-xs-12">
                                <div class="col-md-12">
                                    <?php if (isset($vencerTotal) && $vencerTotal > 0) { ?>
                                        <sup
                                                style="background: #ED2B00 ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px;margin-left: -25px;">
                                            <?php echo $vencerTotal; ?>
                                        </sup>
                                    <?php } ?>
                                </div>
                                <h2><b>Proyecto:</b> <?php echo $mv[0]->mv; ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <!-- start accordion -->
                    <div class="tituloObjetivos row accordion-toggle" data-toggle="collapse" href="#">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="col-md-12">
                                <p><b style="font-size: 16px;">Objetivo anual</b>: <?php echo $objetivo[0]->objetivo ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-8 col-xs-12">
                            <div class="project_progress">
                                <small><?php echo $objetivo[0]->avance ?>% Avance</small>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-blue" role="progressbar"
                                         data-transitiongoal="3" aria-valuenow="56"
                                         style="width: <?php echo $objetivo[0]->avance ?>%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="">
                        <div class="col-md-12 subtitulos">
                            <div class="col-md-12">
                                <h4>Objetivo estratégico</h4>
                                <p><?php echo $objetivo[0]->descripcion ?></p>
                            </div>
                            <div class="col-md-9 col-xs-12">
                                <div class="col-xs-12" style="margin-top: 10px;">
                                    <?php
                                    if (count($keyresult) > 0)
                                        foreach ($keyresult as $kr) {
                                            ?>
                                            <div class="col-xs-0 col-md-2" style="margin-top: 20px;"></div>
                                            <div class="col-xs-12 col-md-4 text-center krr" style="margin-top: 20px;cursor: pointer;" title="<?php echo $kr->idKeyResult; ?>" >
                                                <div class="col-md-12 col-xs-12" style="background: #F7F7F7; border-width: 2px; border-radius: 10px 10px 0px 0px; <?php echo ($kr->vencer==1)? 'border-color: #f07622!important;':'border-color: #03847d;';?> border-style: solid; padding: 0;">
                                                    <div class="col-xs-12 col-xs-offset-0" id='<?php echo "gauge" . $kr->idKeyResult; ?>' ></div>
                                                </div>
                                                <div class="col-md-12 col-xs-12" style="background: #F7F7F7; border-width: 2px; border-radius: 0px 0px 10px 10px; <?php echo ($kr->vencer==1)? 'border-color: #f07622!important;':'border-color: #03847d;';?>  border-style: solid; padding: 0;">
                                                    <div class="col-md-12 col-xs-12" style="<?php echo ($kr->vencer==1)? 'background: #f07622!important;':'background: #03847d;';?> color: white; margin-top: 0px; border-radius: 0px 0px 8px 8px; height: 95px; font-size: 10px;">
                                                        <label class="col-md-12 col-xs-12 text-jusify"><?php echo (strlen($kr->descripcion) > 90) ? substr($kr->descripcion, 0, 90) . "..." : $kr->descripcion; ?></label>
                                                        <label class="col-md-12 col-xs-12 text-center"><?php echo $kr->fechanew; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?></div>


                            </div>
                            <input value="<?php echo $objetivo[0]->idObjetivo; ?>" style="display: none;"
                                   id="idObjetivo">
                            <div class="col-md-3 col-xs-12" style="margin-top:45px">
                                <div class="panel panel-primary">

                                    <div class="panel-heading" style="height: 90px;">
                                        <div class="col-md-12 text-center"
                                             style="font-weight: bold; margin-bottom: 10px; width: 100%; text-align: center;">
                                            Comunicación <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-12" style="float: left;">
                                            <select class="form-control listaLider"
                                                    id="listaLider<?php echo $objetivo[0]->idObjetivo; ?>"
                                                    name="<?php echo $objetivo[0]->idObjetivo; ?>">
                                                <?php foreach ($usuariosAdmin as $usChat) { ?>
                                                    <option value="<?php echo $usChat->user; ?>"><?php echo $usChat->nombre; ?>
                                                        (<?php echo $usChat->nombrePuesto; ?>)
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="panel-body PlanChat"
                                         id="PlanChat<?php echo $objetivo[0]->idObjetivo; ?>">
                                        <ul id="chatObj<?php echo $objetivo[0]->idObjetivo; ?>">
                                            <?php foreach ($chat as $ch) { ?>
                                                <li>
                                                    <div class="col-md-12 mensajeChatDer"
                                                         style="width: 120% !important;">
                                                        <label><?php echo $ch->nombre; ?></label>
                                                        <p><?php echo $ch->mensaje; ?></p>
                                                        <small
                                                                class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="pf">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                            <input type="text" class="form-control"
                                                   id="mensajeChat<?php echo $objetivo[0]->idObjetivo; ?>"
                                                   placeholder="Escribir mensaje">
                                        </div>
                                        <br><br>

                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                            <button class="btn btn-primary enviarChat"
                                                    text="<?php echo $objetivo[0]->idObjetivo; ?>">
                                                Enviar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                    <!-- end of accordion -->
                </div>

            </div>
        </div>
    </div>
</div>
<!--</div>
		<?php //require_once 'complementos/footer.php' ?>-->

<script>
    $(document).ready(function () {
        <?php
        if (count($keyresult) > 0)
        foreach ($keyresult as $kr) {
        ?>
        var g = new JustGage({
            id: "<?php echo 'gauge' . $kr->idKeyResult?>",
            value: <?php echo $kr->avance?>,
            min: <?php echo $kr->medicioncomienzo?>,
            max: <?php echo $kr->medicionfinal?>,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            customSectors:
                [{
                    color: "#931623",
                    lo: 0,
                    hi: 24.999
                }, {
                    color: "#f07622",
                    lo: 25,
                    hi: 49.99
                }, {
                    color: "#f7b21d",
                    lo: 50,
                    hi: 74.99
                }, {
                    color: "#3eb049",
                    lo: 75,
                    hi: 100
                }],
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });
        <?php } ?>

        <?php if($objetivo[0]->anual != 1){ ?>
        $(".krr").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_keyresult/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });
        <?php }else{ ?>
        $(".krr").click(function () {
            $.ajax({
                type: "POST",
                url: 'KeyResultController/detalleKrPrincipal/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

        <?php } ?>

        $(".planess").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_plan/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');

                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

    });
</script>

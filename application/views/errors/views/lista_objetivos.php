<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("America/Mexico_City");

?>

<script src="<?php echo base_url(); ?>assets/build/js/lista_objetivos.js"></script>

<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"
      rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/lista_objetivos.css" rel="stylesheet">

</head>
<style>
    .filaAccion {
        background: #EFE7D5 !important;
    }
</style>
<!--<p class="network-status info-box success">You're online! 😄</p>-->
<body class="nav-md">
<input type="text" value="<?php echo $this->session->userdata('tipo'); ?>" style="display: none;" id="tipoUsuario">
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">-->
                        <h3>Módulo Objetivos</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix"></div>
                            </div>

                            <?php
                            if ($objetivos == null) {
                                echo "<h4>Sin asignación.</h4>";
                            } else
                                foreach ($objetivos as $obj) {
                                    ?>
                                    <div class="x_content listaObjetivos<?php echo $obj->idObjetivo; ?>">
                                        <!-- start accordion -->
                                        <div class="col-md-10">
                                            <div class="tituloObjetivos row">
                                                <div class="row alertAvance">
                                                    <div class="col-md-12">
                                                        <?php if (isset($obj->totalActualizar) && $obj->totalActualizar > 0) { ?>
                                                            <sup
                                                                    style="background: #ED2B00 ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px;">
                                                                <?php echo $obj->totalActualizar; ?>
                                                            </sup>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12 centrado">
                                                    <div class="col-md-12">
                                                        <?php if(isset($obj->vencerOkrBi))
                                                            if($obj->vencerOkrBi >= 1 ){ ?>
                                                        <div class="col-md-1">
                                                            <div class="col-md-6 text-center"
                                                                 style="border-radius: 25px !important; background: green; background: #f07622; color: white; font-weight: bold; padding: 5px;">
                                                                <?php echo $obj->vencerOkrBi;?>
                                                            </div>
                                                        </div>
                                                            <?php } ?>
                                                        <?php
                                                        if ($this->session->userdata('tipo') != 'capturista') {
                                                            if ($this->session->userdata('tipo') == 'admin') { ?>
                                                                <div class="col-md-3 text-right" style="margin-top: 5px;">
                                                                    <button class="btn btn-primary btn-xs editobjt"
                                                                            onclick="editarObjetivo('<?php echo $obj->idObjetivo; ?>')">
                                                                        <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                                        objetivo
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-3 text-right" style="margin-top: 5px;">
                                                                    <?php if ($obj->anual == 1) { ?>

                                                                        <button class="btn btn-warning btn-xs addkr"
                                                                                style="background: #6f15ae !important; border-color: #6f15ae !important;"
                                                                                onclick="agregarKeyB('<?php echo $obj->idObjetivo; ?>')">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                            OKR Bimestral
                                                                        </button>
                                                                    <?php } else { ?>
                                                                        <button class="btn btn-warning btn-xs addkr"
                                                                                onclick="agregarKey('<?php echo $obj->idObjetivo; ?>')">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                            Agregar Key Results
                                                                        </button>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } elseif ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                <div class="col-md-2 text-right" style="margin-top: 5px;">
                                                                    <?php if ($obj->anual == 1) { ?>
                                                                        <button class="btn btn-warning btn-xs addkr"
                                                                                style="background: #f7b21d!important; border-color: #f7b21d!important;"
                                                                                onclick="agregarKeyB('<?php echo $obj->idObjetivo; ?>')">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                            OKR Bimestral
                                                                        </button>
                                                                    <?php } else { ?>
                                                                        <button class="btn btn-warning btn-xs addkr"
                                                                                style="background: #0094c9!important; border-color: #0094c9 !important;"
                                                                                onclick="agregarKey('<?php echo $obj->idObjetivo; ?>')">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                            Agregar Key Results
                                                                        </button>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="col-md-2 text-right" style="margin-top: 5px;">
                                                                    <button class="btn btn-primary btn-xs editobjt"
                                                                            style="background: #0094c9!important; border-color: #0094c9 !important;"
                                                                            onclick="editarObjetivo('<?php echo $obj->idObjetivo; ?>')">
                                                                        <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                                        Objetivo
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-2 text-right" style="margin-top: 5px;">
                                                                    <button class="btn btn-danger btn-xs eliminaObjetivo"
                                                                            style="background: #931623!important; border-color: #931623!important; color: white !important;"
                                                                            value='<?php echo $obj->idObjetivo; ?>' >
                                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                        Eliminar
                                                                        Objetivo
                                                                    </button>
                                                                </div>

                                                            <?php }
                                                        } ?>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr style="color: red !important; margin-top: 10px;">
                                                    </div>


                                                    <div class="col-md-12 ico" data-toggle="collapse"
                                                         href="#<?php echo $obj->idObjetivo; ?>">
                                                        <div class="col-md-12">
                                                            <div class="col-md-11 col-md-offset-1">
                                                                <h2 style="font-weight: bold;">Proyecto: <b
                                                                            style="font-weight: normal;"><?php echo $obj->proyecto; ?></b>
                                                                </h2>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <i
                                                                        class="fa fa-arrow-circle-down iconito"
                                                                        style="font-size: 25px;"
                                                                        aria-hidden="true"></i>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <h2 style="font-weight: bold;">
                                                                    Objetivo<?php echo ($obj->anual == 1) ? " anual" : ""; ?>
                                                                    : <b
                                                                            style="font-weight: normal;"><?php echo $obj->objetivo ?></b>
                                                                </h2>
                                                            </div>
                                                            <div class="col-md-11 col-md-offset-1">
                                                                <h2><?php echo $obj->descripcion ?></h2>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <h6>Avance OKR</h6>
                                            <div id="gauge<?php echo $obj->idObjetivo; ?>" class="grafi"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="<?php echo $obj->idObjetivo; ?>" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <div class="col-md-8">
                                                        <table class="table table-striped table-condensed resp_table demo">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 60%!important;"
                                                                    class="text-left th_planes" id="th_indicador"><i
                                                                            class="fa fa-caret-square-o-right"
                                                                            aria-hidden="true"> </i> Key Result
                                                                </th>
                                                                <!--th class="text-left th_planes" id="th_indicador">Metrica</th>-->
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_avance"><i
                                                                            class="fa fa-line-chart"
                                                                            aria-hidden="true"></i>
                                                                    Avance
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Progreso
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vi"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i> Valor
                                                                    Inicial
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vf"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i> Valor
                                                                    Final
                                                                </th>
                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                    <th></th>
                                                                <?php } ?>
                                                                <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                    <th></th>
                                                                <?php } ?>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            //if (count($obj) > 0)
                                                            foreach ($obj->kr as $kr) {
                                                            ?>
                                                            <tr class="listaKr<?php echo $kr->idKeyResult; ?>" >
                                                                <td style="cursor: pointer;"
                                                                    id="descripcion<?php echo $kr->idKeyResult; ?>"
                                                                    data-toggle="collapse"
                                                                    href="#kri<?php echo $kr->idKeyResult; ?>">
                                                                    <?php if (isset($kr->acciones)) { ?>
                                                                        <?php if($kr->vencerInt>=1){?>
                                                                        <div class="col-md-1 text-center"
                                                                             style="border-radius: 25px !important; background: green; background: #f07622; color: white; font-weight: bold; padding: 2px; font-size: 12px;">
                                                                            <?php echo $kr->vencerInt;?>
                                                                        </div>
                                                                        <?php } ?>
                                                                        <i class="fa fa-arrow-circle-down iconito"
                                                                           style="font-size: 15px;"
                                                                           aria-hidden="true"></i>
                                                                    <?php } ?>
                                                                    <?php echo $kr->descripcion." <b>( ".$kr->ponderacion." ponderación )</b> "; ?>
                                                                </td>
                                                                <td id="avance<?php echo $kr->idKeyResult; ?>"><?php echo $kr->avance; ?></td>
                                                                <td>
                                                                    <div class="project_progress">
                                                                        <small
                                                                                id="small<?php echo $kr->idKeyResult; ?>"><?php echo number_format($kr->avancePorcentaje, 2); ?>
                                                                            %
                                                                        </small>

                                                                        <?php if (count($kr->bitacora) > 0 AND $kr->bitacora[0]->aprobado == 0) { ?>
                                                                            <small style="color: red;"
                                                                                   id="marcadorAvance<?php echo $kr->idKeyResult; ?>">
                                                                                ( <?php echo $kr->avancePorcentaje; ?>%
                                                                                sin autorizar )
                                                                            </small>
                                                                        <?php } else { ?>
                                                                            <small
                                                                                    id="marcadorAvance<?php echo $kr->idKeyResult; ?>"></small>
                                                                        <?php } ?>


                                                                        <div class="progress progress_sm">
                                                                            <div class="progress-bar bg-green"
                                                                                 id="barraProgreso<?php echo $kr->idKeyResult; ?>"
                                                                                 role="progressbar"
                                                                                 data-transitiongoal="<?php echo $kr->avancePorcentaje; ?>"
                                                                                 aria-valuenow="56"
                                                                                 style="width: 57%;"></div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td id="keyresultinicio<?php echo $kr->idKeyResult; ?>"><?php echo $kr->medicioncomienzo; ?></td>
                                                                <td id="keyresultfinal<?php echo $kr->idKeyResult; ?>"><?php echo $kr->medicionfinal; ?></td>
                                                                <?php if ($obj->anual != 1) { ?>

                                                                    <td>
                                                                        <?php if ($kr->avance >= $kr->medicionfinal) { ?>
                                                                            <p>Completado al 100%</p>
                                                                        <?php } else { ?>
                                                                            <button
                                                                                    id="updateAvance<?php echo $kr->idKeyResult; ?>"
                                                                                    class="btn btn-info btn-xs updateAvance"
                                                                                    value="<?php echo $kr->idKeyResult; ?>">
                                                                                <i
                                                                                        class="fa fa-pencil"></i>
                                                                                Actualizar
                                                                            </button>
                                                                        <?php } ?>
                                                                    </td>
                                                                <?php } ?>

                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                    <td>
                                                                        <?php
                                                                        if (count($kr->bitacora) > 0) {
                                                                            if ($kr->bitacora[0]->aprobado == 2) {
                                                                                echo
                                                                                    "<button value='$kr->idKeyResult' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado" . $kr->idKeyResult . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
                                                                            } elseif ($kr->bitacora[0]->aprobado == 0) {
                                                                                echo
                                                                                    "<button class='btn btn-danger btn-xs autorizaAvance' id='autorizaAvance" . $kr->idKeyResult . "' value=" . $kr->bitacora[0]->idBitacora . ">
																						<i class='fa fa-pencil'></i> Autorizar
																					</button>";
                                                                            }

                                                                        } ?>
                                                                    </td>
                                                                <?php } elseif ($this->session->userdata('tipo') == 'capturista') { ?>
                                                                    <td>
                                                                        <?php
                                                                        if (count($kr->bitacora) > 0) {
                                                                            if ($kr->bitacora[0]->aprobado == 2) {
                                                                                echo
                                                                                    "<button value='$kr->idKeyResult' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado" . $kr->idKeyResult . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
                                                                            }
                                                                        } ?>
                                                                    </td>
                                                                <?php } ?>
                                                                <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                    <td>

                                                                        <ul class="nav navbar-right panel_toolbox">
                                                                            <li class="dropdown open">
                                                                                <a href="#" class="dropdown-toggle"
                                                                                   data-toggle="dropdown" role="button"
                                                                                   aria-expanded="true"
                                                                                   style="color: black;"><i
                                                                                            class="fa fa-gear"></i>
                                                                                    Config.</a>
                                                                                <ul class="dropdown-menu" role="menu">

                                                                                    <?php if ($obj->anual != 1) { ?>

                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; cursor: pointer;">
                                                                                            <label style="cursor: pointer;"
                                                                                                    class="bitacoraLista"
                                                                                                    id="bitacora<?php echo $kr->idKeyResult; ?>"
                                                                                                    name="<?php echo $kr->idKeyResult; ?>">
                                                                                                <i class="fa fa-pencil"></i>
                                                                                                Bitácora
                                                                                            </label>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: #00B2A9; cursor: pointer;">
                                                                                        <label class="editarOkrBI"
                                                                                               style="cursor: pointer;"
                                                                                               name='<?php echo $kr->idKeyResult; ?>'>
                                                                                            <i class="fa fa-pencil"
                                                                                               aria-hidden="true"></i>
                                                                                            Editar
                                                                                        </label>
                                                                                    </li>
                                                                                    <?php if ($obj->anual == 1) { ?>
                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; color: dodgerblue; cursor: pointer;">
                                                                                            <label class="addkri"
                                                                                                   style="cursor: pointer;"
                                                                                                   name='<?php echo $kr->idKeyResult; ?>'>
                                                                                                <i class="fa fa-plus-circle"
                                                                                                   aria-hidden="true"></i>
                                                                                                OKR Interno
                                                                                            </label>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: red; cursor: pointer;">
                                                                                        <label class="justificacion"
                                                                                               style="cursor: pointer;color:green;"
                                                                                               name='<?php echo $kr->idKeyResult; ?>'>
                                                                                            <i class="fa fa-trash-o"
                                                                                               aria-hidden="true"></i>
                                                                                            Justificación
                                                                                        </label>
                                                                                    </li>
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: red; cursor: pointer;">
                                                                                        <label class="eliminaKr"
                                                                                               style="cursor: pointer;"
                                                                                               name='<?php echo $kr->idKeyResult; ?>'>
                                                                                            <i class="fa fa-trash-o"
                                                                                               aria-hidden="true"></i>
                                                                                            Eliminar
                                                                                        </label>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                <?php } elseif ($this->session->userdata('tipo') == 'admin') { ?>
                                                                    <td>

                                                                        <ul class="nav navbar-right panel_toolbox">
                                                                            <li class="dropdown open">
                                                                                <a href="#" class="dropdown-toggle"
                                                                                   data-toggle="dropdown" role="button"
                                                                                   aria-expanded="true"
                                                                                   style="color: black;"><i
                                                                                            class="fa fa-gear"></i>
                                                                                    Config.</a>
                                                                                <ul class="dropdown-menu" role="menu">
                                                                                    <?php if ($obj->anual != 1) { ?>

                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; cursor: pointer;">
                                                                                            <label
                                                                                                    class="bitacoraLista"
                                                                                                    style="cursor: pointer;"
                                                                                                    id="bitacora<?php echo $kr->idKeyResult; ?>"
                                                                                                    name="<?php echo $kr->idKeyResult; ?>">
                                                                                                <i class="fa fa-pencil"></i>
                                                                                                Bitácora
                                                                                            </label>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: #00B2A9; cursor: pointer;">
                                                                                        <label class="editarOkrBI"
                                                                                               style="cursor: pointer;"
                                                                                               name='<?php echo $kr->idKeyResult; ?>'>
                                                                                            <i class="fa fa-pencil"
                                                                                               aria-hidden="true"></i>
                                                                                            Editar
                                                                                        </label>
                                                                                    </li>
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: red; cursor: pointer;">
                                                                                        <label class="justificacion"
                                                                                               style="cursor: pointer;color:green;"
                                                                                               name='<?php echo $kr->idKeyResult; ?>'>
                                                                                            <i class="fa fa-trash-o"
                                                                                               aria-hidden="true"></i>
                                                                                            Justificación
                                                                                        </label>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <?php if (isset($kr->acciones)){ ?>
                                                            <tbody id="kri<?php echo $kr->idKeyResult; ?>"
                                                                   class="accordion-body collapse">
                                                            <tr style="background: #cedce7;">
                                                                <td colspan="7"><h6><strong>OKR interno</strong></h6>
                                                                </td>
                                                            </tr>
                                                            <?php foreach ($kr->acciones as $acciones) { ?>
                                                                <tr class="filaAccion" <?php echo ($acciones->pintafila==1)? "style='background: #f07622!important; opacity: 0.7;color: white !important;'":"" ; ?>
                                                                    id="filaAccion<?php echo $acciones->idAcciones; ?>">
                                                                    <td id="columAccion<?php echo $acciones->idAcciones; ?>"><?php echo $acciones->accion; ?></td>
                                                                    <td id="avanceAcciones<?php echo $acciones->idAcciones; ?>"><?php echo $acciones->avance; ?></td>
                                                                    <td>
                                                                        <div class="project_progress">
                                                                            <small id="smallAccion<?php echo $acciones->idAcciones; ?>"><?php echo $acciones->avance; ?>
                                                                                %
                                                                            </small>
                                                                            <small id="marcadorAvanceAcciones<?php echo $acciones->idAcciones; ?>"></small>
                                                                            <div class="progress progress_sm">
                                                                                <div class="progress-bar bg-green"
                                                                                     id="barraProgresoAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                     role="progressbar"
                                                                                     data-transitiongoal="<?php echo $acciones->avance; ?>"
                                                                                     aria-valuenow="56"
                                                                                     style="width: 57%;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>0</td>
                                                                    <td>100</td>
                                                                    <td>
                                                                        <?php if ($acciones->avance >= 100) { ?>
                                                                            <p>Completado al 100%</p>
                                                                        <?php } else { ?>
                                                                            <button
                                                                                    id="updateAvanceAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                    class="btn btn-info btn-xs updateAvanceAcciones"
                                                                                    value="<?php echo $acciones->idAcciones; ?>">
                                                                                <i
                                                                                        class="fa fa-pencil"></i>
                                                                                Actualizar
                                                                            </button>
                                                                        <?php } ?>

                                                                        <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                            <?php
                                                                            if (count($acciones->bitacoraA) > 0) {
                                                                                if ($acciones->bitacoraA[0]->aprobado == 2) {
                                                                                    echo
                                                                                        "<button value='$acciones->idAcciones' class='btn btn-warning btn-xs avanceCanceladoAccion' id='avanceCanceladoAccion" . $acciones->idAcciones . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
                                                                                } elseif ($acciones->bitacoraA[0]->aprobado == 0) {
                                                                                    echo
                                                                                        "<button class='btn btn-danger btn-xs autorizaAvanceAccion' id='autorizaAvanceAccion" . $acciones->idAcciones . "' value=" . $acciones->bitacoraA[0]->idBitacora . ">
																						<i class='fa fa-pencil'></i> Autorizar
																					</button>";
                                                                                }

                                                                            } ?>
                                                                        <?php } elseif ($this->session->userdata('tipo') == 'capturista') {
                                                                        if (count($acciones->bitacoraA) > 0) {
                                                                            if ($acciones->bitacoraA[0]->aprobado == 2) {
                                                                                echo
                                                                                    "<button value='$acciones->idAcciones' class='btn btn-warning btn-xs avanceCanceladoAccion' id='avanceCanceladoAccion" . $acciones->idAcciones . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
                                                                            }
                                                                        }
                                                                        } ?>


                                                                        <!--<button style="background: #a849a3 !important; border-color: #a849a3;"
                                                                                class="btn btn-danger btn-xs bitacoraListaAcciones"
                                                                                id="bitacoraAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                value="<?php echo $acciones->idAcciones; ?>">
                                                                            <i class="fa fa-pencil"></i> Bitácora
                                                                        </button>-->
                                                                        <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>


                                                                            <!--<button class="btn btn-danger btn-xs editarOkrI"
                                                                                    value='<?php echo $acciones->idAcciones; ?>'>
                                                                                <i class="fa fa-plus-circle"
                                                                                   aria-hidden="true"></i> Editar
                                                                            </button>-->
                                                                            <!--<button class="btn btn-danger btn-xs eliminaAccion"
                                                                                    value='<?php echo $acciones->idAcciones; ?>'>
                                                                                <i class="fa fa-trash-o"
                                                                                   aria-hidden="true"></i> Eliminar
                                                                            </button>-->
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td>
                                                                        <ul class="nav navbar-right panel_toolbox">
                                                                            <li class="dropdown open">
                                                                                <a href="#" class="dropdown-toggle"
                                                                                   data-toggle="dropdown" role="button"
                                                                                   aria-expanded="true"
                                                                                   style="color: black;"><i
                                                                                            class="fa fa-gear"></i>
                                                                                    Config.</a>
                                                                                <ul class="dropdown-menu" role="menu">
                                                                                    <li style="height: 25px; font-size: 15px; padding-left: 12px; color: #00B2A9; cursor: pointer;">
                                                                                        <label class="editarOkrI"
                                                                                               style="cursor: pointer;"
                                                                                               name='<?php echo $acciones->idAcciones; ?>'>
                                                                                            <i class="fa fa-plus-circle"
                                                                                               aria-hidden="true"></i>
                                                                                            Editar
                                                                                        </label>
                                                                                    </li>
                                                                                    <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; color: mediumpurple; cursor: pointer;">
                                                                                            <label class="bitacoraListaAcciones"
                                                                                                   style="cursor: pointer;"
                                                                                                   id="bitacoraAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                                   name="<?php echo $acciones->idAcciones; ?>">
                                                                                                <i class="fa fa-pencil"></i>
                                                                                                Bitácora
                                                                                            </label>
                                                                                        </li>
                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; color: darkred; cursor: pointer;">
                                                                                            <label class="eliminaAccion"
                                                                                                   style="cursor: pointer;"
                                                                                                   name='<?php echo $acciones->idAcciones; ?>'>
                                                                                                <i class="fa fa-trash-o"
                                                                                                   aria-hidden="true"></i>
                                                                                                Eliminar
                                                                                            </label>
                                                                                        </li>

                                                                                    <?php } ?>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                            <tr style="background: gainsboro;"
                                                                id="bordeBajo<?php echo $kr->idKeyResult; ?>">
                                                                <td colspan="7"></td>
                                                            </tr>
                                                            </tbody>


                                                            <?php } ?>
                                                            <?php } ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="col-md-12">
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading" style="height: 90px;">
                                                                    <div class="col-md-12 text-center"
                                                                         style="font-weight: bold; margin-bottom: 10px;">
                                                                        Comunicación <i class="fa fa-envelope"
                                                                                        aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="col-md-6" style="float: left;">
                                                                        <select style="10px;"
                                                                                class="form-control listaLider"
                                                                                id="listaLider<?php echo $obj->idObjetivo; ?>"
                                                                                name="<?php echo $obj->idObjetivo; ?>">
                                                                            <?php if ($this->session->userdata('idRol') == 2) { ?>
                                                                                <option value="gob">Gobernador</option>
                                                                            <?php } ?>
                                                                            <?php if (isset($obj->UsuariosChat)) { ?>
                                                                                <?php foreach ($obj->UsuariosChat as $usChat) { ?>
                                                                                    <option value="<?php echo $usChat->user; ?>"><?php echo $usChat->nombre; ?>
                                                                                        (<?php echo $usChat->nombrePuesto; ?>
                                                                                        )
                                                                                    </option>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <div style="cursor: pointer;"
                                                                         class="col-md-6 text-right vaciarChat"
                                                                         name="<?php echo $obj->idObjetivo; ?>">
                                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                        Vaciar Buzón
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body PlanChat"
                                                                     id="PlanChat<?php echo $obj->idObjetivo; ?>">
                                                                    <ul id="chatObj<?php echo $obj->idObjetivo; ?>">
                                                                        <?php foreach ($obj->chat as $ch) { ?>
                                                                            <li>
                                                                                <div class="col-md-10 mensajeChatDer">
                                                                                    <label><?php echo $ch->nombre; ?>
                                                                                        <small style="color: #5B6770">
                                                                                            (<?php echo $ch->nombrePuesto; ?>
                                                                                            )
                                                                                        </small>
                                                                                    </label>
                                                                                    <p><?php echo $ch->mensaje; ?></p>
                                                                                    <small
                                                                                            class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
                                                                                </div>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="panel-footer">
                                                                    <div class="form-row align-items-center">
                                                                        <div class="col-sm-8 my-1">
                                                                            <input type="text" class="form-control"
                                                                                   id="mensajeChat<?php echo $obj->idObjetivo; ?>"
                                                                                   placeholder="Escribir mensaje">
                                                                        </div>
                                                                        <div class="col-auto my-1">
                                                                            <button class="btn btn-primary enviarChat"
                                                                                    text="<?php echo $obj->idObjetivo; ?>">
                                                                                Enviar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of accordion -->
                                        </div>
                                    </div>
                                <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloKr"></h4>

                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="actual"></span></label>
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="tipometrica"></span></label>
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="rango"></span></label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                <input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
                                       class="form-control col-md-7 col-xs-12 has-feedback-left">
                                <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                <small id="msj_avance" style="color: red;"></small>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                <input id="avancet" readonly
                                       class="form-control col-md-7 col-xs-12 has-feedback-left">
                                <span class="fa fa-plus form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                <small id="msj_descripcionavance" style="color: red;"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
                                Adjuntar evidencia
                            </button>
                        </div>
                        <div class="col-md-12 text-center">
                            <small id="mensajeAexos" style="color: red;"></small>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="project_progress" id="barraCarga" style="display: none;">
                                <small id="porcentajeProgreso">
                                </small>
                                <div class="progress progress_sm">
                                    <small id="porcentajeProgreso" style="color: red;"></small>
                                    <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                         data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row" id="formAnexo"></div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizar">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="aprobadoCancel" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label>Existe un avance no autorizado para esta Key Result</label>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="noAutorizar" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">No autorizar el avance de: <label id="avanceDe"></label>
                        </h4>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="motivoCancel"></label>
                        </div>
                    </div>
                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label>Motivo: </label>
                        </div>
                    </div>
                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <textarea id="motivo"></textarea>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" id="aceptarCancelacion"
                                    aria-label="Close">Aceptar
                            </button>
                        </div>
                        <div class="col-md-6 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalValida" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloavance"></h4>

                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="mensajeValida"></label>
                        </div>
                        <div class="form-group">
                            <label id="avanceValida"></label>
                        </div>
                        <div class="form-group">
                            <label id="descripcionValida"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <table class="table table-striped table-bordered" id="tablaAnexos">
                                <thead>
                                <td>Nombre de la evidencia</td>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-4 text-right">
                            <button class="btn btn-primary" id="btnAceptarValida">Autorizar</button>
                        </div>
                        <div class="col-md-4 text-center">
                            <button class="btn btn-danger" id="btnNoAutorizar" class="close" data-dismiss="modal"
                                    aria-label="Close">No autorizar
                            </button>
                        </div>
                        <div class="col-md-4 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalClave" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="leyendaEliminar">Esta apunto de eliminar un objetivo</label><br>
                            <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                        </div>
                        <div class="form-group">
                            <input type="password" id="contraseña">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="validaClave">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="cerraClave">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalClaveEditarKr" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="leyendaEliminar">Esta apunto de editar un Key Result</label><br>
                            <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                        </div>
                        <div class="form-group">
                            <input type="password" id="contraseñaEdit">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="validaClaveEdit">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="cerraClaveEdit">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>"
               style="display: none;">
        <div class="modal fade" id="myModalDic" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="tipometricaDic"></span></label>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <label>¿Desea finalizar el Key Result con el 100%? </label>
                            </div>
                            <br>
                            <div class="radio col-md-6 text-right">
                                <input type="radio" name="dico" value="Si">Si
                            </div>
                            <div class="radio col-md-6 text-left">
                                <input type="radio" name="dico" value="No">No
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Avance:
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control col-md-7 col-xs-12"
                                              id="descripcionavanceDic"></textarea>
                                <small id="msj_descripcionavanceDic"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptarDic">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizarDic">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="modalCancelado" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloCancelado"></h4>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="usuarioCancelado"></h5>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="avanceCancelado"></h5>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="motivoCancelado"></h5>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptarCancelado">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizar">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <div class="modal fade" id="modalCanceladoAccion" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloCanceladoAccion"></h4>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="usuarioCanceladoAccion"></h5>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="avanceCanceladoAccion"></h5>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h5 id="motivoCanceladoAccion"></h5>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptarCanceladoAccion">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizarAccion">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>





        <div class="modal fade" id="modalAddkr" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloCancelado">OKR interno</h4>

                    </div>
                    <div class="x_panel">
                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">OKR
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                  id="accion"></textarea>
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>


                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fecha de
                                    cumplimiento:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" class="form-control col-md-7 col-xs-12 has-feedback-left"
                                           id="fechaAccion">
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-success btn-sm" id="guardarAccion"><i class="fa fa-plus"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btn-sm" id="cerrarAddkrInterno"><i
                                            class="fa fa-arrow-circle-left"></i> Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloKr"></h4>

                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="actual"></span></label>
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="tipometrica"></span></label>
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="rango"></span></label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                <input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
                                       class="form-control col-md-7 col-xs-12 has-feedback-left">
                                <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                <small id="msj_avance" style="color: red;"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                <small id="msj_descripcionavance" style="color: red;"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
                                Adjuntar evidencia
                            </button>
                        </div>
                        <div class="col-md-12 text-center">
                            <small id="mensajeAexos" style="color: red;"></small>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="project_progress" id="barraCarga" style="display: none;">
                                <small id="porcentajeProgreso">
                                </small>
                                <div class="progress progress_sm">
                                    <small id="porcentajeProgreso" style="color: red;"></small>
                                    <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                         data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row" id="formAnexo"></div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizar">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
        <input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
               style="display: none;">


        <div class="modal fade" id="myModalAccion" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="actualAccion"></span></label>
                            <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                               id="rangoAccion"></span></label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Avance</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="avanceAccion" class="form-control col-md-7 col-xs-12" min="0"
                                       step="0.5">
                                <small id="msj_avanceAccion"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción del
                                avance<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12"
                                          id="descripcionavanceAccion"></textarea>
                                <small id="msj_descripcionavanceAccion"></small>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptarAccion">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizarAccion">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="modalClaveAccion" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="leyendaEliminar">Esta apunto de eliminar un OKR interno</label><br>
                            <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                        </div>
                        <div class="form-group">
                            <input type="password" id="claveAccion">
                            <br>
                            <small id="msj_claveAccion"></small>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="validaClaveAccion">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="cerraClaveAccion">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalBitacora" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="indicadorT">Bitácora del indicador: <strong
                                    id="tIndicador"></strong></h4>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <table class="table table-striped table-bordered" id="tablaBitacora">
                                    <thead style="font-size: 12px;">
                                    <td>Fecha</td>
                                    <td>Descripción</td>
                                    <td>Último avance</td>
                                    <td>Avance</td>
                                    <td>Estatus</td>
                                    <!--<td>Usuario/autorizo</td>-->
                                    <td>Usuario</td>
                                    <td>Motivo</td>
                                    <!--<td>Usuario/no autorizo</td>-->
                                    </thead>
                                    <tbody style="font-size: 10px;">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalValidaAccion" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloavance"></h4>

                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="mensajeValidaAccion"></label>
                        </div>
                        <div class="form-group">
                            <label id="avanceValidaAccion"></label>
                        </div>
                        <div class="form-group" id="boxavanceModificacion" style="display: none;">
                            <label class="col-md-6 text-right">Valor:</label>
                            <div class="col-md-2">
                                <input class="form-control" type="number" min="0" step=".5" id="avanceModificacion"
                                       style="height: 25px !important;px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 text-right">Descripcion:</label>
                            <label class="col-md-6 text-left" id="descripcionValidaAccion"></label>
                        </div>
                        <div class="form-group" id="boxMotivoModificacion" style="display: none;">
                            <label class="col-md-6 text-right">Motivo:</label>
                            <div class="col-md-4">
                                <textarea class="form-control" id="motivoModificacion">

                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <table class="table table-striped table-bordered" id="tablaAnexosAccion">
                                <thead>
                                <td>Nombre de la evidencia</td>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <?php if($this->session->userdata('tipo')=='superadmin'){ ?>
                    <div class="row" style="margin-bottom: 25px; display: none; margin-top: 15px;" id="seccionAceptarCambios">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-primary btn-xs" id="btnAutorizarNuevo">
                                <i class="fa fa-check"></i> Autorizar
                            </button>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;" id="botones">
                        <div class="col-md-3 text-center">
                            <button class="btn btn-primary btn-xs" id="btnAceptarValidaAccion">
                                <i class="fa fa-check"></i> Autorizar
                            </button>
                        </div>
                        <div class="col-md-3 text-center">
                            <button class="btn btn-danger btn-xs" id="btnNoAutorizarAccion" class="close"
                                    data-dismiss="modal"
                                    aria-label="Close"><i class="fa fa-close"></i> No autorizar
                            </button>
                        </div>
                        <div class="col-md-3 text-center">
                            <button style="background: #00B2A9; border-color: #00B2A9" class="btn btn-danger btn-xs"
                                    id="btnModificarAvance"
                                    aria-label="Close"><i class="fa fa-pencil"></i> Modificar
                            </button>
                        </div>
                        <div class="col-md-3 text-center">
                            <button class="btn btn-danger btn-xs" class="close" data-dismiss="modal"
                                    aria-label="Close"><i class="fa fa-close"></i> Cerrar
                            </button>
                        </div>
                    </div>
                    <?php }elseif($this->session->userdata('tipo')=='admin'){ ?>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;" id="botones">
                            <div class="col-md-4 text-center">
                                <button class="btn btn-primary btn-xs" id="btnAceptarValidaAccion">
                                    <i class="fa fa-check"></i> Autorizar
                                </button>
                            </div>
                            <div class="col-md-4 text-center">
                                <button class="btn btn-danger btn-xs" id="btnNoAutorizarAccion" class="close"
                                        data-dismiss="modal"
                                        aria-label="Close"><i class="fa fa-close"></i> No autorizar
                                </button>
                            </div>
                            <div class="col-md-4 text-center">
                                <button class="btn btn-danger btn-xs" class="close" data-dismiss="modal"
                                        aria-label="Close"><i class="fa fa-close"></i> Cerrar
                                </button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>


        <div class="modal fade" id="noAutorizarAccion" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">No autorizar el avance de: <label
                                    id="avanceDeAccion"></label>
                        </h4>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="motivoCancelAccion"></label>
                        </div>
                    </div>
                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label>Motivo: </label>
                        </div>
                    </div>
                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <textarea id="motivoAccion"></textarea>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-center">
                            <button class="btn btn-danger" id="aceptarCancelacionAccion">Aceptar
                            </button>
                        </div>
                        <div class="col-md-6 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalEditAccion" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloCancelado">OKR interno</h4>

                    </div>
                    <div class="x_panel">
                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">OKR
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                  id="accionEdit"></textarea>
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>


                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fecha de
                                    cumplimiento:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" class="form-control col-md-7 col-xs-12 has-feedback-left"
                                           id="fechaAccionEdit">
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-success btn-sm" id="aceptarEditAccion"><i class="fa fa-plus"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btn-sm"><i
                                            class="fa fa-arrow-circle-left"></i> Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="modalOkrBiEdit" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloCancelado">Edición OKR</h4>

                    </div>
                    <div class="x_panel">
                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">OKR bimestral
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                  id="descripcionOkrBi"></textarea>
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>


                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fecha de
                                    cumplimiento:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="date" class="form-control col-md-7 col-xs-12 has-feedback-left"
                                           id="fechaCOkrBi">
                                    <small id="msj_accion"></small>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal form-label-left" id="step-1">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Ponderacion:
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" min="0" step="1" class="form-control col-md-7 col-xs-12 has-feedback-left"
                                           id="ponderacion">
                                    <small id="msj_ponderacion"></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <label id="mensajePonderacion"style="display: none;">La ponderación máxima disponible es: <b id="dis"></b></label>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-success btn-sm" id="saveEdicionOkrBi"><i class="fa fa-plus"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btn-sm" data-dismiss="modal"><i
                                            class="fa fa-arrow-circle-left"></i> Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="modalJustificacion" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="text-center">Justificacíon</h4>
                    </div>

                    <div class="col-md-12 form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <textarea class="col-md-10  form-group" id="justifica"></textarea>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-danger" id="aceptarJustifica">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once 'complementos/footer.php' ?>


        <script>
            $(document).ready(function () {

                <?php
                if($objetivos != null){
                foreach ($objetivos as $obj){ ?>

                g = new JustGage({
                    id: 'gauge<?php echo $obj->idObjetivo;?>',
                    value: <?php echo $obj->avancePorcentaje;?>,
                    min: 0,
                    max: 100,
                    titlePosition: "below",
                    valueFontColor: "#3f4c6b",
                    pointer: true,
                    customSectors:
                        [{
                            color : "#931623",
                            lo : 0,
                            hi : 24.999
                        },{
                            color : "#f07622",
                            lo : 25,
                            hi : 49.99
                        },{
                            color : "#f7b21d",
                            lo:50,
                            hi:74.99
                        },{
                            color : "#3eb049",
                            lo:75,
                            hi:100
                        }],
                    pointerOptions: {
                        toplength: -15,
                        bottomlength: 10,
                        bottomwidth: 12,
                        color: '#8e8e93',
                        stroke: '#ffffff',
                        stroke_width: 3,
                        stroke_linecap: 'round'
                    },
                    relativeGaugeSize: true,

                });

                <?php } }?>

            });


        </script>

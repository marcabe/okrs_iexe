<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0; background: #003B5C; >
            <text class=" site_title
        "><span></span></text>
        <img src="<?php echo base_url(); ?>assets/build/images/logo_menus.png" class="img-responsive" width="85%;">

    </div>
    <div class="clearfix"></div>
    <div class="profile clearfix">
        <div class="clearfix"></div>
    </div>
    <br/>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <!--<h3>Menu Principal</h3>-->
            <?php if ($this->session->userdata('tipo') != 'lider') { ?>
                <ul class="nav side-menu">
                    <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                        <li><a><i class="fa fa-user"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo base_url(); ?>lista_usuarios"><i class="fa fa-users"></i>Lista
                                        de Usuarios</a></li>
                                <li><a href="<?php echo base_url(); ?>nuevo_usuario"><i class="fa fa-user-plus"></i>Nuevo
                                        Usuario</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($this->session->userdata('tipo') != 'acuerdo') { ?>

                    <li><a><i class="fa fa-edit"></i> Proyectos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url(); ?>lista_planes">Lista de proyectos</a></li>
                            <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                <li><a href="<?php echo base_url(); ?>nuevo_plan">Nuevo proyecto</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Objetivos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url(); ?>lista_objetivos">Lista de Objetivos</a></li>
                            <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                <li><a href="<?php echo base_url(); ?>nuevo_objetivo">Nuevo Objetivo</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>

                    <?php if($this->session->userdata('acuerdos') != 0){ ?>
                    <li><a><i class="fa fa-edit"></i> Acuerdos <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url(); ?>lista_minutas">Lista de Acuerdos</a></li>
                            <li><a href="<?php echo base_url(); ?>nueva_minuta">Nuevo Acuerdo</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            <?php } elseif ($this->session->userdata('tipo') == 'lider') { ?>
                <ul class="nav side-menu">
                    <li>
                        <a href="<?php echo base_url() ?>tablero">
                            <i class="fa fa-home"></i>
                            Mis proyectos prioritarios
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>reuniones">
                            <i class="fa fa-bank"></i>
                            <text class="minut"
                                  title="">Reuniones
                            </text>
                        </a>
                    </li>
                    <?php if ($reunionMenu == 0) { ?>
                        <?php foreach ($planes as $plan) { ?>
                            <li>
                                <a>
                                    <i class="fa fa-edit"></i>
                                    <text class="planes"
                                          title="<?php echo $plan->idMv; ?>"><?php echo $plan->mv; ?></text>
                                    <!--<span class="fa fa-chevron-down"></span>-->
                                </a>
                                <!--<ul class="nav child_menu">
                                    <?php foreach ($plan->objetivos as $objetivos) { ?>
                                        <li><a>
                                                <text class="objetivos"
                                                      title="<?php echo $objetivos->idObjetivo; ?>"><?php echo $objetivos->objetivo; ?></text>
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                                <?php foreach ($objetivos->kr as $kr) { ?>
                                                    <li class="sub_menu"><a>
                                                            <text class="kr"
                                                                  title="<?php echo $kr->idKeyResult; ?>"><?php echo $kr->descripcion; ?></text>
                                                        </a></li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>-->
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <li>
                            <a href="<?php echo base_url() ?>MesaGabinete">
                                <i class="fa fa-folder-open-o"></i>
                                <text class="minut"
                                      title="">Mesa de gabinete
                                </text>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>MesaProyectosEstrategicosGabinete">
                                <i class="fa fa-folder-open-o"></i>
                                <text class="minut"
                                      title="">Mesa de proyectos estratégicos
                                </text>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>MesasSectoriales">
                                <i class="fa fa-folder-open-o"></i>
                                <text class="minut"
                                      title="">Mesas sectoriales
                                </text>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>MesasTransversales">
                                <i class="fa fa-folder-open-o"></i>
                                <text class="minut"
                                      title="">Mesas transversales
                                </text>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<style>
    .nav li.current-page {
        background: none !important;
    }

    .nav.side-menu > li.current-page {
        border-right: none !important;
    }
</style>

<script>
    function deshabilitaRetroceso() {
        window.location.hash = "no-back-button";
        window.location.hash = "Again-No-back-button" //chrome
        window.onhashchange = function () {
            window.location.hash = "no-back-button";
        }
    }

    $(document).ready(function () {


        $(".planes").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_plan/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

        $(".minutas").click(function () {
            $.ajax({
                type: "POST",
                url: 'MinutasController/gobernador/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });


        $(".objetivos").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_objetivo/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

        $(".kr").click(function () {
            $.ajax({
                type: "POST",
                url: 'detalle_keyresult/' + $(this).attr('title'),
                data: {},
                success: function (datos) {
                    $('#carga').empty();
                    $('#carga').html(datos);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                },
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onloadstart = function (e) {
                        $("#fondoLoader").show();
                        console.log("Esta cargando");
                    };
                    xhr.onloadend = function (e) {
                        $("#fondoLoader").fadeOut(500);
                        console.log("Termino de cargar");
                    }
                    return xhr;
                }
            });
        });

//eventos agregados de forma personalizada
        $(".side-menu a").click(function () {
            var texto = $(this).text();
            var distancefromtop =
                $(".boxdato").empty().html(texto).addClass('fondo');

            var anchopage = $(window).width();
            var scrollTop = $(window).scrollTop(),
                elementOffset = $(this).offset().top,
                distance = (elementOffset - scrollTop);

            if (distance >= 300 && anchopage <= 680) {
                //$(window).scrollTop(0);
                $("html, body").animate({scrollTop: 0}, 'slow');
            }
        });


        /*$(window).resize(function () {
            var anchopage = $(window).width();
            var scrollTop = $(window).scrollTop(),
                elementOffset = $(this).offset().top,
                distance = (elementOffset - scrollTop);

            if (distance >= 300 && anchopage <= 680) {
                //$(window).scrollTop(0);
                $("html, body").animate({scrollTop: 0}, 'slow');
            }
        });*/


    });
</script>

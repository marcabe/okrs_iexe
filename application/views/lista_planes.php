<?php
require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
?>

<script src="<?php echo base_url(); ?>assets/build/js/lista_planes.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/lista_planes.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--<img src="<?php echo base_url(); ?>assets/build/images/mp.png" class="img-responsive">-->
                        <h3>Módulo proyectos</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix"></div>
                            </div>
                            <?php
                            if ($planes == null) {
                                echo "<h4>Sin asignación.</h4>";
                            } else
                                foreach ($planes

                                         as $plan) { ?>
                                    <div class="x_content" id="listaPlanes<?php echo $plan->idMv; ?>">
                                        <div class="col-md-10">
                                            <!-- start accordion -->
                                            <div class="tituloObjetivos row accordion-toggle">
                                                <div class="row alertAvance">
                                                    <div class="col-md-12">
                                                        <?php if (isset($plan->totalActualizar) && $plan->totalActualizar > 0) { ?>
                                                            <sup
                                                                    style="background: #ED2B00 ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px; margin-left: 3px;">
                                                                <?php echo $plan->totalActualizar; ?>
                                                            </sup>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php if (count($plan->updatePlan) > 0) { ?>
                                                    <?php foreach ($plan->updatePlan as $up) { ?>
                                                        <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                            <button style="margin-left: 2%;"
                                                                    text="<?php echo $up->idCambio; ?>"
                                                                    class="btn btn-xs btn-danger aceptarCambio"
                                                                    id="aceptarCambio<?php echo $up->idCambio; ?>">
                                                                Editado
                                                                por: <?php echo $up->nombre; ?></button>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-12 text-rigth" style="margin-top: 7px;">
                                                            <button class="btn btn-danger btn-xs justificacion"
                                                            style="background: #0094c9; border-color: #0094c9;"
                                                            value='<?php echo $plan->idMv; ?>'>
                                                            Justificación
                                                            </button>
                                                        </div>
                                                        <?php if ($this->session->userdata('tipo') == 'admin' || $this->session->userdata('tipo') == 'capturista') { ?>
                                                            <div class="col-md-4 text-left" style="margin-top: 7px;">
                                                                <button class="btn btn-primary btn-xs editaPlan" style="background: #f7b21d !important; border-color: #f7b21d !important;"
                                                                        onclick="editarPlan('<?php echo $plan->idMv; ?>')">
                                                                    Editar
                                                                    proyecto
                                                                </button>
                                                            </div>
                                                        <?php } elseif ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                            <div class="col-md-2 text-center" style="margin-top: 7px; ">
                                                                <button class="btn btn-primary btn-xs editaPlan" style="background: #f7b21d !important; border-color: #f7b21d !important;"
                                                                        onclick="editarPlan('<?php echo $plan->idMv; ?>')">
                                                                    Editar
                                                                    proyecto
                                                                </button>
                                                            </div>
                                                            <div class="col-md-2 text-center" style="margin-top: 7px;">
                                                                <button class="btn btn-danger btn-xs editDiagnostico"
                                                                        style="background: #0094c9; border-color: #0094c9;"
                                                                        value='<?php echo $plan->idMv; ?>'>
                                                                    Editar diagnóstico
                                                                </button>
                                                            </div>


                                                            <div class="col-md-2 text-center" style="margin-top: 7px;">
                                                                <button class="btn btn-danger btn-xs adjuntarDiagnostico"
                                                                        style="background: #3eb049 !important; border-color: #3eb049 !important;"
                                                                        value='<?php echo $plan->idMv; ?>'
                                                                        name="d">
                                                                    Adjuntar al diagnóstico
                                                                </button>
                                                            </div>
                                                            <div class="col-md-2 text-center" style="margin-top: 7px;">
                                                                <button class="btn btn-danger btn-xs adjuntarDiagnostico"
                                                                        style="background: #f07622 !important; border-color: #f07622 !important;"
                                                                        value='<?php echo $plan->idMv; ?>'
                                                                        name="p">
                                                                    Policy memo
                                                                </button>
                                                            </div>
                                                            <div class="col-md-1 text-center" style="margin-top: 7px;">
                                                                <button class="btn btn-danger btn-xs eliminaPlan"
                                                                        value='<?php echo $plan->idMv; ?>'>
                                                                    Eliminar
                                                                </button>
                                                            </div>
                                                            <div class="col-md-2 text-center" style="margin-top: 7px;">
                                                                <button class="btn btn-danger btn-xs vadjuntos"
                                                                        style="background: dodgerblue !important; border-color: dodgerblue !important;"
                                                                        value='<?php echo $plan->idMv; ?>'
                                                                        name="p">
                                                                    Ver adjuntos
                                                                </button>
                                                            </div>


                                                            <div class="col-md-1 text-center" style="margin-top: 7px;">
                                                                <?php if ($plan->visible == 1) { ?>
                                                                    <i class="fa fa-eye fa-2x visto"
                                                                       id="visible<?php echo $plan->idMv; ?>"
                                                                       name="<?php echo $plan->visible; ?>"
                                                                       text="<?php echo $plan->idMv; ?>"></i>
                                                                <?php } else { ?>
                                                                    <i class="fa fa-eye-slash fa-2x visto"
                                                                       id="visible<?php echo $plan->idMv; ?>"
                                                                       name="<?php echo $plan->visible; ?>"
                                                                       text="<?php echo $plan->idMv; ?>"></i>
                                                                <?php } ?>
                                                            </div>

                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <hr style="color: red !important; margin-top: 10px;">
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 centrado" style="margin-top: -20px !important;">
                                                        <div class="col-md-6 ico" data-toggle="collapse"
                                                             href="#<?php echo $plan->idMv; ?>">
                                                            <div class="col-md-12">
                                                                <div class="col-md-11 col-md-offset-1">
                                                                    <h2 style="font-weight: bold;"><b
                                                                                style="font-weight: normal;"><?php echo $plan->mv ?></b>
                                                                    </h2>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <i
                                                                            class="fa fa-arrow-circle-down iconito"
                                                                            style="font-size: 25px;"
                                                                            aria-hidden="true"></i>
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <b>Beneficios: </b><?php if (isset($plan->beneficio)) echo $plan->beneficio; ?>
                                                                </div>
                                                                <div class="col-md-11 col-md-offset-1">
                                                                    <b>Diagnóstico: </b><?php if (isset($plan->diagnostico)) echo $plan->diagnostico; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php //if ($this->session->userdata('tipo') != 'capturista') { ?>

                                                        <?php //} ?>
                                                    </div>
                                            </div>

                                                <!--<div class="col-md-5 col-sm-8 col-xs-12">
										<div class="project_progress">
											<small><?php echo number_format($plan->avance, 2); ?>% Progreso OkR</small>
											<div class="progress progress_sm">
												<div class="progress-bar bg-green" role="progressbar"
													 data-transitiongoal="<?php echo $plan->avance; ?>"
													 aria-valuenow="56" style="width: 100%;"></div>
											</div>
										</div>
									</div>-->

                                            </div>
                                        </div>
                                        <!--<div class="col-md-2 text-center">
                                            <h6>Progreso OKR</h6>
                                            <div id="gauge<?php echo $plan->idMv; ?>" class="grafi"></div>
                                        </div>-->
                                        <div class="col-md-2 text-center">
                                            <h6>Avance proyecto</h6>
                                            <div id="gaugeIndi<?php echo $plan->idMv; ?>" class="grafi"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="<?php echo $plan->idMv; ?>" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                        <table class="table table-striped table-condensed resp_table demo">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 40%!important;"
                                                                    class="text-left th_planes" id="th_indicador"><i
                                                                            class="fa fa-caret-square-o-right"
                                                                            aria-hidden="true"></i>
                                                                    Objetivo
                                                                </th>
                                                                <th colspan="2" style="width: 30%!important;"
                                                                    class="text-left th_planes" id="th_avance"><i
                                                                            class="fa fa-line-chart"
                                                                            aria-hidden="true"></i>
                                                                    Descripción
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Progreso
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vi"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i>
                                                                    Valor
                                                                    Inicial
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vf"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i>
                                                                    Valor
                                                                    Final
                                                                </th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            //if (count($plan) > 0)
                                                            foreach ($plan->objetivos as $obj) { ?>
                                                                <tr>
                                                                    <td data-th="Indicador"><?php echo $obj->objetivo; ?></td>
                                                                    <td colspan="2" data-th="Avance"><?php echo $obj->descripcion; ?></td>
                                                                    <td data-th="Progreso">
                                                                        <div class="project_progress">
                                                                            <small>
                                                                                <?php echo number_format($obj->avancePorcentaje, 2); ?>
                                                                                %
                                                                            </small>

                                                                            <div class="progress progress_sm">
                                                                                <div
                                                                                        id="indicador<?php echo $obj->idObjetivo; ?>"
                                                                                        class="progress-bar bg-green"
                                                                                        role="progressbar"
                                                                                        data-transitiongoal="<?php echo $obj->avancePorcentaje; ?>"
                                                                                        aria-valuenow="56"
                                                                                        style="width: 57%;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td data-th="Inicio">0</td>
                                                                    <td data-th="Final">100</td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                            <tr class="division">
                                                                <td colspan="8" class="text-center">Indicadores</td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 40%!important;"
                                                                    class="text-left th_planes" id="th_indicador"><i
                                                                            class="fa fa-caret-square-o-right"
                                                                            aria-hidden="true"></i>
                                                                    Indicador
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vf"><i
                                                                            class="fa fa-medkit"
                                                                            aria-hidden="true"></i>
                                                                    Métrica
                                                                </th>
                                                                <th style="width: 20%!important;"
                                                                    class="text-left th_planes" id="th_avance"><i
                                                                            class="fa fa-line-chart"
                                                                            aria-hidden="true"></i>
                                                                    Avance
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Progreso
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vi"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i>
                                                                    Valor
                                                                    Inicial
                                                                </th>
                                                                <th style="width: 10%!important;"
                                                                    class="text-left th_planes" id="th_vf"><i
                                                                            class="fa fa-percent"
                                                                            aria-hidden="true"></i>
                                                                    Valor
                                                                    Final
                                                                </th>

                                                                <th></th>
                                                                <th>
                                                                    <button class="btn btn-success btn-xs new_indicator"
                                                                            name="<?php echo $plan->idMv; ?>"><i
                                                                                class="fa fa-plus"></i> Nuevo indicador
                                                                    </button>
                                                                </th>
                                                            </tr>
                                                            <tbody id="cuerpoIndicador<?php echo $plan->idMv;?>">
                                                            <?php foreach ($plan->indicadores as $indicadores) { ?>
                                                                <tr id="listaIndi<?php echo $indicadores->idIndicadores; ?>">
                                                                    <td><?php echo $indicadores->nombreIndicador; ?></td>
                                                                    <td><?php if($indicadores->metrica=='m'){echo "Minutos";}
                                                                    elseif($indicadores->metrica=='c'){
                                                                        echo "Cantidad";
                                                                    } elseif ($indicadores->metrica=='p'){
                                                                        echo "Porcentaje";
                                                                        }?></td>

                                                                    <td id="avance<?php echo $indicadores->idIndicadores; ?>"><?php echo $indicadores->avance; ?></td>
                                                                    <td>
                                                                        <div class="project_progress">

                                                                            <small
                                                                                    id="small<?php echo $indicadores->idIndicadores; ?>"><?php echo number_format($indicadores->avancePorcentaje, 2); ?>
                                                                                %
                                                                            </small>

                                                                            <?php if (count($indicadores->bitacora) > 0 AND $indicadores->bitacora[0]->aprobado == 0) { ?>
                                                                                <small style="color: red;"
                                                                                       id="marcadorAvance<?php echo $indicadores->idIndicadores; ?>">
                                                                                    ( <?php echo $indicadores->bitacora[0]->avance; ?>
                                                                                    %
                                                                                    sin autorizar )
                                                                                </small>
                                                                            <?php } else { ?>
                                                                                <small
                                                                                        id="marcadorAvance<?php echo $indicadores->idIndicadores; ?>"></small>
                                                                            <?php } ?>

                                                                            <div class="progress progress_sm">
                                                                                <div
                                                                                        id="indicadorI<?php echo $indicadores->idIndicadores; ?>"
                                                                                        class="progress-bar bg-green"
                                                                                        role="progressbar"
                                                                                        data-transitiongoal="<?php echo $indicadores->avancePorcentaje; ?>"
                                                                                        aria-valuenow="56"
                                                                                        style="width: 57%;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td><?php echo $indicadores->inicio; ?></td>
                                                                    <td><?php echo $indicadores->final; ?></td>
                                                                    <td>
                                                                        <button class="btn btn-success btn-xs actualizarIndicador"
                                                                                value="<?php echo $indicadores->idIndicadores; ?>">
                                                                            Actualizar
                                                                        </button>
                                                                        <?php if (isset($indicadores->bitacora[0])) { ?>
                                                                            <?php if ($indicadores->bitacora[0]->aprobado == 0) { ?>
                                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                                    <button class="btn btn-danger btn-xs autorizaAvance"
                                                                                            style="background: orangered !important; border-color: orangered !important;"
                                                                                            value="<?php echo $indicadores->bitacora[0]->idBitacora; ?>">
                                                                                        <i class="fa fa-thumbs-o-up"></i>
                                                                                        Autorizar
                                                                                    </button>
                                                                                <?php } ?>
                                                                            <?php } elseif ($indicadores->bitacora[0]->aprobado == 2) { ?>
                                                                                <?php if ($this->session->userdata('tipo') == 'capturista') { ?>
                                                                                    <button class="btn btn-danger btn-xs avanceCancelado"
                                                                                            value="<?php echo $indicadores->idIndicadores; ?>"
                                                                                            id="avic<?php echo $indicadores->idIndicadores; ?>">
                                                                                        <i class="fa fa-times"></i>
                                                                                        Rechazado
                                                                                    </button>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </td>

                                                                    <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                        <td>
                                                                            <ul class="nav navbar-right panel_toolbox">
                                                                                <li class="dropdown open">
                                                                                    <a href="#" class="dropdown-toggle"
                                                                                       data-toggle="dropdown"
                                                                                       role="button"
                                                                                       aria-expanded="true"
                                                                                       style="color: black;"><i
                                                                                                class="fa fa-gear"></i>
                                                                                        Config.</a>
                                                                                    <ul class="dropdown-menu"
                                                                                        role="menu">
                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; color: #00B2A9; cursor: pointer;">
                                                                                            <label class="editarIndi"
                                                                                                   style="cursor: pointer;"
                                                                                                   name='<?php echo $indicadores->idIndicadores; ?>'>
                                                                                                <i class="fa fa-pencil"
                                                                                                   aria-hidden="true"></i>
                                                                                                Editar
                                                                                            </label>
                                                                                        </li>
                                                                                        <li style="height: 25px; font-size: 15px; padding-left: 12px; color: red; cursor: pointer;">
                                                                                            <label class="eliminaInd"
                                                                                                   style="cursor: pointer;"
                                                                                                   name='<?php echo $indicadores->idIndicadores; ?>'>
                                                                                                <i class="fa fa-trash-o"
                                                                                                   aria-hidden="true"></i>
                                                                                                Eliminar
                                                                                            </label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    <?php } ?>

                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="col-md-12">
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading" style="height: 90px;">
                                                                    <div class="col-md-12 text-center"
                                                                         style="font-weight: bold; margin-bottom: 10px;">
                                                                        Comunicación <i class="fa fa-envelope"
                                                                                        aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="col-md-6" style="float: left;">
                                                                        <select style="10px;"
                                                                                class="form-control listaLider"
                                                                                id="listaLider<?php echo $plan->idMv; ?>"
                                                                                name="<?php echo $plan->idMv; ?>">

                                                                            <?php if ($this->session->userdata('idRol') == 2) { ?>
                                                                                <option value="gob">Gobernador</option>
                                                                            <?php } ?>
                                                                            <?php if (isset($plan->UsuariosChat)) { ?>
                                                                                <?php foreach ($plan->UsuariosChat as $usChat) { ?>
                                                                                    <option value="<?php echo $usChat->user; ?>"><?php echo $usChat->nombre; ?>
                                                                                        (<?php echo $usChat->nombrePuesto; ?>
                                                                                        )
                                                                                    </option>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <div style="cursor: pointer;"
                                                                         class="col-md-6 text-right vaciarChat"
                                                                         name="<?php echo $plan->idMv; ?>">
                                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                        Vaciar Buzón
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body PlanChat"
                                                                     id="PlanChat<?php echo $plan->idMv; ?>">
                                                                    <ul id="chatPlan<?php echo $plan->idMv; ?>">
                                                                        <?php
                                                                        if (isset($plan->chat))
                                                                            foreach ($plan->chat as $ch) { ?>
                                                                                <li>
                                                                                    <div class="col-md-11 mensajeChatDer">
                                                                                        <label><?php echo $ch->nombre; ?>
                                                                                            <small style="color: #5B6770">
                                                                                                (<?php echo $ch->nombrePuesto; ?>
                                                                                                )
                                                                                            </small>
                                                                                        </label>
                                                                                        <p><?php echo $ch->mensaje; ?></p>
                                                                                        <small
                                                                                                class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
                                                                                    </div>
                                                                                </li>
                                                                            <?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="panel-footer">
                                                                    <div class="form-row align-items-center">
                                                                        <div class="col-sm-8 my-1">
                                                                            <input type="text" class="form-control"
                                                                                   id="mensajeChat<?php echo $plan->idMv; ?>"
                                                                                   placeholder="Escribir mensaje">

                                                                        </div>
                                                                        <div class="col-auto my-1">
                                                                            <button class="btn btn-primary enviarChat"
                                                                                    text="<?php echo $plan->idMv; ?>">
                                                                                Enviar
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end of accordion -->
                                        </div>
                                    </div>
                                <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalAcutaliza" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="tituloKr"></h4>

                    </div>


                    <div class="row" style="margin-top: 25px;">
                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="actual"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="tipometrica"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="rango"></span></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                    <input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
                                           class="form-control col-md-7 col-xs-12 has-feedback-left">
                                    <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                    <small id="msj_avance" style="color: red;"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                    <small id="msj_descripcionavance" style="color: red;"></small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
                                Adjuntar evidencia
                            </button>
                        </div>
                        <div class="col-md-12 text-center">
                            <small id="mensajeAexos" style="color: red;"></small>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="project_progress" id="barraCarga" style="display: none;">
                                <small id="porcentajeProgreso">
                                </small>
                                <div class="progress progress_sm">
                                    <small id="porcentajeProgreso" style="color: red;"></small>
                                    <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                         data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row" id="formAnexoIndicador"></div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 text-right">
                            <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                        </div>
                        <div class="col-md-6 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close" id="btnFinalizar">Cerrar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="noAprobado" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label>El indicador tiene un avance que aun no ha sido validado</label>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalValida" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="indicadorTa"></h4>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-group">
                            <label id="mensajeValida"></label>
                        </div>
                        <!--<div class="form-group">
                            <label id="capturista"></label>
                        </div>-->
                        <div class="form-group">
                            <label id="avanceValida"></label>
                        </div>
                        <div class="form-group">
                            <label id="descripcionValida"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <table class="table table-striped table-bordered" id="tablaAnexos">
                                <thead>
                                <td>Nombre de la evidencia</td>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                        <div class="col-md-4 text-right">
                            <button class="btn btn-primary" id="btnAceptarValida">Autorizar</button>
                        </div>
                        <div class="col-md-4 text-center">
                            <button class="btn btn-danger" id="btnNoAutorizar" class="close" data-dismiss="modal"
                                    aria-label="Close">No autorizar
                            </button>
                        </div>
                        <div class="col-md-4 text-left">
                            <button class="btn btn-danger" class="close" data-dismiss="modal"
                                    aria-label="Close">Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalClave" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                        <div class="form-grou>
								<label id=" leyendaEliminar
                        ">Esta apunto de eliminar un plan</label><br>
                        <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                    </div>
                    <div class="form-group">
                        <input type="password" id="contraseña"><br>
                        <small id="msj_claveNoValida"></small>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="validaClave">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalClaveEditar" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-grou>
								<label id=" leyendaEliminar
                    ">Esta apunto de editar un indicador</label><br>
                    <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                </div>
                <div class="form-group">
                    <input type="password" id="contraseñaUpdate">
                </div>
            </div>
            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" id="validaClaveEditar">Aceptar</button>
                </div>
                <div class="col-md-6 text-left">
                    <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                            aria-label="Close">Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="updateCancel" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <label>El indicador ya cuenta con una actualización</label>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="noAutorizar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">No autorizar el avance de: <label id="avanceDe"></label></h4>

            </div>

            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <label id="motivoCancel"></label>
                </div>
            </div>
            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <label>Motivo: </label>
                </div>
            </div>
            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <textarea id="motivo"></textarea>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" id="aceptarCancelacion" aria-label="Close">
                        Aceptar
                    </button>
                </div>
                <div class="col-md-6 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCancelado" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="tituloCancelado"></h4>

            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h5 id="usuarioCancelado"></h5>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h5 id="avanceCancelado"></h5>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h5 id="motivoCancelado"></h5>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" id="btnAceptarCancelado">Aceptar</button>
                </div>
                <div class="col-md-6 text-left">
                    <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                            aria-label="Close" id="btnFinalizar">Cerrar
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>
<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
       style="display: none;">
<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>" style="display: none;">
<input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">


<div class="modal fade" id="modalBitacora" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="indicadorT">Bitácora del indicador: <strong
                            id="tIndicador"></strong></h4>
            </div>

            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <table class="table table-striped table-bordered" id="tablaBitacora">
                            <thead style="font-size: 12px;">
                            <td>Fecha</td>
                            <td>Descripción</td>
                            <td>Último avance</td>
                            <td>Avance</td>
                            <td>Estatus</td>
                            <!--<td>Usuario/autorizo</td>-->
                            <td>Usuario</td>
                            <td>Motivo</td>
                            <td>Adjuntos</td>
                            <!--<td>Usuario/no autorizo</td>-->
                            </thead>
                            <tbody style="font-size: 10px;">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalUpdatePlan" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Detalles de la edición</h4>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Presupuesto 2018:</label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="p2018" style="font-size: 13px; color: grey;"></label>
                        </div>
                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Presupuesto 2019: </label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="p2019" style="font-size: 13px; color: grey;"></label> </label>
                        </div>

                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Indicador estratégico:</label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="ie" style="font-size: 13px; color: grey;"></label>
                        </div>
                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Inversion 2018:</label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="i2018" style="font-size: 13px; color: grey;"></label>
                        </div>

                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Inversion 2019:</label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="i2019" style="font-size: 13px; color: grey;"></label>
                        </div>
                        <div class="col-md-6 text-right">
                            <label style="font-size: 13px; color: #931623;">Meta Indicador:</label>
                        </div>
                        <div class="col-md-6 text-left">
                            <label id="mi" style="font-size: 13px; color: grey;"></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 25px;">
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" id="btnAceptarUpdatePlan">Autorizar</button>
                </div>
                <div class="col-md-6 text-left">
                    <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                            aria-label="Close" id="noautorizarUpdate">No autorizar
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="modalDiagnostico" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="text-center">Edición diagnóstico</h4>
            </div>

            <div class="col-md-12 form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <textarea class="col-md-10  form-group" id="diagnostico"></textarea>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" id="aceptaDiganostico">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalAdjuntos" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="tituloKr"></h4>

            </div>


            <div class="row" style="margin-top: 25px;">
                <div class="col-md-12 text-center">
                    <button class="btn btn-success btn-xs agregaAnexoDiag"><i class="fa fa-paperclip"></i>
                        Adjuntar evidencia
                    </button>
                </div>
                <div class="col-md-12 text-center">
                    <small id="mensajeAexos" style="color: red;"></small>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="project_progress" id="barraCargadp" style="display: none;">
                        <small id="porcentajeProgreso">
                        </small>
                        <div class="progress progress_sm">
                            <small id="porcentajeProgreso" style="color: red;"></small>
                            <div class="progress-bar bg-green" id="barraProgresoCargadp" role="progressbar"
                                 data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row" id="formAnexoDiag"></div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" id="btnAceptarDiagAnex">Aceptar</button>
                </div>
                <div class="col-md-6 text-left">
                    <button class="btn btn-danger" class="close" data-dismiss="modal"
                            aria-label="Close" id="btnFinalizar">Cerrar
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="modalAddIndicador" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="tituloCancelado">Nuevo Indicador</h4>

            </div>
            <div class="x_panel">
                <div class="form-horizontal form-label-left" id="step-1">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Indicador
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="indicador">
                            <small id="msj_indicador"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Métrica
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control has-feedback-left sel" id="metrica">
                                <option value="0">Selecciona una opción</option>
                                <option value="c">Cantidad</option>
                                <option value="p">Porcentaje</option>
                                <option value="m">Minutos</option>
                            </select>
                            <small id="msj_metrica"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Valor inicial
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="vinicial">
                            <small id="msj_vinicial"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Valor final
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="vfinal">
                            <small id="msj_vfinal"></small>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-success btn-sm" id="guardarIndicador"><i class="fa fa-plus"></i>
                            Guardar
                        </button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btn-sm" class="close" data-dismiss="modal"
                                aria-label="Close" ><i
                                    class="fa fa-arrow-circle-left"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalUpdateIndicador" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="tituloCancelado">Edición del Indicador</h4>

            </div>
            <div class="x_panel">
                <div class="form-horizontal form-label-left" id="step-1">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Indicador
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="Updateindicador">
                            <small id="msj_indicador"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Métrica
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control has-feedback-left sel" id="Updatemetrica">
                                <option value="0">Selecciona una opción</option>
                                <option value="c">Cantidad</option>
                                <option value="p">Porcentaje</option>
                                <option value="m">Minutos</option>
                            </select>
                            <small id="msj_metrica"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Valor inicial
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="Updatevinicial">
                            <small id="msj_vinicial"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Valor final
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 has-feedback-left" id="Updatevfinal">
                            <small id="msj_vfinal"></small>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-success btn-sm" id="updateguardarIndicador"><i class="fa fa-plus"></i>
                            Guardar
                        </button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btn-sm" class="close" data-dismiss="modal"
                                aria-label="Close" ><i
                                    class="fa fa-arrow-circle-left"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="modal fade in" id="modalAcuerdos" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title text-center" id="indicadorT">Archivos adjuntos <strong id="tIndicador"></strong></h4>
            </div>

            <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <table class="table table-striped table-bordered" id="tablaBitacora">
                            <thead style="font-size: 12px;">
                                <tr>
                                    <td>Acuerdo</td>
                                    <td>Tipo</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody style="font-size: 10px;">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalJustificacion" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="text-center">Justificacíon</h4>
            </div>

            <div class="col-md-12 form-horizontal form-label-left text-center" style="margin-top: 20px;">
                <div class="form-group">
                    <div class="col-md-1"></div>
                    <textarea class="col-md-10  form-group" id="justifica"></textarea>
                </div>
            </div>

            <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <button class="btn btn-danger" id="aceptarJustifica">Aceptar</button>
                </div>
            </div>

        </div>
    </div>
</div>
<?php require_once 'complementos/footer.php' ?>


<script>
    $(document).ready(function () {



        <?php foreach ($planes as $plan){ ?>
        g = new JustGage({
            id: 'gaugeIndi<?php echo $plan->idMv;?>',
            value: <?php echo $plan->avanceIndicadores; ?>,
            min: 0,
            max: 100,
            titlePosition: "below",
            valueFontColor: "#3f4c6b",
            pointer: true,
            customSectors:
                [{
                    color : "#931623",
                    lo : 0,
                    hi : 24.999
                },{
                    color : "#f07622",
                    lo : 25,
                    hi : 49.99
                },{
                    color : "#f7b21d",
                    lo:50,
                    hi:74.99
                },{
                    color : "#3eb049",
                    lo:75,
                    hi:100
                }],
            pointerOptions: {
                toplength: -15,
                bottomlength: 10,
                bottomwidth: 12,
                color: '#8e8e93',
                stroke: '#ffffff',
                stroke_width: 3,
                stroke_linecap: 'round'
            },
            relativeGaugeSize: true,

        });

        <?php } ?>

    });


</script>

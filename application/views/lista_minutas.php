<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("America/Mexico_City");

?>


<script src="<?php echo base_url(); ?>assets/build/js/lista_minutas.js"></script>
<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"
      rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/lista_minutas.css" rel="stylesheet">
</head>
<!--<p class="network-status info-box success">You're online! 😄</p>-->
<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url(); ?>assets/build/images/500.gif"
         style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<input type="text" value="<?php echo $this->session->userdata('tipo'); ?>" style="display: none;" id="tipoUsuario">

<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">-->
                        <h3>Módulo acuerdos</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix"></div>
                            </div>

                            <?php
                            if ($planes == null){
                                echo "<h4>Sin asignación.</h4>";
                            }
                            else{
                            $i = 0;
                            foreach ($planes

                            as $pl){
                            $i++;
                            if (count($pl->minutas) > 0) {
                                ?>

                                <div class="col-md-12 col-xs-12 tituloPlanes">
                                    <div class="acordingPlanes">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <?php if($pl->vencer>=1){?>
                                            <div
                                                 style="border-radius: 25px !important; background: green; background: #f07622; color: white; font-weight: bold; padding: 5px; width: 4%; text-align: center;">
                                                <?php echo $pl->vencer;?>
                                            </div>
                                            <?php } ?>
                                            <div class="col-md-12 col-xs-12">
                                                <h2 style="color: white; font-size: 15px !important; font-weight: bold; cursor: pointer;"
                                                    class="row accordion-toggle" data-toggle="collapse"
                                                    href="#listaMinutas<?php echo $pl->idMv ?>">
                                                    <i class="fa fa-plus-circle iconito"></i> <?php echo $pl->mv; ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div id="listaMinutas<?php echo $pl->idMv ?>" class="accordion-body collapse">
                                <?php $j = 0;
                                foreach ($pl->minutas as $m){
                                $j++; ?>
                                <div>
                                    <!-- start accordion -->
                                    <div class="col-md-10">
                                        <div class="tituloObjetivos row">
                                            <div class="col-md-12">
                                                <?php if($m->vencer >= 1 ){ ?>
                                                <div class="col-md-1">
                                                    <div class="col-md-6 text-center"
                                                         style="border-radius: 25px !important; background: green; background: #f07622; color: white; font-weight: bold; padding: 5px;">
                                                        <?php echo $m->vencer;?>
                                                    </div>
                                                </div
                                                <?php } ?>>
                                                <div class="col-md-2 text-center" style="margin-top: 5px;">
                                                    <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                        <button class="btn btn-primary btn-xs verAcuerdos"
                                                                style="background: #f7b21d; border-color: #f7b21d;"
                                                                id="acuerdos<?php echo $m->idMinuta; ?>"
                                                                value="<?php echo $m->idMinuta; ?>">
                                                            <i class="fa fa-edit" aria-hidden="true"></i> Archivos
                                                            adjuntos
                                                        </button>
                                                    <?php } ?>
                                                </div>


                                                <div class="col-md-3 text-center" style="margin-top: 5px;">
                                                    <button class="btn btn-primary btn-xs verOrden"
                                                            style="background: #0094c9; border-color: #0094c9;"
                                                            id="acuerdos<?php echo $m->idMinuta; ?>"
                                                            value="<?php echo $m->idMinuta; ?>">
                                                        <i class="fa fa-edit" aria-hidden="true"></i> Orden del día
                                                    </button>
                                                </div>


                                                <?php
                                                if ($this->session->userdata('tipo') != 'capturista') {
                                                    if ($this->session->userdata('tipo') == 'admin') { ?>
                                                        <div class="col-md-3 text-right" style="margin-top: 5px;">
                                                            <button class="btn btn-primary btn-xs editobjt"
                                                                    style="background: #3eb049!important; border-color: #3eb049!important;"
                                                                    onclick="editaAcuerdo('<?php echo $m->idMinuta; ?>')">
                                                                <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                                Acuerdo
                                                            </button>
                                                        </div>
                                                    <?php } elseif ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                        <div class="col-md-2 text-right" style="margin-top: 5px;">
                                                            <button class="btn btn-primary btn-xs editobjt"
                                                                    style="background: #3eb049!important; border-color: #3eb049!important;"
                                                                    onclick="editaAcuerdo('<?php echo $m->idMinuta; ?>')">
                                                                <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                                Acuerdo
                                                            </button>
                                                        </div>
                                                        <div class="col-md-3 text-right" style="margin-top: 5px;">
                                                            <button class="btn btn-danger btn-xs eliminaMinuta"
                                                                    style="background: #761c19; border-color: #761c19;"
                                                                    value='<?php echo $m->idMinuta; ?>'>
                                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                Eliminar
                                                                Acuerdo
                                                            </button>
                                                        </div>
                                                        <div class="col-md-1 text-left" style="margin-top: 7px;">
                                                            <?php if ($m->visible == 1) { ?>
                                                                <i class="fa fa-eye fa-2x visto"
                                                                   id="visible<?php echo $m->idMinuta; ?>"
                                                                   name="<?php echo $m->visible; ?>"
                                                                   text="<?php echo $m->idMinuta; ?>"></i>
                                                            <?php } else { ?>
                                                                <i class="fa fa-eye-slash fa-2x visto"
                                                                   id="visible<?php echo $m->idMinuta; ?>"
                                                                   name="<?php echo $m->visible; ?>"
                                                                   text="<?php echo $m->idMinuta; ?>"></i>
                                                            <?php } ?>
                                                        </div>

                                                    <?php }
                                                } else { ?>
                                                    <div class="col-md-3 text-right" style="margin-top: 5px;">
                                                        <button class="btn btn-primary btn-xs editobjt"
                                                                onclick="editaAcuerdo('<?php echo $m->idMinuta; ?>')">
                                                            <i class="fa fa-edit" aria-hidden="true"></i> Editar
                                                            Acuerdo
                                                        </button>
                                                    </div>
                                                <?php } ?>
                                            </div>



                                            <div class="col-md-12">
                                                <hr style="color: red !important; margin-top: 10px;">
                                            </div>



                                            <div class="col-md-12 col-sm-12 col-xs-12 centrado">
                                                <div class="col-md-6 ico" data-toggle="collapse"
                                                     href="#<?php echo $m->idMinuta; ?>">
                                                    <div class="col-md-12">
                                                        <div class="col-md-11 col-md-offset-1">
                                                            <h2><b>Proyecto: </b>
                                                                <?php echo $m->plan; ?>
                                                            </h2>
                                                        </div>
                                                        <div class="col-md-11 col-md-offset-1">
                                                            <h2><b>Código: </b>
                                                                <?php echo $m->codigo; ?>
                                                            </h2>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <i
                                                                    class="fa fa-arrow-circle-down iconito"
                                                                    style="font-size: 25px;"
                                                                    aria-hidden="true"></i>
                                                        </div>
                                                        <div class="col-md-11">
                                                            <h2><b>Lugar: </b>
                                                                <?php echo $m->lugar; ?>
                                                            </h2>
                                                        </div>

                                                        <div class="col-md-11 col-md-offset-1">
                                                            <h2>
                                                                <b>Fecha: </b>
                                                                <?php echo $m->fecha; ?>
                                                            </h2>
                                                        </div>
                                                        <div class="col-md-11 col-md-offset-1">
                                                            <h2>
                                                                <b>Objetivo: </b>
                                                                <?php echo $m->objetivo; ?>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h6>Avance Acuerdos</h6>
                                        <div id="gauge<?php echo $m->idMinuta; ?>" class="grafi"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="<?php echo $m->idMinuta; ?>" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <?php if ($this->session->userdata('tipo') != 'capturista'){ ?>
                                                <div class="col-md-12">
                                                    <?php }else{ ?>
                                                    <div class="col-md-12">
                                                        <?php } ?>
                                                        <table class="table table-striped table-condensed resp_table demo">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 35%;" class="text-left th_planes azul"
                                                                    id="th_indicador">
                                                                    <i
                                                                            class="fa fa-caret-square-o-right"
                                                                            aria-hidden="true"> </i> Actividad

                                                                    <div style="margin-top: 5px;">
                                                                        <select class="form-control col-md-12 selectActividad" name="<?php echo $m->idMinuta?>">
                                                                            <option value="0" name="">Todo</option>
                                                                            <?php foreach ($m->acuerdos as $acuerdos) { ?>
                                                                                <option value="<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->actividad; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>

                                                                </th>
                                                                <!--th class="text-left th_planes" id="th_indicador">Metrica</th>-->
                                                                <th class="text-left th_planes" id="th_avance"><i
                                                                            class="fa fa-line-chart"
                                                                            aria-hidden="true"></i>
                                                                    Responsable
                                                                    <div style="margin-top: 5px;">
                                                                        <select class="form-control col-md-12 selectResponsable" name="<?php echo $m->idMinuta?>">
                                                                            <option value="0" name="">Todo</option>
                                                                            <?php foreach ($m->listaResponsable as $r) { ?>
                                                                                <option value="<?php echo $r; ?>"><?php echo $r; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </th>
                                                                <!--<th class="text-left th_planes verde" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i> Fecha
                                                                </th>-->
                                                                <th style="width: 10%;"
                                                                    class="text-left th_planes chicle"
                                                                    id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Cumplimiento
                                                                    <div style="margin-top: 5px;">
                                                                        <select class="form-control col-md-12 selectFecha" name="<?php echo $m->idMinuta?>">
                                                                            <option value="0" name="">Todo</option>
                                                                            <?php foreach ($m->listaFechas as $r) { ?>
                                                                                <option value="<?php echo $r; ?>"><?php echo $r; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </th>
                                                                <th class="text-left th_planes azul" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Progreso
                                                                </th>
                                                                <!--<th class="text-left th_planes" id="th_progreso"><i
                                                                            class="fa fa-gear" aria-hidden="true"></i>
                                                                    Plan
                                                                </th>-->
                                                                <th></th>
                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                    <th></th>
                                                                <?php } ?>
                                                                <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                    <th></th>
                                                                <?php } ?>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="table<?php echo $m->idMinuta?>" >
                                                            <?php
                                                            if (count($m->acuerdos) > 0 && isset($m->acuerdos))
                                                                foreach ($m->acuerdos as $acuerdos) {
                                                                    ?>
                                                                    <tr id="listaAc<?php echo $acuerdos->idAcuerdo; ?>" name="<?php echo $acuerdos->idAcuerdo; ?>"
                                                                        <?php echo ($acuerdos->pintafila==1)? "style='background: #f07622!important; opacity: 0.7;color: white !important;'":"" ; ?>
                                                                        class="text-left">
                                                                        <td><?php echo $acuerdos->actividad; ?></td>
                                                                        <td id="avance<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->responsable; ?></td>
                                                                        <!--<td id="fecha<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->fechaAlta; ?></td>-->
                                                                        <td id="fecha<?php echo $acuerdos->idAcuerdo; ?>"><?php echo $acuerdos->fecha; ?></td>

                                                                        <td>
                                                                            <div class="project_progress">
                                                                                <small
                                                                                        id="small<?php echo $acuerdos->idAcuerdo; ?>"><?php echo number_format($acuerdos->avance, 2); ?>
                                                                                    %
                                                                                </small>
                                                                                <div class="progress progress_sm">
                                                                                    <div class="progress-bar bg-green"
                                                                                         id="barraProgreso<?php echo $acuerdos->idAcuerdo; ?>"
                                                                                         role="progressbar"
                                                                                         data-transitiongoal="<?php echo $acuerdos->avance; ?>"
                                                                                         aria-valuenow="56"
                                                                                         style="width: 57%;"></div>
                                                                                </div>
                                                                            </div>
                                                                        </td>

                                                                        <!--<td id="keyresultinicio<?php echo $acuerdos->idAcuerdo; ?>">
                                                                            <?php echo $m->plan; ?>
                                                                        </td>-->
                                                                        <td>
                                                                            <?php if ($acuerdos->avance >= 100) { ?>
                                                                                <p>Completado al 100%</p>
                                                                            <?php } else { ?>
                                                                                <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                                    <button
                                                                                            id="updateAvance<?php echo $acuerdos->idAcuerdo; ?>"
                                                                                            class="btn btn-info btn-xs updateAvance"
                                                                                            value="<?php echo $acuerdos->idAcuerdo; ?>">
                                                                                        <i
                                                                                                class="fa fa-pencil"></i>
                                                                                        Actualizar
                                                                                    </button>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                                <button
                                                                                        class="btn btn-danger btn-xs bitacoraLista"
                                                                                        id="bitacora<?php echo $acuerdos->idAcuerdo; ?>"
                                                                                        value="<?php echo $acuerdos->idAcuerdo; ?>">
                                                                                    <i class="fa fa-pencil"></i>
                                                                                    Bitácora
                                                                                </button>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <?php if ($this->session->userdata('tipo') != 'capturista') { ?>
                                                                            <td>
                                                                                <?php
                                                                                if (count($acuerdos->bitacora) > 0) {
                                                                                    if ($acuerdos->bitacora[0]->aprobado == 2) {
                                                                                        echo
                                                                                            "<button value='$acuerdos->idAcuerdo' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado" . $acuerdos->idAcuerdo . "'>
                                                                                            <i class='fa fa-pencil'></i> Avance Cancelado
                                                                                        </button>";
                                                                                    } elseif ($acuerdos->bitacora[0]->aprobado == 0) {
                                                                                        echo
                                                                                            "<button class='btn btn-danger btn-xs autorizaAvance' id='autorizaAvance" . $acuerdos->idAcuerdo . "' value=" . $acuerdos->bitacora[0]->idBitacoraGral . ">
                                                                                            <i class='fa fa-pencil'></i> Autorizar
                                                                                        </button>";
                                                                                    }

                                                                                } ?>
                                                                            </td>
                                                                        <?php } ?>
                                                                        <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
                                                                            <td>
                                                                                <button class="btn btn-danger btn-xs eliminaAcuerdo"
                                                                                        value='<?php echo $acuerdos->idAcuerdo; ?>'>
                                                                                    <i class="fa fa-trash-o"
                                                                                       aria-hidden="true"></i> Eliminar
                                                                                </button>
                                                                            </td>
                                                                        <?php } ?>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of accordion -->
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloKr"></h4>

                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="actual"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="tipometrica"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="rango"></span></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                    <input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
                                           class="form-control col-md-7 col-xs-12 has-feedback-left">
                                    <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                    <small id="msj_avance" style="color: red;"></small>
                                </div>
                            </div>
                            <input type="hidden" name="id_de_la_minuta" id="id_de_la_minuta">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                    <small id="msj_descripcionavance" style="color: red;"></small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <small id="mensajeAexos" style="color: red;"></small>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="project_progress" id="barraCarga" style="display: none;">
                                    <small id="porcentajeProgreso">
                                    </small>
                                    <div class="progress progress_sm">
                                        <small id="porcentajeProgreso" style="color: red;"></small>
                                        <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                             data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" id="formAnexo"></div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizar">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="aprobadoCancel" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label>Existe un avance no autorizado para este
                                    <Acuerdo></Acuerdo>
                                </label>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="noAutorizar" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center">No autorizar el avance de: <label id="avanceDe"></label>
                            </h4>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="motivoCancel"></label>
                            </div>
                        </div>
                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label>Motivo: </label>
                            </div>
                        </div>
                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <textarea id="motivo"></textarea>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" id="aceptarCancelacion"
                                        aria-label="Close">Aceptar
                                </button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalValida" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="mensajeValida"></label>
                            </div>
                            <!--<div class="form-group">
                                <label id="capturista"></label>
                            </div>-->
                            <div class="form-group">
                                <label id="avanceValida"></label>
                            </div>
                            <div class="form-group">
                                <label id="descripcionValida"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <table class="table table-striped table-bordered" id="tablaAnexos">
                                    <thead>
                                    <td>Nombre de la evidencia</td>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-4 text-right">
                                <button class="btn btn-primary" id="btnAceptarValida">Autorizar</button>
                            </div>
                            <div class="col-md-4 text-center">
                                <button class="btn btn-danger" id="btnNoAutorizar" class="close" data-dismiss="modal"
                                        aria-label="Close">No autorizar
                                </button>
                            </div>
                            <div class="col-md-4 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalClave" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="leyendaEliminar">Esta apunto de eliminar un acuerdo</label><br>
                                <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                            </div>
                            <div class="form-group">
                                <input type="password" id="contraseña">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="validaClave">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>"
                   style="display: none;">
            <!--<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
				   style="display: none;">-->


            <div class="modal fade" id="myModalDic" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="tipometricaDic"></span></label>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <label>¿Desea finalizar el Key Result con el 100%? </label>
                                </div>
                                <br>
                                <div class="radio col-md-6 text-right">
                                    <input type="radio" name="dico" value="Si">Si
                                </div>
                                <div class="radio col-md-6 text-left">
                                    <input type="radio" name="dico" value="No">No
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Avance:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control col-md-7 col-xs-12"
                                              id="descripcionavanceDic"></textarea>
                                    <small id="msj_descripcionavanceDic"></small>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptarDic">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizarDic">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="modalCancelado" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloCancelado"></h4>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h5 id="usuarioCancelado"></h5>
                            </div>
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h5 id="avanceCancelado"></h5>
                            </div>
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h5 id="motivoCancelado"></h5>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptarCancelado">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizar">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="modalBitacora" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="indicadorT">Bitácora de: <strong
                                        id="tIndicador"></strong></h4>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 text-center">
                                    <table class="table table-striped table-bordered" id="tablaBitacora">
                                        <thead style="font-size: 12px;">
                                        <td>Fecha</td>
                                        <td>Descripción</td>
                                        <td>Último avance</td>
                                        <td>Avance</td>
                                        <td>Estatus</td>
                                        <!--<td>Usuario/autorizo</td>-->
                                        <td>Usuario</td>
                                        <td>Motivo</td>
                                        <!--<td>Usuario/no autorizo</td>-->
                                        </thead>
                                        <tbody style="font-size: 10px;">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalAcuerdos" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="indicadorT">Archivos adjuntos <strong
                                        id="tIndicador"></strong></h4>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 text-center">
                                    <table class="table table-striped table-bordered" id="tablaBitacora">
                                        <thead style="font-size: 12px;">
                                        <td>Acuerdo</td>
                                        <td></td>
                                        </thead>
                                        <tbody style="font-size: 10px;">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
            <input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
                   style="display: none;">


            <div class="modal fade" id="modalOrdenden" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="text-center">Orden del día</h3>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label>
                                    <p id="odd"></p>
                                </label>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php require_once 'complementos/footer.php' ?>


            <script>
                $(document).ready(function () {

                    <?php
                    if($planes != null){
                    foreach ($planes as $pl){
                    foreach ($pl->minutas as $m){ ?>

                    g<?=$m->idMinuta;?> = new JustGage({
                        id: 'gauge<?php echo $m->idMinuta;?>',
                        value: <?php echo $m->avancePorcentaje;?>,
                        min: 0,
                        max: 100,
                        titlePosition: "below",
                        valueFontColor: "#3f4c6b",
                        pointer: true,
                        customSectors:
                            [{
                                color : "#931623",
                                lo : 0,
                                hi : 24.999
                            },{
                                color : "#f07622",
                                lo : 25,
                                hi : 49.99
                            },{
                                color : "#f7b21d",
                                lo:50,
                                hi:74.99
                            },{
                                color : "#3eb049",
                                lo:75,
                                hi:100
                            }],
                        pointerOptions: {
                            toplength: -15,
                            bottomlength: 10,
                            bottomwidth: 12,
                            color: '#8e8e93',
                            stroke: '#ffffff',
                            stroke_width: 3,
                            stroke_linecap: 'round'
                        },
                        relativeGaugeSize: true,

                    });

                    <?php } } }?>



                });
            </script>
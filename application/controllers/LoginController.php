<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel');
        $this->load->model('UsuariosModel');
        $this->load->model('SesionModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }

    public function index()
    {
        $this->session->unset_userdata('usuario');
        $this->session->unset_userdata('tipo');
        $this->session->unset_userdata('idUser');
        $this->session->unset_userdata('rol');
        $this->load->view('login');


    }

    public function valida()
    {
        $data = $this->input->post();
        $datosUsua = $this->LoginModel->getByUser($data['user']);
        if (count($datosUsua) > 0 && $datosUsua[0]->status == 1) {
            $response = $this->LoginModel->valida($data);
            if (count($response) > 0) {
                #Existe registro
                $tipo = $response[0]->idRol;
                switch ($tipo) {
                    #Lider
                    case 1:
                        $rol = "lider";
                        $puesto = "Gobernador";
                        break;
                    #Super admin
                    case 2:
                        $rol = "superadmin";
                        $puesto = "Superadmin";
                        break;
                    #Admin
                    case 3:
                        $rol = "admin";
                        $puesto = "Lider";
                        break;
                    #Capturista
                    case 4:
                        $rol = "capturista";
                        $puesto = "Enlace";
                        break;
                    #Acuerdos
                    case 5:
                        $rol = "acuerdo";
                        $puesto = "Acuerdo";
                        break;
                }
                $ip = $this->ObtenerIP();
                $arraydata = array(
                    'usuario' => $response[0]->nombre,
                    'tipo' => $rol,
                    'idUser' => $response[0]->user,
                    'idRol' => $response[0]->idRol,
                    'puesto' => $puesto,
                    'acuerdos' => $datosUsua[0]->acuerdos
                );
                $data = array(
                    'usuario' => $response[0]->user,
                    'fecha' => date("Y-m-d H:i"),
                    'tipo' => 'Inicio de Sesion',
                    'ip' => $ip
                );
                $this->SesionModel->insert($data);
                $this->session->set_userdata($arraydata);

                $respuesta = array(
                    "rol" => $rol,
                    "ingresa" => 2
                );
                echo json_encode($respuesta);
            } else {
                $respuesta = array(
                    "ingresa" => 1
                );
                echo json_encode($respuesta);
            }
        } else {
            #Usuario no valido
            $respuesta = array(
                "ingresa" => 0
            );
            echo json_encode($respuesta);
        }


    }


    public function pagenotfound()
    {
        $this->load->view('pagenotfound');
    }


    function ObtenerIP()
    {
        $ip = "";
        if (isset($_SERVER)) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } else {
            if (getenv('HTTP_CLIENT_IP')) {
                $ip = getenv('HTTP_CLIENT_IP');
            } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
                $ip = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                $ip = getenv('REMOTE_ADDR');
            }
        }
        // En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma
        if (strstr($ip, ',')) {
            $ip = array_shift(explode(',', $ip));
        }
        return $ip;
    }

    public function view_recuperar_clave()
    {
        $this->load->view('recuperar');
    }

    public function recuperar_clave()
    {
        $dato = $this->input->post('dato');
        if (strpos($dato, "@")) {
            #Es correo
            $data['correo'] = $dato;
            $responseUsuarios = $this->UsuariosModel->getEmail($data);
            $this->enviarCorreo($responseUsuarios);
        } else {
            #Es usuario
            $data['user'] = $dato;
            $responseUsuarios = $this->UsuariosModel->getUser($data);
            $this->enviarCorreo($responseUsuarios);
        }
    }


    public function enviarCorreo($data)
    {
        include_once('PHPMailer/class.phpmailer.php');
        include_once('PHPMailer/class.smtp.php');

        $mail = new PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->CharSet = "UTF-8";
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Host = 'mail.estrategicosags.com';
        $mail->Port = 465;
        $mail->Username = 'servicios@estrategicosags.com';
        $mail->Password = 'obiwankenobi';
        $mail->SMTPAuth = true;

        $mail->setFrom('servicios@estrategicosags.com', 'Estrategicos OKR');
        $mail->AddAddress('ing.marco.cardona@gmail.com', $data[0]->nombre.' '.$data[0]->apellidoP.' '.$data[0]->apellidoM);
        $mail->IsHTML(true);
        $mail->Subject = "'Recuperacion de contraseña'";
        $mensaje = '<body>
    <div class="container">
        <div class="banner">
            <img src="assets/img/banner_formulario.png" width="100%">
        </div>

        <br>
        <div>
            <div>
                <p>
                    Hola: '.$data[0]->nombre.' '.$data[0]->apellidoP.' '.$data[0]->apellidoM.' 
                </p>
            </div> 
        </div>
        <div>
            <div>
                <p>
                    Hemos recibido la solicitud de enviarte tu contraseña por correo. En caso de no haberlo hecho hacer caso omiso a este correo</p>
            </div> 
        </div>
        <div>
            <div>
                <p>
                    Tu contraseña es: '.$data[0]->clave.'
                </p>
            </div>
        </div>
        <div style="text-align: center;">
                <a style="cursor: pointer;" onclick="window.location = "https://estrategicosags.com/" style="background: #EC8D15;padding: 10px;border-radius:10px;color: white; ">INGRESAR AL SISTEMA</a><br><br>
            </div>
    </div>
</body>';

        $mail->MsgHTML($mensaje);
        $mail->send();
    }


}

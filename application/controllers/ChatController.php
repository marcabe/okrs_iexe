<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class ChatController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ChatModel');
        $this->load->model('ChatModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');

    }


    public function insert()
    {
        $data = $this->input->post();
        $data['status'] = 1;
        $data['de'] = $data['idUsuario'];
        $data['fechahora'] = date('Y-m-d H:i');
        $result = $this->ChatModel->insert($data);
        echo ($result != null) ? $result : 0;
    }

    public function delete($idTipo)
    {
        $this->ChatModel->deleteObjetivo($idTipo);
    }

    public function getMeParaPlaen()
    {
        $usuario = $this->input->post("usuario");
        $idPlan = $this->input->post("idPlan");
        $me = $this->input->post("me");
        $dataMensajes = $this->ChatModel->getMeParaPlaen($usuario, $idPlan, $me);
        echo json_encode($dataMensajes);
    }

    public function getMeParaObj()
    {
        $usuario = $this->input->post("usuario");
        $idObj = $this->input->post("idObj");
        $me = $this->input->post("me");
        $dataMensajes = $this->ChatModel->getMeParaObj($usuario, $idObj, $me);
        echo json_encode($dataMensajes);
    }

    public function deletePlan($idPlan){
        $this->ChatModel->deletePlan($idPlan);

    }
}

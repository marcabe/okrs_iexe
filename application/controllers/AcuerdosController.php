<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class AcuerdosController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AcuerdosModel');
        $this->load->model('AccionesModel');
        $this->load->model('KeyResultModel');
        $this->load->model('PlanesModel');
        $this->load->model('UsuariosModel');
        $this->load->model('MinutasModel');
        $this->load->model('UsuariosPlanesModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
    }

    public function insert()
    {
        $data = json_decode($this->input->post('obj'));
        foreach ($data as $indicador) {
            $dataInsert = array(
                'nombreIndicador' => $indicador->nombreIndicador,
                'inicio' => $indicador->inicio,
                'final' => $indicador->final,
                'avance' => $indicador->avance,
                'idPlan' => $indicador->idPlan
            );
            $result = $this->IndicadorModel->insert($dataInsert);
        }

        echo ($result != null) ? $result : 0;

    }

    public function tareaRepetir()
    {
        $today = new DateTime(date("Y-m-d"));

        $dataUsuario = $this->UsuariosPlanesModel->get();
        foreach ($dataUsuario as $usuarios) {
            #Enviar Acuerdos
            if ($usuarios->notAcuerdos == 1) {
                $dataMinutas = $this->MinutasModel->getByIdPlan($usuarios->plan);
                foreach ($dataMinutas as $minutas) {
                    $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                    foreach ($dataAcuerdos as $acuerdo) {
                        $fecha = new DateTime($acuerdo->fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $nombre = $usuarios->nombre . " " . $usuarios->apellidoP . " " . $usuarios->apellidoM;
                            $this->enviarCorreo($nombre, $usuarios->correo, "acuerdos", $acuerdo->actividad, $acuerdo->fecha);
                        }
                    }
                }
            }
            if ($usuarios->notOkr == 1) {
                $dataObjetivo = $this->ObjetivosModel->getObjetivosByPlan($usuarios->plan);
                foreach ($dataObjetivo as $objetivos) {
                    $dataKr = $this->KeyResultModel->getByIdObj($objetivos->idObjetivo);
                    foreach ($dataKr as $kr) {
                        $fecha = new DateTime($kr->fecha);
                        $diff = $today->diff($fecha);
                        if ($diff->days == 3 && $diff->invert == 0) {
                            $nombre = $usuarios->nombre . " " . $usuarios->apellidoP . " " . $usuarios->apellidoM;
                            $this->enviarCorreo($nombre, $usuarios->correo, "acuerdos", $acuerdo->actividad, $acuerdo->fecha);
                        }
                    }
                }
            }

            if ($usuarios->notOkrBi == 1) {
                $dataObjetivo = $this->ObjetivosModel->getObjetivosByPlan($usuarios->plan);
                foreach ($dataObjetivo as $objetivos) {
                    $dataKr = $this->KeyResultModel->getByIdObj($objetivos->idObjetivo);
                    foreach ($dataKr as $Kr) {

                        $dataAcciones = $this->AccionesModel->getByIdKr($$kr->idKeyResult);
                        foreach ($dataAcciones as $kr) {
                            $fecha = new DateTime($kr->fecha);
                            $diff = $today->diff($fecha);
                            if ($diff->days == 3 && $diff->invert == 0) {
                                $nombre = $usuarios->nombre . " " . $usuarios->apellidoP . " " . $usuarios->apellidoM;
                                $this->enviarCorreo($nombre, $usuarios->correo, "acuerdos", $acuerdo->actividad, $acuerdo->fecha);
                            }
                        }
                    }
                }

            }

            /*#Enviar Acuerdos
            $dataAcuerdos = $this->AcuerdosModel->getAll();
            foreach ($dataAcuerdos as $acuerdo) {
                $fecha = new DateTime($acuerdo->fecha);
                $diff = $today->diff($fecha);
                if ($diff->days == 1 && $diff->invert == 0) {
                    $dataUser = $this->UsuariosModel->getnotacuerdos();
                    foreach ($dataUser as $usuario) {
                        $nombre = $usuario->nombre . " " . $usuario->apellidoP . " " . $usuario->apellidoM;
                        $this->enviarCorreo($nombre, $usuario->correo, "acuerdos", $acuerdo->actividad, $acuerdo->fecha);
                    }
                }
            }

            #Enviar Okr Interno (Acciones)
            $dataOkrInterno = $this->AccionesModel->getAll();
            foreach ($dataOkrInterno as $acciones) {
                $fecha = new DateTime($acciones->fecha);
                $diff = $today->diff($fecha);
                if ($diff->days == 1 && $diff->invert == 0) {
                    $dataUser = $this->UsuariosModel->getnotokrint();
                    foreach ($dataUser as $usuario) {
                        $nombre = $usuario->nombre . " " . $usuario->apellidoP . " " . $usuario->apellidoM;
                        $this->enviarCorreo($nombre, $usuario->correo, "okrint", $acciones->accion, $acciones->fecha);
                    }
                }
            }

            #Enviar Okr
            $dataOkrInterno = $this->KeyResultModel->get();
            foreach ($dataOkrInterno as $kr) {
                $fecha = new DateTime($kr->fecha);
                $diff = $today->diff($fecha);
                if ($diff->days == 1 && $diff->invert == 0) {
                    $dataUser = $this->UsuariosModel->getnotokrbi();
                    foreach ($dataUser as $usuario) {
                        $nombre = $usuario->nombre . " " . $usuario->apellidoP . " " . $usuario->apellidoM;
                        $this->enviarCorreo($nombre, $usuario->correo, "okrbi", $kr->descripcion, $kr->fecha);
                    }
                }
            }*/
        }
    }

        public
        function enviarCorreo($nombre, $correo, $tipo, $nombrePrincipal, $fecha)
        {
            include_once('PHPMailer/class.phpmailer.php');
            include_once('PHPMailer/class.smtp.php');

            if ($tipo == 'acuerdos') {
                $subject = "Un acuerdo va a finalizar";
                $palabra = "acuerdo";
            } elseif ($tipo == 'okrint') {
                $subject = "Un OKR interno va a finalizar";
                $palabra = "OKR interno";
            } elseif ($tipo == 'okrbi') {
                $subject = "Un OKR bimestral va a finalizar";
                $palabra = "OKR bimestra";
            }


            $mail = new PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->CharSet = "UTF-8";
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'ssl';
            $mail->Host = 'mail.estrategicosags.com';
            $mail->Port = 465;
            $mail->Username = 'servicios@estrategicosags.com';
            $mail->Password = 'obiwankenobi';
            $mail->SMTPAuth = true;

            $mail->setFrom('noreply@estrategicosags.com', 'Estrategicosags');
            $mail->AddAddress($correo, $nombre);
            $mail->IsHTML(true);
            $mail->Subject = "'" . $subject . "'";
            $mensaje = '
            <body style="background: #edf1f3; padding-top: 25px; padding-bottom: 25px; ">
                <div style="background: #e5eaf3; margin-left: 20%; margin-top: 25px; margin-bottom: 25px; width: 60%; border-style: solid; border-width: 5px; border-radius: 7px; border-color: #003B5C ;">
                    <div style="width: 100%; text-align: center; background: #003B5C;">
                        <img src="assets/build/images/logo_menus.png" width="35%">
                    </div>
                    <div style="background: #e5eaf3;">
                        <h1 style="color: #f07622; ">¡Hola ' . $nombre . '!</h1>
                        <p style="font-size: 15px;">Le <b>recormados</b> que el ' . $palabra . ': <label style="color: #0094c9;">"' . $nombrePrincipal . '"</label> finalizará el dia <label style="color: #0094c9;">"' . $fecha . '"</label></p>
                    </div>
                </div>
            </body>';

            $mail->MsgHTML($mensaje);
            $mail->send();
        }


        public
        function eliminar($idAcuerdo)
        {

            $dataAcuerdo = $this->AcuerdosModel->getByIdAcuerdos($idAcuerdo);
            $idMinuta = $dataAcuerdo[0]->idMInuta;

            $this->AcuerdosModel->deleteByIdAcuerdo($idAcuerdo);


            $dataAcuerdo = $this->AcuerdosModel->getByidMinutas($idMinuta);
            $promAcuerdos = 0;
            foreach ($dataAcuerdo as $acuerdos) {
                $promAcuerdos += $acuerdos->avance;
            }
            $promAcuerdos = $promAcuerdos / count($dataAcuerdo);

            $this->MinutasModel->setPorcentaje($promAcuerdos, $idMinuta);

            $dataResponse = array(
                "idMinuta" => $idMinuta,
                "NuevoPorcentaje" => $promAcuerdos
            );


            $data = json_encode($dataResponse);

            echo $data;
        }

        public
        function getById()
        {
            $data = $this->input->post('idAcuerdo');
            $result = $this->AcuerdosModel->getById($data);
            echo json_encode($result);

        }

        public
        function getPlanByIdIndicador()
        {
            $data = $this->input->post('idIndicador');
            $result = $this->IndicadorModel->getById($data);
            $idPlan = $result[0]->idPlan;
            $dataIndicador = $this->IndicadorModel->ObtienePromedioByPlan($idPlan);
            $promedio = $dataIndicador[0]->promedio;
            $dataUpdate = array(
                "avanceIndicadores" => $promedio
            );
            $this->PlanesModel->update($idPlan, $dataUpdate);

            $dataPlanes = $this->PlanesModel->getById($idPlan);
            echo json_encode($dataPlanes);
        }

        public
        function editarAvance()
        {
            $data = $this->input->post();
            $dataOperacion = $this->IndicadorModel->getById($data['idIndicador']);
            $porcentaje = ($data['avance'] * 100) / $dataOperacion[0]->final;
            $response = $this->IndicadorModel->updateAvance($data['idIndicador'], $data['avance']);

            echo $porcentaje;
        }

        public
        function edit($idIndicador)
        {
            var_dump($idIndicador);
        }


        public
        function update($idPlan = null)
        {
            if (isset($idPlan)) {
                $data = $this->input->post();
                $result = $this->PlanesModel->update($idPlan, $data);
                echo ($result != 0) ? 1 : 0;
            }
        }

        /*


        public function detalle($idObjetivo){
            $response = $this->objetivosMenu();
            $dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);
            $dataKeyResult = $this->KeyResultModel->getByObjetivos($idObjetivo);
            $dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);

            $data = array(
                'admin' => true,
                'objetivos' => $response,
                'objetivo' => $dataObjetivo,
                'keyresult' => $dataKeyResult,
                'mv'=> $dataMv
            );
            $this->load->view('detalle_objetivo', $data);
        }


        #Funciones independientes
        public function objetivosMenu(){
            $dataObjetivos = $this->ObjetivosModel->get();
            foreach ($dataObjetivos as $objetivos){
                #Hacemos consulta sobre las key result de ese objetivo
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $progresoIndividual = 0;
                if(count($dataKeyResult) > 0) {
                    foreach ($dataKeyResult as $kr) {
                        if ($kr->metrica != 'Porcentaje') {
                            $progreso = (($kr->avance) * 100) / $kr->medicionfinal;
                        } else {
                            $progreso = $kr->avance;
                        }
                        $kr->progreso = $progreso;
                        $progresoIndividual = $progresoIndividual + $progreso;
                    }

                    $progresoObjetivo = ($progresoIndividual * 100) / (count($dataKeyResult) * 100);
                    $objetivos->progreso = $progresoObjetivo;
                }else{
                    $objetivos->progreso = 0;
                }
                $objetivos->kr = $dataKeyResult;
            }

            return $dataObjetivos;
        }*/


    }

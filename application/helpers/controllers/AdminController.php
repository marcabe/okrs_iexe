<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PlanesModel');
        $this->load->model('ObjetivosModel');
        $this->load->model('KeyResultModel');
        $this->load->model('IndicadorModel');
        $this->load->model('ReunionesModel');
        $this->load->model('PlanesModel');
        $this->load->model('AcuerdosModel');

        $this->load->model('MinutasModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->reunionMenu=0;
    }

    public function index()
    {
        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $dataPlanes = $this->PlanesModel->getVisibles();

            $response = $this->ElementosMenu();
            $responseMinutas = $this->ElemntosAcuerdos();
            $this->reunionMenu=0;
            $data = array(
                'planes' => $response,
                'm' => $responseMinutas,
                'planesGraf' => $dataPlanes,
                'reunionMenu' => $this->reunionMenu
            );
            $this->load->view('dashboardAdmin', $data);
        } else {
            redirect(base_url());
        }
    }

    public function reuniones(){
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu'=>$this->reunionMenu
        );
        $this->load->view('reuniones', $data);
    }


    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->getVisibles();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }

    public function ElemntosAcuerdos()
    {
        $dataMinutas = $this->MinutasModel->get();
        return $dataMinutas;
    }



    public function mesaGabinete(){
        $dataPlanes = $this->PlanesModel->getByReunion(1);
        foreach ($dataPlanes as $planes){
            $dataMinutas = $this->MinutasModel->getByIdPlan($planes->idMv);
            foreach ($dataMinutas as $minutas){
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu'=>$this->reunionMenu,
            'planes'=>$dataPlanes
        );
        $this->load->view('mesaGabinete', $data);
    }

    public function mesaProyectosEstrategicos(){
        $dataPlanes = $this->PlanesModel->getByReunion(2);
        foreach ($dataPlanes as $planes){
            $dataMinutas = $this->MinutasModel->getByIdPlan($planes->idMv);
            foreach ($dataMinutas as $minutas){
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu'=>$this->reunionMenu,
            'planes'=>$dataPlanes
        );
        $this->load->view('mesaProyectosEstrategicos', $data);
    }

    public function mesasSectoriales(){
        $dataPlanes = $this->PlanesModel->getByReunion(3);
        foreach ($dataPlanes as $planes){
            $dataMinutas = $this->MinutasModel->getByIdPlan($planes->idMv);
            foreach ($dataMinutas as $minutas){
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu'=>$this->reunionMenu,
            'planes'=>$dataPlanes
        );
        $this->load->view('mesasSectoriales', $data);
    }

    public function mesasTransversales(){
        $dataPlanes = $this->PlanesModel->getByReunion(4);
        foreach ($dataPlanes as $planes){
            $dataMinutas = $this->MinutasModel->getByIdPlan($planes->idMv);
            foreach ($dataMinutas as $minutas){
                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                $minutas->acuerdos = $dataAcuerdos;
            }
            $planes->minutas = $dataMinutas;
        }
        $this->reunionMenu = 1;

        $data = array(
            'reunionMenu'=>$this->reunionMenu,
            'planes'=>$dataPlanes
        );
        $this->load->view('mesasTransversales', $data);
    }

}

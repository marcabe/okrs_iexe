<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class PlanesController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PlanesModel');
        $this->load->model('ChatModel');
        $this->load->model('IndicadorModel');
        $this->load->model('ReunionesModel');
        $this->load->model('BitacoraIndicadorModel');
        $this->load->model('IndicadorTempModel');
        $this->load->model('UsuariosPlanesModel');
        $this->load->model('UsuariosModel');

        $this->load->model('BitacoraMovimientosModel');

        $this->load->model('MvModel');
        $this->load->model('ObjetivosModel');
        $this->load->model('KeyResultModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index()
    {
        $dataChats = array();
        $dataIndicadores = array();
        $totalActualizar = 0;
        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $tipo = $this->session->userdata('tipo');
            if ($tipo == 'capturista' || $tipo == 'admin') {
                $us = $this->session->userdata('idUser');
                $pluser = $this->UsuariosPlanesModel->getPlanesByUser($us);
                foreach ($pluser as $plu) {
                    $pl = $plu->plan;
                    $response = $this->PlanesModel->getById($pl);
                    $dataPlanes[] = $response[0];
                }
                if (isset($dataPlanes)) {

                    foreach ($dataPlanes as $planes) {
                        $dataUpdatePlan = $this->PlanesModel->getPlanCapUpdate($planes->idMv);
                        if (count($dataUpdatePlan) > 0) {
                            $dataUsuarios = $this->UsuariosModel->getByUser($dataUpdatePlan[0]->user);
                            $dataUpdatePlan[0]->nombre = $dataUsuarios[0]->nombre;
                            $planes->updatePlan = $dataUpdatePlan;
                        } else {
                            $planes->updatePlan = null;
                        }

                        $dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($planes->idMv);


                        $objetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
                        $planes->objetivos = $objetivos;
                        //$dataChats = $this->ChatModel->getChatByIdPlan($planes->idMv);


                        if ($tipo == 'admin') {
                            $dataUsers = $this->UsuariosModel->getByIdRol(2);
                            $dataChats = $this->ChatModel->getMeParaPlaen($dataUsers[0]->user, $planes->idMv, $this->session->userdata('idUser'));
                            $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesAdmin($planes->idMv);
                            $dataSadmins = $this->UsuariosModel->getByIdRol(2);
                            //var_dump($dataSadmins);die;
                            foreach ($dataSadmins as $sadmin) {
                                $dataObject = new \stdClass();
                                $dataObject->nombre = $sadmin->nombre;
                                $dataObject->apellidoP = $sadmin->apellidoP;
                                $dataObject->apellidoM = $sadmin->apellidoM;
                                $dataObject->nombrePuesto = $sadmin->nombrePuesto;
                                $dataObject->plan = $planes->idMv;
                                $dataObject->user = $sadmin->user;
                                array_push($dataUsuarios, $dataObject);
                            }
                            $planes->UsuariosChat = $dataUsuarios;

                        } elseif ($tipo == 'capturista') {
                            $dataUsers = $this->UsuariosModel->getByIdRol(3);
                            $dataChats = $this->ChatModel->getMeParaPlaen($dataUsers[0]->user, $planes->idMv, $this->session->userdata('idUser'));
                            $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesEnlace($planes->idMv);
                            $planes->UsuariosChat = $dataUsuarios;
                        }


                        $dataTotal = $this->PlanesModel->totalActualiza($planes->idMv);
                        $totalActualizar += count($dataTotal);
                        $planes->chat = $dataChats;
                        $planes->indicadores = $dataIndicadores;
                        foreach ($dataIndicadores as $indc) {
                            $indc->avanceReal = $indc->avance;
                            // $indc->avance = ($indc->avance * 100) / $indc->final;
                            $indc->avance = $indc->avancePorcentaje;

                            #Verificamos si hay algun indicador con avance
                            $dataBitacoraIndicador = $this->BitacoraIndicadorModel->getOneByIdIndicador($indc->idIndicadores);
                            $dataBitacoraIndicadorTemp = $this->IndicadorTempModel->getOneByIdIndicador($indc->idIndicadores);
                            $indc->bitacora = $dataBitacoraIndicador;
                            $indc->bitacoraTemp = $dataBitacoraIndicadorTemp;
                        }
                        $planes->totalActualizar = $totalActualizar;
                        $totalActualizar = 0;
                    }
                    $data = array(
                        'chats' => $dataChats,
                        'planes' => $dataPlanes,
                        'indicadores' => $dataIndicadores,
                        'admin' => false,
                    );
                } else {
                    $data = array(
                        'chats' => null,
                        'planes' => null,
                        'indicadores' => null,
                        'admin' => false,
                    );
                }
                $this->load->view('lista_planes', $data);
            } else {
                #Seccion para el superadmin
                $dataPlanes = $this->PlanesModel->get();
                foreach ($dataPlanes as $planes) {
                    #Hacemos consulta sobre los indicadores del plan
                    $objetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
                    $planes->objetivos = $objetivos;
                    #Hacemos consulta sobre los chat de cada plan
                    #Obtenemos el usuario del Gob
                    $dataUsers = $this->UsuariosModel->getByIdRol(1);
                    $dataChats = $this->ChatModel->getMeParaPlaen($dataUsers[0]->user, $planes->idMv, $this->session->userdata('idUser'));
                    if (count($dataChats) != 0) {
                        $planes->chat = $dataChats;
                    } else {
                        $dataUsers = $this->UsuariosModel->getByIdRol(2);
                        $dataChats = $this->ChatModel->getMeParaPlaen($dataUsers[0]->user, $planes->idMv, $this->session->userdata('idUser'));
                        if (count($dataChats) != 0) {
                            $planes->chat = $dataChats;
                        }
                    }


                    $dataTotal = $this->PlanesModel->totalActualiza($planes->idMv);
                    $totalActualizar += count($dataTotal);


                    $dataUsuarios = $this->UsuariosPlanesModel->getAllUsuariosByIdPlanesSadmin($planes->idMv);
                    $planes->UsuariosChat = $dataUsuarios;
                    $dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($planes->idMv);
                    $planes->indicadores = $dataIndicadores;
                    foreach ($dataIndicadores as $indc) {
                        //$indc->avanceReal = $indc->avance;
                        //$indc->avance = ($indc->avance * 100) / $indc->final;
                        //$indc->avance = $indc->avancePorcentaje;
                        #Verificamos si hay algun indicador con avance
                        $dataBitacoraIndicador = $this->BitacoraIndicadorModel->getOneByIdIndicador($indc->idIndicadores);
                        //$dataBitacoraIndicadorTemp = $this->IndicadorTempModel->getOneByIdIndicador($indc->idIndicadores);
                        $indc->bitacora = $dataBitacoraIndicador;
                        //$indc->bitacoraTemp = $dataBitacoraIndicadorTemp;
                    }
                    //var_dump($dataIndicadores->bitacora);

                    //$planes->totalActualizar = $totalActualizar;
                    //$totalActualizar = 0;

                    #Buscamos la ultima actualizacion
                    $dataUpdatePlan = $this->PlanesModel->getPlanCapUpdate($planes->idMv);
                    if (count($dataUpdatePlan) > 0) {
                        $dataUsuarios = $this->UsuariosModel->getByUser($dataUpdatePlan[0]->user);
                        $dataUpdatePlan[0]->nombre = $dataUsuarios[0]->nombre;
                        $planes->updatePlan = $dataUpdatePlan;
                    } else {
                        $planes->updatePlan = null;
                    }
                }

                /*$data = array(
                    'chats' => $dataChats,
                    'planes' => $dataPlanes,
                    'indicadores' => $dataIndicadores,
                    'admin' => false,
                );*/
                $data = array(
                    'chats' => $dataChats,
                    'planes' => $dataPlanes,
                    'admin' => false,
                );
                $this->load->view('lista_planes', $data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function getAnual()
    {
        $mv = $this->input->post('idmv');
        #verificamos si existe un objetivo anual
        $objetivoAnual = $this->ObjetivosModel->getAnual($mv);
        if (count($objetivoAnual) >= 1) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function alta()
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
            $dataReuniones = $this->ReunionesModel->get();
            $data = array(
                'admin' => false,
                'reuiniones' => $dataReuniones
            );
            $this->load->view('nuevo_plan', $data);
        } else {
            redirect(base_url());
        }
    }

    public function edita($idPlan)
    {
        if ($this->session->userdata('usuario') != null) {
            $dataReuniones = $this->ReunionesModel->get();

            $dataPlan = $this->PlanesModel->getById($idPlan);
            $dataPlan[0]->indicadores = $this->IndicadorModel->getIndicadoresByIdPlan($idPlan);
            //var_dump($dataPlan);die;
            $data = array(
                'admin' => false,
                'reuiniones' => $dataReuniones,
                'plan' => $dataPlan
            );
            $this->load->view('edicion_plan', $data);
        } else {
            redirect(base_url());
        }
    }

    public function insert()
    {
        $data = $this->input->post();
        $data['estado'] = 1;

        $porciones = explode("/", $data['finicial']);
        $data['finicial'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

        $porciones = explode("/", $data['ffinal']);
        $data['ffinal'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

        $result = $this->PlanesModel->insert($data);
        $data = array(
            'movimiento' => 'Alta de nuevo plan',
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($data);
        echo ($result != null) ? $result : 0;
    }


    public function update($idPlan = null)
    {
        if (isset($idPlan)) {
            $data = $this->input->post();

            $porciones = explode("/", $data['finicial']);
            $data['finicial'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];

            $porciones = explode("/", $data['ffinal']);
            $data['ffinal'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];


            $result = $this->PlanesModel->update($idPlan, $data);
            echo ($result != 0) ? 1 : 0;
        }
    }


    public function detalle($idPlan)
    {
        /*if($this->session->userdata('usuario')!= null ) {*/
        $dataPlan = $this->PlanesModel->getById($idPlan);

        $dataChat = $this->ChatModel->getMeParaPlaen('sadmin', $idPlan, $this->session->userdata('idUser'));
        $dataObjetivo = $this->ObjetivosModel->getObjetivosByPlanNoAnual($idPlan);
        $dataIndicadores = $this->IndicadorModel->getIndicadoresByIdPlan($idPlan);
        $dataObjetivoAnual = $this->ObjetivosModel->getAnual($idPlan);
        if (count($dataObjetivoAnual) == 0) {
            $dataObjetivoAnual = 0;
            $dataKrAnual = 0;
        } else {
            $dataKrAnual = $this->KeyResultModel->getByObjetivos($dataObjetivoAnual[0]->idObjetivo);
        }
        $dataUsers = $this->UsuariosModel->getByIdRol(2);
        $response = $this->ElementosMenu();


        $data = array(
            'admin' => true,
            'chat' => $dataChat,
            'indicadores' => $dataIndicadores,
            'planes' => $response,
            'pl' => $dataPlan,
            'obj' => $dataObjetivo,
            'objAnual' => $dataObjetivoAnual,
            'krAnual' => $dataKrAnual,
            'usuariosAdmin' => $dataUsers,
        );
        $this->load->view('detalle_plan', $data);
        /*}else{
            redirect(base_url());
        }*/
    }

    public function elimina($idPlan)
    {
        $response = $this->PlanesModel->deleteById($idPlan);
        $data = array(
            'movimiento' => 'Eliminacion del plan: ' . $idPlan,
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($data);
        echo $response;
    }

    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }

    public function editVisible($idPlan)
    {
        $visible = $this->input->post('visible');
        $data = array(
            "visible" => intval($visible)
        );
        $this->PlanesModel->update($idPlan, $data);
        echo 1;
    }

    public function updatePlanCap($idProyecto)
    {
        $data = array(
            "idProyecto" => $idProyecto,
            "presupuesto2018" => $this->input->post('presupuesto2018'),
            "presupuesto2019" => $this->input->post('presupuesto2019'),
            "indicadorEstrategico" => $this->input->post('indicadorEstrategico'),
            "inversion2018" => $this->input->post('inversion2018'),
            "inversion2019" => $this->input->post('inversion2019'),
            "metaIndicador" => $this->input->post('metaIndicador'),
            "fecha" => date("Y-m-d"),
            "user" => $this->input->post('user'),
            "status" => 0
        );

        $this->PlanesModel->insertPlanCapUpdate($data);

        echo 1;
    }

    public function intercambio($idCambio)
    {
        $dataCamio = $this->PlanesModel->getUpdateCambio($idCambio);
        $data = array(
            "presupuesto2018" => $dataCamio[0]->presupuesto2018,
            "presupuesto2019" => $dataCamio[0]->presupuesto2019,
            "indicadorEstrategico" => $dataCamio[0]->indicadorEstrategico,
            "inversion2018" => $dataCamio[0]->inversion2018,
            "inversion2019" => $dataCamio[0]->inversion2019,
            "metaIndicador" => $dataCamio[0]->metaIndicador
        );
        $idPlan = $dataCamio[0]->idProyecto;
        $this->PlanesModel->update($idPlan, $data);
        $data = array(
            "status" => 1
        );
        $this->PlanesModel->cambioUpdateCambio($idCambio, $data);
    }

    public function getInter($idCambio)
    {
        $dataCamio = $this->PlanesModel->getUpdateCambio($idCambio);
        echo json_encode($dataCamio);
    }

    public function cambiaStatusUpdate($idCambio)
    {
        $this->PlanesModel->cambioUpdateCambio($idCambio);
        echo 1;
    }

    public function getInfo($idProyecto)
    {
        $dataProyecto = $this->PlanesModel->getById($idProyecto);
        echo json_encode($dataProyecto);
    }

    public function updateInfo($idProyecto)
    {
        $data = $this->input->post();
        $this->PlanesModel->update($idProyecto, $data);
        echo 1;
    }


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccionesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model("ObjetivosModel");
        $this->load->model("AccionesModel");
		$this->load->model('KeyResultModel');
        $this->load->model('PlanesModel');

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');
	}


	 public function alta($idObejtivo){
		if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {

            $response = $this->ElementosMenu();
            $dataObj = $this->ObjetivosModel->getById($idObejtivo);
            $data = array(
                'idObjetivo' => $idObejtivo,
                'planes' => $response,
                'admin' => false,
                'nObjetivo' => $dataObj[0]->mv
            );

            $this->load->view('agrega_kr_b', $data);

        } else {
			redirect(base_url());
		}
	}
    public function getById()
    {
        $idAccion = $this->input->post('idAccion');
        $result = $this->AccionesModel->getByIdAccion($idAccion);
        echo json_encode($result);
    }

	public function insert(){
	    $data = $this->input->post();
	    $this->AccionesModel->insert($data);
	    $prom = 0;
	    $dataAcciones = $this->AccionesModel->getByIdKr($data['idKr']);
	    foreach ($dataAcciones as $acciones){
	        $prom += $acciones->avance;
        }
        $prom = $prom/count($dataAcciones);

        $this->KeyResultModel->updateAvance($data['idKr'], $prom);
        $this->KeyResultModel->updateAvancePorcentaje($data['idKr'], $prom);
        $dataKr = $this->KeyResultModel->getById($data['idKr']);
        $objetivoPrincipal = $dataKr[0]->idObjetivo;
        $dataUpdateObj = array(
            "avance" => $prom,
            "avancePorcentaje" => $prom
        );
        $this->ObjetivosModel->update($objetivoPrincipal, $dataUpdateObj);
        $dataObjetivos = $this->ObjetivosModel->getById($objetivoPrincipal);
        $idMv = $dataObjetivos[0]->idmv;
        $dataUpdatePlan = array(
            "avance" => $prom,
            "avanceIndicadores" => $prom
        );
        $this->PlanesModel->update($idMv, $dataUpdatePlan);

        $dataResponse = array(
            "idKr" => $data['idKr'],
            "promedio" => $prom,
            "objetivo" => $objetivoPrincipal
        );
        echo json_encode($dataResponse);
    }

    public function index(){
        $dataAcciones = $this->AccionesModel->getAll();

        $data = array(
	        "acciones"=>$dataAcciones
        );
        $this->load->view('lista_acciones', $data);

    }

    public function editAccion($idAccion){
        $data  = $this->input->post();
        $this->AccionesModel->editar($idAccion, $data);
        echo 1;
    }

    public function delete(){
        $sumaAvanceAcciones = 0;
        $sumaAvanceKr = 0;
	    $idAccion =  $this->input->post('idAccion');
        $this->AccionesModel->deleteAccion($idAccion);
        $dataAcciones = $this->AccionesModel->getByIdAccion($idAccion);
        #Obtenemos la Kr principal
        $krPrincipal = $dataAcciones[0]->idKr;
        #Obtenemos todas las acciones relacionadas con esa kr activas
        $dataAcciones = $this->AccionesModel->getByIdKrActivos($krPrincipal);

        foreach ($dataAcciones as $acciones) {
            #realizamos la suma de los avances de las acciones
            $sumaAvanceAcciones += $acciones->avance;
        }
        #Obtenemos el primedio de las acciones
        if(count($dataAcciones)!= 0 ) {
            $promedioKr = $sumaAvanceAcciones / count($dataAcciones);
        }else{
            $promedioKr = 0;
        }

        #Modificamos el avance de la Kr principal asi como su porcentaje
        $this->KeyResultModel->updateAvance($krPrincipal, $promedioKr);
        $this->KeyResultModel->updateAvancePorcentaje($krPrincipal, $promedioKr);
        #Obtenemos la infomacion de la kr para obtener el objetivo
        $dataKeyResult = $this->KeyResultModel->getById($krPrincipal);
        $objetivoPrincipal = $dataKeyResult[0]->idObjetivo;
        $dataKeyResult = $this->KeyResultModel->getByIdObj($objetivoPrincipal);
        foreach ($dataKeyResult as $kr) {
            #realizamos la suma de los avances de los OKR
            $sumaAvanceKr += $kr->avancePorcentaje;
        }
        if(count($dataKeyResult)!=0) {
            $promedioObjetivo = $sumaAvanceKr / count($dataKeyResult);
        }else{
            $promedioObjetivo = 0;
        }
        $dataUpdateObj = array(
            "avance" => $promedioObjetivo,
            "avancePorcentaje" => $promedioObjetivo
        );

        $this->ObjetivosModel->update($objetivoPrincipal, $dataUpdateObj);

        $dataObjetivos = $this->ObjetivosModel->getById($dataKeyResult[0]->idObjetivo);
        $idPlan = $dataObjetivos[0]->idmv;
        $dataUpdatePlan = array(
            "avanceIndicadores" => $promedioObjetivo
        );
        $this->PlanesModel->update($idPlan, $dataUpdatePlan);

        $dataResponse = array(
            "promedio" => $promedioKr,
            "promedioObjetivo" => $promedioObjetivo,
            "objetivo" => $objetivoPrincipal,
            "kr" => $krPrincipal
        );
        echo json_encode($dataResponse);
    }

    public function editarAvance(){
        $idAccion = $this->input->post('idAccion');
        $avance = $this->input->post('avance');
        $this->AccionesModel->updateAvance($idAccion, $avance);

        #obtenemos la informacion de la accion para obtener la kr principal
        $dataAcciones = $this->AccionesModel->getByIdAccion($idAccion);
        #Obtenemos la Kr principal
        $krPrincipal = $dataAcciones[0]->idKr;
        #Obtenemos todas las acciones relacionadas con esa kr activas
        $dataAcciones = $this->AccionesModel->getByIdKrActivos($krPrincipal);
        $sumaAvanceAcciones=0;
        foreach ($dataAcciones as $acciones) {
            #realizamos la suma de los avances de las acciones
            $sumaAvanceAcciones += $acciones->avance;
        }
        #Obtenemos el primedio de las acciones
        $promedioKr = $sumaAvanceAcciones / count($dataAcciones);

        #Modificamos el avance de la Kr principal asi como su porcentaje
        $this->KeyResultModel->updateAvance($krPrincipal, $promedioKr);
        $this->KeyResultModel->updateAvancePorcentaje($krPrincipal, $promedioKr);
        #Obtenemos la infomacion de la kr para obtener el objetivo
        $dataKeyResult = $this->KeyResultModel->getById($krPrincipal);
        $objetivoPrincipal = $dataKeyResult[0]->idObjetivo;
        $dataKeyResult = $this->KeyResultModel->getByIdObj($objetivoPrincipal);
        $sumaAvanceKr=0;
        foreach ($dataKeyResult as $kr) {
            #realizamos la suma de los avances de los OKR
            $sumaAvanceKr += $kr->avancePorcentaje;
        }
        $promedioObjetivo = $sumaAvanceKr / count($dataKeyResult);
        $dataUpdateObj = array(
            "avance" => $promedioObjetivo,
            "avancePorcentaje" => $promedioObjetivo
        );
        $this->ObjetivosModel->update($objetivoPrincipal, $dataUpdateObj);

        $dataObj = $this->ObjetivosModel->getById($dataKeyResult[0]->idObjetivo);
        $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($dataObj[0]->idmv);
        $promPlan = 0;
        foreach ($dataObjetivos as $objetivos){
            $promPlan += $objetivos->avancePorcentaje;
        }
        $promPlan = $promPlan/count($dataObjetivos);

        #Provisional
        $dataInsertPlan = array("avanceIndicadores"=>$promedioObjetivo);
        $this->PlanesModel->update($dataObj[0]->idmv, $dataInsertPlan);


        $dataResponse = array(
            "promedio" => $promedioKr,
            "promedioObjetivo" => $promedioObjetivo,
            "objetivo" => $objetivoPrincipal,
            "kr" => $krPrincipal
        );
        echo json_encode($dataResponse);
    }


    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }

}

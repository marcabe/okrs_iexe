<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pdfPlanesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model("pdfPlanesModel");

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');
	}

	public function index(){
	    echo "hola";
    }

	public function getByIdPlan($idPlan){
        $dataPdf = $this->pdfPlanesModel->getByIdPlan($idPlan);
        echo json_encode($dataPdf);
    }

    public function compara($cadena)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);

    }

    public function upFile()
    {
        $upload_folder = 'pdfPlan';
        foreach ($_FILES as $i) {
            $archi = $this->compara($i['name']);
            $nombre_archivo = $archi;
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, utf8_encode($archivador));
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }

    public function borrarArchivoTemporal()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfPlan';
        unlink($upload_folder . '/' . $archivo);
    }

    public function insert($idPlan){
        $data = $this->input->post();
        if (isset($data['archivos'])) {
            foreach ($data['archivos'] as $anexos) {
                $dataInsert = array(
                    "idPlan" => $idPlan,
                    "pdf" => $anexos
                );
                $this->pdfPlanesModel->insert($dataInsert);
            }
        }
        echo 1;
    }

}

<?php

class MvModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "plan";
	}

	public function get(){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where('estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getById($idMv){
		$this->db->select("*");
		$this->db->from($this->tabla);
		$this->db->where("idMv",$idMv);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}


	public function deleteById($idMv){
		$this->db->where('idMv', $idMv);
		if($this->db->update($this->tabla, array('estado' => 0)))
			return 1;
		else
			return 0;
	}

}

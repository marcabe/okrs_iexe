<?php

class BitacoraModificacionesModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "bitacoramodificaciones";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

}
